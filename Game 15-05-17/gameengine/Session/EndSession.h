#pragma once

#include "Session.h"
#include "../gameobjects/GUI/EndGUI.h"
#include "../gameobjects/GUI/GUI Primitives/Text2D.h"

#include "../gameobjects/Users/Player.h"

class EndSession : public Session {
public:

	EndSession(glm::mat4* Projection, AssetLibrary* models);

	void Init();
	void Draw();
	void Update(double deltaT);

	void passGameData(int winner, Player* playerOne, Player* playerTwo);

	bool keyboardProcess(SDL_KeyboardEvent* keyStates, double deltaT);
	void mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT);

private:

	EndGUI* endGUI;
	Text2D textWriter;

	int winner;
	pair<int, int> unitsBuilt;
	pair<int, int> unitsLost;
	pair<int, int> buildingsBuilt;
	pair<int, int> buildingsLost;
	pair<int, int> resourcesGained;
	pair<int, int> resourcesSpent;

};
