#include "EndSession.h"

EndSession::EndSession(glm::mat4 * Projection, AssetLibrary* assets) {
	this->assets = assets;

	this->textWriter = Text2D(assets->getFont(), assets->getFontShader());
}

void EndSession::Init() {

	endGUI = new EndGUI(assets);

	this->textWriter.init();

	next = END;
}

void EndSession::Draw() {

	endGUI->Draw();

	string winTitle;
	switch (winner) {
	case 0:
		winTitle = "Game Forfeited";
		break;
	case 1:
		winTitle = "Player One Wins";
		break;
	case 2:
		winTitle = "Player Two Wins";
		break;
	}
		
	this->textWriter.printString(winTitle.c_str(), glm::vec2(-0.27,-0.48), assets->getScreenDim(), 60);

	//Stat Table
	stringstream statHead;
	statHead << "Player # \t Resources Gained \t Resources Spent \t Units Built \t Units Lost \t Buildings Built \t Buildings Lost";
	this->textWriter.printString(statHead.str().c_str(), glm::vec2(-0.69, -0.3), assets->getScreenDim(), 25);

	stringstream stat1;
	stat1 << " 1 \t\t\t\t\t\t\t\t\t\t\t\t\t " << resourcesGained.first << " \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t " << resourcesSpent.first << " \t\t\t\t\t\t\t\t\t\t\t\t " << unitsBuilt.first << " \t\t\t\t\t\t\t\t\t\t " << unitsLost.first << " \t\t\t\t\t\t\t\t\t\t\t\t " << buildingsBuilt.first << " \t\t\t\t\t\t\t\t\t\t\t\t\t " << buildingsLost.first;
	this->textWriter.printString(stat1.str().c_str(), glm::vec2(-0.69, -0.11), assets->getScreenDim(), 25);

	stringstream stat2;
	stat2 << " 2 \t\t\t\t\t\t\t\t\t\t\t\t\t " << resourcesGained.second << " \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t " << resourcesSpent.second << " \t\t\t\t\t\t\t\t\t\t\t\t " << unitsBuilt.second << " \t\t\t\t\t\t\t\t\t\t " << unitsLost.second << " \t\t\t\t\t\t\t\t\t\t\t\t " << buildingsBuilt.second << " \t\t\t\t\t\t\t\t\t\t\t\t  " << buildingsLost.second;
	this->textWriter.printString(stat2.str().c_str(), glm::vec2(-0.69, 0.07), assets->getScreenDim(), 25);

}

void EndSession::Update(double deltaT) {
	endGUI->Update();

	if (endGUI->mainMenuButt->isSelected == 2)
		next = MENU;

}

void EndSession::passGameData(int winner, Player * playerOne, Player * playerTwo){
	this->winner = winner;
	this->unitsBuilt = pair<int, int>(playerOne->unitsBuilt, playerTwo->unitsBuilt);
	this->unitsLost = pair<int, int>(playerOne->unitsLost, playerTwo->unitsLost);
	this->buildingsBuilt = pair<int, int>(playerOne->buildingsBuilt, playerTwo->buildingsBuilt);
	this->buildingsLost = pair<int, int>(playerOne->buildingsLost, playerTwo->buildingsLost);
	this->resourcesGained = pair<int, int>(playerOne->resourcesGained, playerTwo->resourcesGained);
	this->resourcesSpent = pair<int, int>(playerOne->resourcesSpent, playerTwo->resourcesSpent);

}

bool EndSession::keyboardProcess(SDL_KeyboardEvent * keyStates, double deltaT) {

	return false;
}

void EndSession::mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT) {

	glm::vec4 normalisedCoords = glm::vec4(
		(2.0f*mouse.x) / screen.x - 1.0f,	// x
		1.0f - (2.0f*mouse.y) / screen.y,  // y
		0.5f,							// z
		1.0f							// w
	);
	switch (event.type)
	{
	case SDL_MOUSEMOTION: {
		//if mouse in GUI perform GUI related operations
		if (endGUI->checkGUIcollision(normalisedCoords)) {
			endGUI->checkHighlight(normalisedCoords);
		}
		break;
	}
	case SDL_MOUSEBUTTONDOWN:
	{
		SDL_MouseButtonEvent mbe = event.button;
		if (mbe.button == SDL_BUTTON_LEFT) {
			//if mouse in GUI perform GUI related operations
			if (endGUI->checkGUIcollision(normalisedCoords)) {
				endGUI->checkClicked(normalisedCoords);
			}
		}
		break;
	}
	}

}
