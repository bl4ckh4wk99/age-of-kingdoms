#pragma once


#include "Session.h"
#include "GameSession.h"
#include "MenuSession.h"
#include "EndSession.h"
#include "CompSession.h"

class SessionHandler {

public:
	Session* curSession;
	SessionTypes current;

	void Init(DisplayWindow* display);
	void Draw();
	void Update(double deltaT);

	void Reshape(int width, int height);
	bool KeyboardControl(SDL_KeyboardEvent* keyStates, double deltaT);
	void MouseControl(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT);

private:

	DisplayWindow* display;
	Uint8* KeyStates;
	Session* sessions[NUM_SESSIONS];
	AssetLibrary* assets;

	double deltaT;

	glm::mat4 ProjectionMatrix;

	void swapSessions(SessionTypes old, SessionTypes next);
};