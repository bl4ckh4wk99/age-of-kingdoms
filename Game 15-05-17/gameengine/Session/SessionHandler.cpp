#include "SessionHandler.h"


void SessionHandler::Init(DisplayWindow* display){
	this->display = display;

	glm::vec2 screenDim = glm::vec2(display->screenWidth, display->screenHeight);
	assets = new AssetLibrary(screenDim);

	//Populate sessions Array
	sessions[MENU] = new MenuSession(&ProjectionMatrix, assets);
	sessions[END] = new EndSession(&ProjectionMatrix, assets);
	sessions[PvAI] = new GameSession(&ProjectionMatrix, assets);
	sessions[AIvAI] = new CompSession(&ProjectionMatrix, assets);

	//Set session and initilise it
	current = MENU;
	curSession = sessions[MENU];
	curSession->Init();
	assets->playMenuMusic();
}

void SessionHandler::Draw(){
	display->ClearColour(1.0f, 0.0f, 0.0f, 1.0f); // Set clear colour

	curSession->Draw();
}

void SessionHandler::Update(double deltaT){
	curSession->Update(deltaT);

	if (current != curSession->next)
		swapSessions(current, curSession->next);
}

void SessionHandler::Reshape(int width, int height){
	// Reset The Current Viewport
	glViewport(0, 0, width, height);

	//Set the projection matrix
	ProjectionMatrix = glm::perspective(70.0f, (GLfloat)width / (GLfloat)height, 0.1f, 10000.0f);
}

bool SessionHandler::KeyboardControl(SDL_KeyboardEvent* keyStates, double deltaT){
	return curSession->keyboardProcess(keyStates, deltaT);
}

void SessionHandler::MouseControl(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT){
	curSession->mouseProcess(event, mouse, screen, deltaT);
}

void SessionHandler::swapSessions(SessionTypes old, SessionTypes next){

	switch (old)
	{
		case MENU: {
			if (next == PvAI) {
				curSession = sessions[PvAI];
				curSession->Init();
				assets->stopMusic();
				assets->playGameMusic();
				current = PvAI;
			}
			if (next == AIvAI) {
				curSession = sessions[AIvAI];
				curSession->Init();
				assets->stopMusic();
				assets->playGameMusic();
				current = AIvAI;
			}

			break;
		}
		case PvAI:{
			if (next == END) {
				//Pass game data
				GameSession* temp = (GameSession*)curSession;
				EndSession* temp2 = (EndSession*)sessions[END];
				temp2->passGameData(temp->getWinner(), temp->getPlayerOne(), temp->getPlayerTwo());
				assets->stopMusic();
				assets->playMenuMusic();
				curSession = sessions[END];
				curSession->Init();
				current = END;
			}
			if (next == MENU) {
				curSession = sessions[MENU];
				curSession->Init();
				assets->stopMusic();
				assets->playMenuMusic();
				current = MENU;
			}

			break;
		}
		case AIvAI: {
			if (next == END) {
				//Pass game data
				GameSession* temp = (GameSession*)curSession;
				EndSession* temp2 = (EndSession*)sessions[END];
				temp2->passGameData(temp->getWinner(), temp->getPlayerOne(), temp->getPlayerTwo());
				assets->stopMusic();
				assets->playMenuMusic();
				curSession = sessions[END];
				curSession->Init();
				current = END;
			}
			if (next == MENU) {
				curSession = sessions[MENU];
				curSession->Init();
				assets->stopMusic();
				assets->playMenuMusic();
				current = MENU;
			}

			break;
		}
		case END: {
			if (next == MENU) {
				curSession = sessions[MENU];
				curSession->Init();
				current = MENU;
			}
			break;
		}
	}


}
