#pragma once

#include "Session.h"
#include "../gameobjects/Primitives/CubeMap.h"
#include <windows.h>

#include "../gameobjects/World/World.h"
#include "../gameobjects/Users/Player.h"
#include "../gameobjects/Users/Human.h"
#include "../gameobjects/Users/AI.h"
#include "../gameobjects/Primitives/Ray.h"
#include "../gameobjects/Primitives/AStar.h"

class CompSession : public Session {
public:

	CompSession(glm::mat4* Projection, AssetLibrary* models);

	void Init();
	void Draw();
	void Update(double deltaT);

	inline Player* getPlayerOne() { return this->playerOne; }
	inline Player* getPlayerTwo() { return this->playerTwo; }
	inline int getWinner() { return this->winner; }

	bool keyboardProcess(SDL_KeyboardEvent* keyStates, double deltaT);
	void mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT);

private:

	AI* playerOne;
	AI* playerTwo;

	int winner;

	AssetLibrary* assets;

	CubeMap* skyBox;
	Shader* cubeMapShader;

	World gameWorld;

	double GetRunningTime() { double runningTime = ((double)GetTickCount() - (double)startTime) / 1000.0f; return runningTime; }
	long long startTime;

	glm::vec2 projectRaytoGrid(glm::vec2 mouse, glm::vec2 screen);
};
