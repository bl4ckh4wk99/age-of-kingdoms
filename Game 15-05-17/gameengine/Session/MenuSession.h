#pragma once

#include "Session.h"
#include "../gameobjects/GUI/MenuGUI.h"


class MenuSession : public Session {
public:

	MenuSession(glm::mat4* Projection, AssetLibrary* models);

	void Init();
	void Draw();
	void Update(double deltaT);

	bool keyboardProcess(SDL_KeyboardEvent* keyStates, double deltaT);
	void mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT);

private:

	MenuGUI* menuGUI;

};
