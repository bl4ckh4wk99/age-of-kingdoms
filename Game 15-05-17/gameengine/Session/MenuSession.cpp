#include "MenuSession.h"

MenuSession::MenuSession(glm::mat4 * Projection, AssetLibrary* assets){
	this->assets = assets;

}

void MenuSession::Init(){

	menuGUI = new MenuGUI(assets);

	next = MENU;
}

void MenuSession::Draw(){

	menuGUI->Draw();

}

void MenuSession::Update(double deltaT){
	menuGUI->Update();

	if (menuGUI->PvAIButt->isSelected == 2)
		next = PvAI;
	if (menuGUI->AIvAIButt->isSelected == 2)
		next = AIvAI;

}

bool MenuSession::keyboardProcess(SDL_KeyboardEvent * keyStates, double deltaT){

	if (keyStates->keysym.scancode == SDL_SCANCODE_ESCAPE) {
		return true;
	}

	return false;
}

void MenuSession::mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT){

	glm::vec4 normalisedCoords = glm::vec4(
		(2.0f*mouse.x) / screen.x - 1.0f,	// x
		1.0f - (2.0f*mouse.y) / screen.y,  // y
		0.5f,							// z
		1.0f							// w
	);
	switch (event.type)
	{
	case SDL_MOUSEMOTION: {
		//if mouse in GUI perform GUI related operations
		if (menuGUI->checkGUIcollision(normalisedCoords)) {
			menuGUI->checkHighlight(normalisedCoords);
		}
		break;
	}
	case SDL_MOUSEBUTTONDOWN:
	{
		SDL_MouseButtonEvent mbe = event.button;
		if (mbe.button == SDL_BUTTON_LEFT) {
			//if mouse in GUI perform GUI related operations
			if (menuGUI->checkGUIcollision(normalisedCoords)) {
				menuGUI->checkClicked(normalisedCoords);
			}
		}
		break;
	}
	}

}
