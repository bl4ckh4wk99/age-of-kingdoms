#pragma once

#include "../IO/DisplayWindow.h"
#include "../gameobjects/AssetLibrary.h"

enum SessionTypes { MENU, PvAI, AIvAI, END, NUM_SESSIONS };

class Session {

public:
	SessionTypes next;

	virtual void Init() = 0;
	virtual void Draw() = 0;
	virtual void Update(double deltaT) = 0;

	//Protyping methods
	virtual bool keyboardProcess(SDL_KeyboardEvent* keyStates, double deltaT) = 0;
	virtual void mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT) = 0;
protected:
	//Program Variables
	DisplayWindow* display;
	int *screenWidth, *screenHeight;
	AssetLibrary* assets;

	//Session Variables
	glm::mat4* ProjectionMatrix;
	Camera camera;

};
