#include "CompSession.h"

CompSession::CompSession(glm::mat4* Projection, AssetLibrary* assets) {
	this->ProjectionMatrix = Projection;
	this->camera = Camera(glm::vec3(0, 100, 50), this->ProjectionMatrix);
	this->camera.lookAtPlayerOne();

	this->assets = assets;

	//cubeMap
	cubeMapShader = assets->getCubeMapShader();
	skyBox = new CubeMap();

	gameWorld = World(assets);
	gameWorld.Init();

	startTime = GetTickCount();
}

void CompSession::Init() {
	next = AIvAI;

	playerOne = new AI(gameWorld.getGrid(), assets, ONE);
	playerTwo = new AI(gameWorld.getGrid(), assets, TWO);
	winner = 0;

	playerOne->Init(playerTwo);
	Building* throne = playerOne->addBuilding(PlayerOneStart, THRONE);
	throne->instaBuild();
	playerOne->income += throne->income;
	playerOne->selectedBuilding = nullptr;
	playerOne->addUnit(throne->exitCell(), BUILDER);
	playerOne->selectedUnit = nullptr;
	playerOne->resourcesSpent = 0;

	playerTwo->Init(playerOne);
	throne = playerTwo->addBuilding(PlayerTwoStart, THRONE);
	throne->instaBuild();
	playerTwo->income += throne->income;
	playerTwo->addUnit(throne->exitCell(), BUILDER);
	playerTwo->resourcesSpent = 0;
}

void CompSession::Draw() {

	//----------------------------Sky Box-------------------------
	glDisable(GL_DEPTH_TEST);

	glDepthMask(GL_FALSE);

	cubeMapShader->Bind();
	cubeMapShader->UpdateSkyBox(&camera);

	skyBox->draw();

	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);


	//----------------------------World Map------------------------
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	gameWorld.Draw(&camera);

	//----------------------------Game Objects--------------------


	playerOne->Draw(&camera);
	playerTwo->Draw(&camera);
}

void CompSession::Update(double deltaT) {

	playerOne->Update(deltaT);
	playerTwo->Update(deltaT);

	gameWorld.Update(deltaT);

	if (playerOne->isDead()) {
		winner = 2;
		next = END;
	}
	if (playerTwo->isDead()) {
		winner = 1;
		next = END;
	}
}

bool CompSession::keyboardProcess(SDL_KeyboardEvent* keyEvent, double deltaT) {


	if (keyEvent->type == SDL_KEYDOWN) {
		if (keyEvent->keysym.scancode == SDL_SCANCODE_W)
			camera.moveDir.y = 1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_S)
			camera.moveDir.y = -1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_A)
			camera.moveDir.x = -1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_D)
			camera.moveDir.x = 1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_Q)
			camera.rotDir = -1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_E)
			camera.rotDir = 1;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_SPACE)
			next = END;
	}
	else if (keyEvent->type == SDL_KEYUP) {
		if (keyEvent->keysym.scancode == SDL_SCANCODE_W)
			camera.moveDir.y = 0;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_S)
			camera.moveDir.y = 0;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_A)
			camera.moveDir.x = 0;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_D)
			camera.moveDir.x = 0;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_Q)
			camera.rotDir = 0;

		if (keyEvent->keysym.scancode == SDL_SCANCODE_E)
			camera.rotDir = 0;
	}

	camera.move(1);
	camera.rotate(1);

	if (keyEvent->keysym.scancode == SDL_SCANCODE_ESCAPE) {
		next = END;
		winner = 0;
	}

	return false;
}

void CompSession::mouseProcess(SDL_Event event, glm::vec2 mouse, glm::vec2 screen, double deltaT) {


	//Camera Zoom
	if (event.type == SDL_MOUSEWHEEL) {

		if (event.wheel.y < 0) {
			camera.zoomDir = -1;
		}
		else {
			camera.zoomDir = 1;
		}
		camera.zoom(1);
	}


	/********************Human Input***************************/

	glm::vec2 mouseCellPos = projectRaytoGrid(mouse, screen);

	glm::vec4 normalisedCoords = glm::vec4(
		(2.0f*mouse.x) / screen.x - 1.0f,	// x
		1.0f - (2.0f*mouse.y) / screen.y,  // y
		0.5f,							// z
		1.0f							// w
	);

	normalisedCoords.y = 1.0f - (2.0f*mouse.y) / screen.y;
	mouse.y = ((1.0f - normalisedCoords.y) * screen.y) / 2.0f;


	switch (event.type)
	{
	//Human Game Input
	case SDL_MOUSEBUTTONDOWN:
	{
		SDL_MouseButtonEvent mbe = event.button;

			if (mbe.button == SDL_BUTTON_LEFT) {
				gameWorld.highLightSelect(mouseCellPos);
			}
			else {
				if (mbe.button == SDL_BUTTON_RIGHT) {
					gameWorld.highLightAction(mouseCellPos);
				}
			}
		break;
	}
	}

}

glm::vec2 CompSession::projectRaytoGrid(glm::vec2 mouse, glm::vec2 screen) {
	Ray ray = Ray(mouse.x, screen.y - mouse.y, screen.x, screen.y, &camera);
	glm::vec3 rayDir = ray.getRayDir();

	glm::vec3 camPos = camera.getPos();
	float modifier = camPos.y / -rayDir.y;

	glm::vec3 worldCoords = glm::vec3(camPos.x + (rayDir.x * modifier), 0, camPos.z + (rayDir.z * modifier));
	return gameWorld.getGrid()->getCellFromPoint(worldCoords);
}
