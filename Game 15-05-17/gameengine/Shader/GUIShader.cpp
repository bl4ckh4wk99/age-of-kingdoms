#include "GUIShader.h"

GUIShader::GUIShader(const string & fileName){

	main_program = glCreateProgram(); // Create new program
	main_shaders[0] = CreateShader(LoadShader(fileName + ".vert"), GL_VERTEX_SHADER); // Create a new vertex shader
	main_shaders[1] = CreateShader(LoadShader(fileName + ".frag"), GL_FRAGMENT_SHADER); // Create a new fragment shader

	for (int i = 0; i < NUM_SHADERS; i++)
		glAttachShader(main_program, main_shaders[i]); // Attach shaders to our program

	bool failed = false;

	glLinkProgram(main_program); // Link the program
	failed = CheckShaderError(main_program, GL_LINK_STATUS, true, "Error: Shader failed to link with program."); // Check for shader link errors

	glValidateProgram(main_program); // Validate the program
	failed = CheckShaderError(main_program, GL_VALIDATE_STATUS, true, "Error: Validation of program failed."); // Check for shader validation errors

	if (!failed)
		cout << "Shader: " << fileName << " Loaded Succesfully" << endl;

	gui_uniforms[COLOUR] = glGetUniformLocation(main_program, "Colour");

	gui_uniforms[IS_SELECTED] = glGetUniformLocation(main_program, "isSelected");

	gui_uniforms[DIFFUSE_MAP] = glGetUniformLocation(main_program, "DiffuseMap");

}

void GUIShader::Update(int isSelected){

	glUniform1i(gui_uniforms[IS_SELECTED], isSelected);
	openGLErrorCheck(__FILE__, __LINE__);
}

void GUIShader::UpdateHealthBar(glm::vec3 colour){

	glUniform3f(gui_uniforms[COLOUR], colour.x, colour.y, colour.z);
}


GUIShader::~GUIShader() {
	for (int i = 0; i < NUM_SHADERS; i++) // Unbind and delete shaders
	{
		glDetachShader(main_program, main_shaders[i]);
		glDeleteShader(main_shaders[i]);
	}
	glDeleteProgram(main_program); // Remove program
}
