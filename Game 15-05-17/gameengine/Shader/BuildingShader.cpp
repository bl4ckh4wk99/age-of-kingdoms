#include "BuildingShader.h"

BuildingShader::BuildingShader(const string & fileName):ModelShader(fileName){

	build_uniforms[BUILDING_ALPHA] = glGetUniformLocation(main_program, "BuildingAlpha");
}

void BuildingShader::Update(float buildingAlpha, Transform * transform, Camera * camera){
	ModelShader::Update(transform, camera);

	glUniform1f(build_uniforms[BUILDING_ALPHA], buildingAlpha);
}

BuildingShader::~BuildingShader(){
	ModelShader::~ModelShader();

}
