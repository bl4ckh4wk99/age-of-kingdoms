#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat3 NormalMatrix;

layout (location = 0) in vec3 in_position; 
layout (location = 1) in vec2 in_texCoord; 
layout (location = 2) in vec3 in_normal; 

out vec2 vert_texCoord;
out vec3 vert_normal;

void main(void)
{
	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(in_position, 1.0);

	vert_normal = NormalMatrix * in_position;
	
	vert_texCoord = in_texCoord;
}