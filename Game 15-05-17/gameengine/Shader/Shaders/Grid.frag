// Fragment Shader

#version 330

in  vec4 ex_Color;  //colour arriving from the vertex
out vec4 out_Color; //colour for the pixel

void main(void)
{
	out_Color = ex_Color;
	//out_Color = vec4(0.0, 0.5, 0.0, 0.5);
}