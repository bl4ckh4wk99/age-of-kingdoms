#version 330

uniform vec3 Colour;

layout (location = 0) in vec3 in_position;

out vec3 ex_Colour;

void main(void)
{
	gl_Position = vec4(in_position, 1.0);

	ex_Colour = Colour;
}