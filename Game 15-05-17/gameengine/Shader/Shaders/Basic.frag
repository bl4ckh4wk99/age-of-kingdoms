// Fragment Shader

#version 150

in  vec3 ex_Color;  //colour arriving from the vertex
out vec4 out_Color; //colour for the pixel

void main(void)
{
	out_Color = mix(vec4(0.0,1.0,0.0,1.0),vec4(ex_Color,1.0),0.6);
}