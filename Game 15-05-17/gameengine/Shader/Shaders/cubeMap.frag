#version 330 

uniform samplerCube CubeMap;

in vec3 vert_texCoord;

out vec4 out_col;

void main()
{    
	//colour is calculated using texture
    out_col = texture(CubeMap, vert_texCoord);
	//out_col = vec4(1.0,0.0,0.0,1.0);
}
  