#version 330

in vec2 vert_texCoord;

//Texture
uniform sampler2D DiffuseMap;


out vec4 out_col;

void main(void)
{
	out_col = vec4(texture(DiffuseMap, vert_texCoord).rgba);
}