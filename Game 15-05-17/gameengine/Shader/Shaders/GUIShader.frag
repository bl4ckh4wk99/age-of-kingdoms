#version 430

uniform sampler2D DiffuseMap;
uniform int isSelected;

in vec2 vert_tex;
out vec4 out_col;

void main()
{
	if(isSelected == 3){
		out_col = vec4(mix(texture(DiffuseMap, vert_tex), vec4(1.0, 0.0, 0.0, 0.5f), 0.6f));
	}
	else{ 
		if(isSelected == 2){
			out_col = vec4(mix(texture(DiffuseMap, vert_tex), vec4(0.0, 0.2, 0.4, 0.5f), 0.6f));
		}
		else{ 
			if(isSelected == 1){
				out_col = vec4(mix(texture(DiffuseMap, vert_tex), vec4(1.0, 1.0, 0.2, 0.5f), 0.6f));
			}
			else{
				out_col = texture(DiffuseMap, vert_tex);
			}
		}
	}
}