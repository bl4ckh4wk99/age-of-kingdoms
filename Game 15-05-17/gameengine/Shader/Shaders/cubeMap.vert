#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;

in vec3 in_position;

out vec3 vert_texCoord;

void main()
{
	//position is as passed
    gl_Position =   ProjectionMatrix * ViewMatrix * vec4(in_position, 1.0);
	//tex coords are the posiiton
    vert_texCoord = in_position;
} 