#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;

in  vec4 in_Position;  // Position coming in

out vec4 ex_Color;     // colour leaving the vertex, this will be sent to the fragment shader

void main(void)
{
	gl_Position = ProjectionMatrix * ModelMatrix * ViewMatrix * vec4(in_Position.xyz, 1.0);
	
	if(in_Position.w == 1.0){
		ex_Color = vec4(0,0,0,0.0);
	}
	else{if(in_Position.w == 2.0){
			ex_Color = vec4(0,0,1,0.5);
		}
		else{
			ex_Color = vec4(1,0,0,0.5);
		}
	}
}