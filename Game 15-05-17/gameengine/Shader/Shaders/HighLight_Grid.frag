// Fragment Shader

#version 330

in vec3 ex_Colour;
in float ex_Mix;

out vec4 out_Colour; //colour for the pixel

void main(void)
{
	out_Colour = vec4(ex_Colour, ex_Mix);
	//out_Colour = vec4(ex_Colour, 1.0);
}