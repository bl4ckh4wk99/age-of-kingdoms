// Fragment Shader

#version 330

in vec3 ex_Colour;  //colour arriving from the vertex

out vec4 out_Colour; //colour for the pixel

void main(void)
{
	out_Colour = vec4(ex_Colour,1.0);

}