#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat3 NormalMatrix;

layout (location = 0) in vec3 in_Position;
layout (location = 1) in vec3 in_Colour;

out vec3 ex_Colour;

void main(void)
{
	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(in_Position, 1.0);

	ex_Colour = in_Colour;
}