#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat3 NormalMatrix;


const int MAX_BONES = 100;
uniform mat4 Bones[MAX_BONES];

layout (location = 0) in vec3 in_position; 
layout (location = 1) in vec2 in_texCoord; 
layout (location = 2) in vec3 in_normal;
layout (location = 3) in ivec4 in_BoneIDs;
layout (location = 4) in vec4 in_Weights;

out vec2 vert_texCoord;
out vec3 vert_normal;

void main(void)
{
	mat4 BoneTransform = Bones[in_BoneIDs[0]] * in_Weights[0];
    BoneTransform += Bones[in_BoneIDs[1]] * in_Weights[1];
    BoneTransform += Bones[in_BoneIDs[2]] * in_Weights[2];
    BoneTransform += Bones[in_BoneIDs[3]] * in_Weights[3];

	vec4 pos = BoneTransform * vec4(in_position, 1.0);
	gl_Position = ProjectionMatrix * ModelMatrix * ViewMatrix * pos;

	vec4 normalL = BoneTransform * vec4(in_normal, 0.0);
	vert_normal = NormalMatrix * vec3(normalL);
	
	vert_texCoord = in_texCoord;
}