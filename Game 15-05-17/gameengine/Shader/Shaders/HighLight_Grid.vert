#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;

layout (location = 0) in vec3 in_Position; 
layout (location = 1) in vec3 in_Colour; 
layout (location = 2) in float in_Mix; 

out vec3 ex_Colour;     // colour leaving the vertex, this will be sent to the fragment shader
out float ex_Mix;

void main(void)
{
	gl_Position = ProjectionMatrix * ModelMatrix * ViewMatrix * vec4(in_Position, 1.0);
	
	ex_Colour = in_Colour;
	ex_Mix = in_Mix;
}