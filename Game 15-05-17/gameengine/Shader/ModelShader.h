#pragma once
#include "Shader.h"


class ModelShader : public Shader {

public:

	ModelShader() {}
	ModelShader(const string& fileName);

	//void Bind();
	void Update(Transform* transform, Camera* camera);
	void UpdateStatic(Camera* camera);

	virtual ~ModelShader();

	GLuint getTextureUniform() { return this->model_uniforms[DIFFUSE_MAP]; }

protected:

	enum ModelUniforms
	{
		DIFFUSE_MAP,

		NUM_UNIFORMS
	};

	GLuint model_uniforms[NUM_UNIFORMS];


};
