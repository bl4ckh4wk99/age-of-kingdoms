#include "AnimationShader.h"

AnimationShader::AnimationShader(const string & fileName):ModelShader(fileName) {

	for (unsigned int i = 0; i < MAX_BONES; i++) {
		stringstream ss;
		ss << "Bones[" << i << "]";
		string name = ss.str();
		animation_uniforms[i] = glGetUniformLocation(main_program, name.c_str());
	}
}

void AnimationShader::Update(Transform * transform, Camera * camera) {

	ModelShader::Update(transform, camera);
}

void AnimationShader::UpdateBones(int boneIndex, glm::mat4 boneTransform){
	
	assert(boneIndex < MAX_BONES);
	glUniformMatrix4fv(animation_uniforms[boneIndex], 1, GL_TRUE, &boneTransform[0][0]);
}

AnimationShader::~AnimationShader() {
	ModelShader::~ModelShader();
}
