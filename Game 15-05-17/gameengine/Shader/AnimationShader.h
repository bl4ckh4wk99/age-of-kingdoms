#pragma once
#include "ModelShader.h"

#define MAX_BONES 100

class AnimationShader : public ModelShader {

public:

	AnimationShader() {}
	AnimationShader(const string& fileName);

	//void Bind();
	void Update(Transform * transform, Camera * camera);

	void UpdateBones(int boneIndex, glm::mat4 boneTransform);

	virtual ~AnimationShader();

	GLuint getTextureUniform() { return this->model_uniforms[DIFFUSE_MAP]; }

protected:

	GLuint animation_uniforms[MAX_BONES];

};
