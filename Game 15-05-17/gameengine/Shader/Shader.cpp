#include "Shader.h"
#include <fstream>
#include <iostream>


//Lighting Constants
GLfloat Light_Ambient_And_Diffuse[4] = { 0.2, 0.2, 0.2, 1.0 };
GLfloat Light_Specular[4] = { 1.0,1.0,1.0,1.0 };
GLfloat Light_Dir[] = { 0.0f, -0.8f, 0.5f };

Shader::Shader(const string& fileName)
{
	main_program = glCreateProgram(); // Create new program
	main_shaders[0] = CreateShader(LoadShader(fileName + ".vert"), GL_VERTEX_SHADER); // Create a new vertex shader
	main_shaders[1] = CreateShader(LoadShader(fileName + ".frag"), GL_FRAGMENT_SHADER); // Create a new fragment shader

	for (int i = 0;i < NUM_SHADERS;i++)
		glAttachShader(main_program, main_shaders[i]); // Attach shaders to our program

	bool failed = false;

	glLinkProgram(main_program); // Link the program
	failed = CheckShaderError(main_program, GL_LINK_STATUS, true, "Error: Shader failed to link with program."); // Check for shader link errors

	glValidateProgram(main_program); // Validate the program
	failed = CheckShaderError(main_program, GL_VALIDATE_STATUS, true, "Error: Validation of program failed."); // Check for shader validation errors

	if (!failed)
		cout << "Shader: "<< fileName << " Loaded Succesfully" << endl;

	main_uniforms[PROJECT_U] = glGetUniformLocation(main_program, "ProjectionMatrix"); // Figure out what we should apply our uniform transform to (transform that is applied to all vertices)

	main_uniforms[MODEL_U] = glGetUniformLocation(main_program, "ModelMatrix");

	main_uniforms[VIEW_U] = glGetUniformLocation(main_program, "ViewMatrix");

	main_uniforms[NORMAL_U] = glGetUniformLocation(main_program, "NormalMatrix");

	openGLErrorCheck(__FILE__, __LINE__);
}

Shader::~Shader()
{
	for (int i = 0;i < NUM_SHADERS;i++) // Unbind and delete shaders
	{
		glDetachShader(main_program, main_shaders[i]);
		glDeleteShader(main_shaders[i]);
	}
	glDeleteProgram(main_program); // Remove program
}

void Shader::Bind()
{
	glUseProgram(main_program); // Bind the shader to the program
	openGLErrorCheck(__FILE__, __LINE__);
}

void Shader::Update(Transform* transform, Camera* camera){

	glm::mat4 ProjMat = camera->getProjectionMatrix();
	glUniformMatrix4fv(main_uniforms[PROJECT_U], 1, GL_FALSE, &ProjMat[0][0]);

	glm::mat4 ModMat = transform->getModel();
	glUniformMatrix4fv(main_uniforms[MODEL_U], 1, GL_FALSE, &ModMat[0][0]);

	glm::mat4 ViewMat = glm::mat4(camera->getView());
	glUniformMatrix4fv(main_uniforms[VIEW_U], 1, GL_FALSE, &ViewMat[0][0]);

	glm::mat3 normalMatix = transform->getNormal();
	glUniformMatrix3fv(main_uniforms[NORMAL_U], 1, GL_FALSE, &normalMatix[0][0]);

	openGLErrorCheck(__FILE__, __LINE__);
}

void Shader::UpdateSkyBox(Camera * camera){
	glm::mat4 ProjMat = camera->getProjectionMatrix();
	glUniformMatrix4fv(main_uniforms[PROJECT_U], 1, GL_FALSE, &ProjMat[0][0]);

	glm::mat4 ViewMat = glm::mat4(glm::mat3(camera->getView()));
	glUniformMatrix4fv(main_uniforms[VIEW_U], 1, GL_FALSE, &ViewMat[0][0]);
}

GLuint Shader::CreateShader(const string& input, GLenum type)
{
	GLuint shader = glCreateShader(type);
	openGLErrorCheck(__FILE__, __LINE__);

	if (shader == 0)
		cerr << "Error: Failed to create shader." << endl;

	const GLchar* shaderStringList[1];
	GLint shaderStringListLength[1];

	shaderStringList[0] = input.c_str();
	shaderStringListLength[0] = input.length();

	glShaderSource(shader, 1, shaderStringList, shaderStringListLength);
	glCompileShader(shader); // Build shader

	CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error: Shader failed to compile."); // Check for shader compilation errors

	return shader;
}

string Shader::LoadShader(const string& fileName)
{
	ifstream file;
	file.open((fileName).c_str());

	string output, line;
	
	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
		cerr << "Unable to load shader: " << fileName << endl;
	return output;
}

bool Shader::CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const string& errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		cerr << errorMessage << ": '" << error << "'" << endl;

		return true;

	}
	return false;
}





