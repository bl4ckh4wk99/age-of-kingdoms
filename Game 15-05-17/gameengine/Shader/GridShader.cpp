#include "GridShader.h"

GridShader::GridShader(const string & fileName) {

	main_program = glCreateProgram(); // Create new program
	main_shaders[0] = CreateShader(LoadShader(fileName + ".vert"), GL_VERTEX_SHADER); // Create a new vertex shader
	main_shaders[1] = CreateShader(LoadShader(fileName + ".frag"), GL_FRAGMENT_SHADER); // Create a new fragment shader

	for (int i = 0;i < NUM_SHADERS;i++)
		glAttachShader(main_program, main_shaders[i]); // Attach shaders to our program

	bool failed = false;

	glLinkProgram(main_program); // Link the program
	failed = CheckShaderError(main_program, GL_LINK_STATUS, true, "Error: Shader failed to link with program."); // Check for shader link errors

	glValidateProgram(main_program); // Validate the program
	failed = CheckShaderError(main_program, GL_VALIDATE_STATUS, true, "Error: Validation of program failed."); // Check for shader validation errors

	if (!failed)
		cout << "Shader: " << fileName << " Loaded Succesfully" << endl;

	main_uniforms[PROJECT_U] = glGetUniformLocation(main_program, "ProjectionMatrix"); // Figure out what we should apply our uniform transform to (transform that is applied to all vertices)

	main_uniforms[MODEL_U] = glGetUniformLocation(main_program, "ModelMatrix");

	main_uniforms[VIEW_U] = glGetUniformLocation(main_program, "ViewMatrix");
}

//void ModelShader::Bind(){
//
//}

void GridShader::Update(Camera * camera) {

	glm::mat4 ProjMat = camera->getProjectionMatrix();
	glUniformMatrix4fv(main_uniforms[PROJECT_U], 1, GL_FALSE, &ProjMat[0][0]);

	glm::mat4 ModMat = glm::mat4(1.0);
	glUniformMatrix4fv(main_uniforms[MODEL_U], 1, GL_FALSE, &ModMat[0][0]);

	glm::mat4 ViewMat = glm::mat4(camera->getView());
	glUniformMatrix4fv(main_uniforms[VIEW_U], 1, GL_FALSE, &ViewMat[0][0]);

	openGLErrorCheck(__FILE__, __LINE__);
}


GridShader::~GridShader() {
	for (int i = 0;i < NUM_SHADERS;i++) // Unbind and delete shaders
	{
		glDetachShader(main_program, main_shaders[i]);
		glDeleteShader(main_shaders[i]);
	}
	glDeleteProgram(main_program); // Remove program
}
