#include "ModelShader.h"

ModelShader::ModelShader(const string & fileName):Shader(fileName){

	model_uniforms[DIFFUSE_MAP] = glGetUniformLocation(main_program, "DiffuseMap");
}


void ModelShader::Update(Transform * transform, Camera * camera){
	Shader::Update(transform, camera);
}

void ModelShader::UpdateStatic(Camera* camera) {

	glm::mat4 ProjMat = camera->getProjectionMatrix();
	glUniformMatrix4fv(main_uniforms[PROJECT_U], 1, GL_FALSE, &ProjMat[0][0]);

	glm::mat4 ModMat = glm::mat4(1.0);
	glUniformMatrix4fv(main_uniforms[MODEL_U], 1, GL_FALSE, &ModMat[0][0]);

	glm::mat4 ViewMat = glm::mat4(camera->getView());
	glUniformMatrix4fv(main_uniforms[VIEW_U], 1, GL_FALSE, &ViewMat[0][0]);

	glm::mat3 normalMatix = glm::mat3(1.0);
	glUniformMatrix3fv(main_uniforms[NORMAL_U], 1, GL_FALSE, &normalMatix[0][0]);

	openGLErrorCheck(__FILE__, __LINE__);
}

ModelShader::~ModelShader(){
	Shader::~Shader();
}
