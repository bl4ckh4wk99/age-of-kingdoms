#pragma once
#include "Shader.h"

class FontShader : public Shader {

public:

	FontShader() {}

	FontShader(const std::string& filename);

	void setTextureLocation(unsigned int loc);

private:
	GLuint m_textureLocation;

};