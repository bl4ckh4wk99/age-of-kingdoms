#include "FontShader.h"

FontShader::FontShader(const std::string & filename):Shader(filename)
{
	m_textureLocation = glGetUniformLocation(main_program, "gSampler");
}

void FontShader::setTextureLocation(unsigned int loc)
{
	glUniform1i(m_textureLocation, loc);
}
