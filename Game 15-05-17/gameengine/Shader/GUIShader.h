#pragma once
#include "Shader.h"


class GUIShader : public Shader {

public:

	GUIShader() {}
	GUIShader(const string& fileName);

	//void Bind();
	void Update(int isSelected);

	void UpdateHealthBar(glm::vec3 colour);

	virtual ~GUIShader();

	GLuint getTextureUniform() { return this->gui_uniforms[DIFFUSE_MAP]; }

protected:

	enum GuiUniforms
	{
		COLOUR,

		IS_SELECTED,

		DIFFUSE_MAP,

		NUM_UNIFORMS
	};

	GLuint gui_uniforms[NUM_UNIFORMS];


};