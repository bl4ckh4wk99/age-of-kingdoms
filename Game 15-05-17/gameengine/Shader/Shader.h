#pragma once
using namespace std;
#include <sstream>
#include <string>
#include <iostream>
#include<gl\glew.h>
#include "../GameObjects/Transform.h"
#include "../GameObjects/Camera.h"
#include "../OpenGLHelper.h"

class Shader
{
public:
	Shader() {}
	Shader(const string& fileName);

	void Bind();
	void Update(Transform* transform, Camera* camera);
	void UpdateSkyBox(Camera* camera);

	virtual ~Shader();

protected:
	static const unsigned int NUM_SHADERS = 2;
	Shader(const Shader& o) {}
	void operator*(const Shader& o) {}

	static bool CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const string& errorMessage);
	static string LoadShader(const string& fileName);
	static GLuint CreateShader(const string& input, GLenum type);

	enum ShaderUniforms
	{
		PROJECT_U,

		MODEL_U,

		VIEW_U,

		NORMAL_U,

		NUM_UNIFORMS
	};

	GLuint main_program;
	GLuint main_shaders[NUM_SHADERS];
	GLuint main_uniforms[NUM_UNIFORMS];

	//GLuint bone_uniforms[MAX_BONES];
};

