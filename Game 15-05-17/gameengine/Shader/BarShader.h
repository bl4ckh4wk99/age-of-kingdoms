#pragma once

#include "Shader.h"

class BarShader : public Shader {

public:

	BarShader() {}
	BarShader(const string& fileName);

	//void Bind();
	void Update(Transform* transform, Camera* camera);

	virtual ~BarShader();

protected:


};