#pragma once
#include "ModelShader.h"


class BuildingShader : public ModelShader {

public:

	BuildingShader() {}
	BuildingShader(const string& fileName);

	//void Bind();
	void Update(float buildingAlpha, Transform* transform, Camera* camera);

	virtual ~BuildingShader();

protected:

	enum BuildUniforms
	{
		BUILDING_ALPHA,

		NUM_UNIFORMS
	};

	GLuint build_uniforms[NUM_UNIFORMS];


};
