#include "DisplayWindow.h"



DisplayWindow::DisplayWindow(){
	string name("Default");
	int width = 800;
	int height = 600;

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO); // Initialise SDL


	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8); // Initialise SDL colour bit attributes
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16); // Setup zBuffer depth
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32); // Initialise SDL buffer value
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // Initialise a double buffer

	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		//success = false;
	}


	main_Window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL /*| SDL_WINDOW_FULLSCREEN*/ | SDL_WINDOW_INPUT_GRABBED);
	main_glContext = SDL_GL_CreateContext(main_Window);

	GLenum windowStatus = glewInit(); // Initialise glew

	if (windowStatus != GLEW_OK) // Check for glew errors
		cerr << "Glew failed to initialise in DisplayWindow.cpp." << endl;

	cout << "GLEW version: " << glewGetString(GLEW_VERSION) << endl;

	//checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << "Open GL Version: " << OpenGLVersion[0] << "." << OpenGLVersion[1] << endl;

	finished = false; // Make sure we won't terminate instantly

	// Enable zBuffer
	glEnable(GL_DEPTH_TEST);
}

DisplayWindow::DisplayWindow(int width, int height, const string& name)
{
	screenWidth = width;
	screenHeight = height;

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO); // Initialise SDL


	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8); // Initialise SDL colour bit attributes
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16); // Setup zBuffer depth
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32); // Initialise SDL buffer value
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // Initialise a double buffer

	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		//success = false;
	}


	main_Window = SDL_CreateWindow(name.c_str(),SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL /*| SDL_WINDOW_FULLSCREEN*/ | SDL_WINDOW_INPUT_GRABBED);
	main_glContext = SDL_GL_CreateContext(main_Window);

	GLenum windowStatus = glewInit(); // Initialise glew

	if (windowStatus != GLEW_OK) // Check for glew errors
		cerr << "Glew failed to initialise in DisplayWindow.cpp." << endl;

	cout << "GLEW version: " << glewGetString(GLEW_VERSION) << endl;

	//checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << "Open GL Version: " << OpenGLVersion[0] << "." << OpenGLVersion[1] << endl;

	finished = false; // Make sure we won't terminate instantly

	// Enable zBuffer
	glEnable(GL_DEPTH_TEST);
}

DisplayWindow::~DisplayWindow()
{
	SDL_GL_DeleteContext(main_glContext);
	SDL_DestroyWindow(main_Window);

	SDL_Quit();
}

void DisplayWindow::Update()
{
	SDL_GL_SwapWindow(main_Window); // Swap buffer window

	SDL_Event e;

	while (SDL_PollEvent(&e)) // Check for termination of program
	{
		cout << "in display while loop" << endl;
		if (e.type == SDL_QUIT)
			finished = true;
	}

}

bool DisplayWindow::isFinished()
{
	return finished;
}

void DisplayWindow::ClearColour(float a, float b, float c, float d)
{
	glClearColor(a,b,c,d);
	glClear(GL_COLOR_BUFFER_BIT); // Clear the colour buffer
	glClear(GL_DEPTH_BUFFER_BIT); // Clear the zBuffer


}

void DisplayWindow::setWindowTitle(const char * name)
{

	SDL_SetWindowTitle(this->main_Window, name);
}

