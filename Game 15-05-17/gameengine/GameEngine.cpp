#include "GameEngine.h"

using namespace std;

//Program Variables
SessionHandler sessionHandler;
DisplayWindow* display;

#define WIDTH 1920
#define HEIGHT 1080

//DeltaT using c++
double previousTime;
int frame_accum = 0;
double startTime = 0.0f;
int fps = 0;

//KeyboardInput keyboard(true);
SDL_Event event;
const Uint8* keyState = SDL_GetKeyboardState(NULL);
bool isFinished = false;

glm::vec2 mouse, screen;


int main(int argc, char** argv)
{
	// Setup OpenGL
	SDL_PumpEvents();
	display = new DisplayWindow(WIDTH, HEIGHT, "Game Engine"); // Create window
	screen = glm::vec2(display->screenWidth, display->screenHeight);

	sessionHandler.Init(display);

	while (!isFinished)
	{
		//**************************DELTAT USING C++*********************************
		// Calculate delta time
		double currentTime = GetTickCount();
		double deltaT = (currentTime - previousTime) / 1000.0f;
		if (deltaT > 0.016) {
			deltaT = 0.016;
		}
		previousTime = currentTime;

		frame_accum++;
		if (currentTime - startTime >= 1000.0f)
		{
			stringstream title;
			title << "Game Engine - FPS [" << frame_accum << "]";
			display->setWindowTitle(title.str().c_str());
			startTime = currentTime;
			frame_accum = 0;
		}

		sessionHandler.Reshape(display->screenWidth, display->screenHeight);

		sessionHandler.Update(deltaT);
		sessionHandler.Draw();


		glFlush();

		// Obtain mouse coordinates
		int x = 0; int y = 0;
		SDL_GetMouseState(&x, &y);
		mouse = glm::vec2(x, y);

		while (SDL_PollEvent(&event) != 0) //Check for mouse events
		{
			if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
				SDL_KeyboardEvent *kb = (SDL_KeyboardEvent*)&event.key;
				isFinished = sessionHandler.KeyboardControl(kb, deltaT);
			}
			sessionHandler.MouseControl(event, mouse, screen, deltaT);
		}

		SDL_GL_SwapWindow(display->main_Window); // Swap buffer window
	}
	return 0;
}





