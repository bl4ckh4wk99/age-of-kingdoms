#pragma once

#pragma once

#include "../Shader/ModelShader.h"
#include "../Shader/AnimationShader.h"
#include "../Shader/BuildingShader.h"
#include "../Shader/GUIShader.h"
#include "../Shader/FontShader.h"
#include "../Shader/BarShader.h"
#include "../Shader/GridShader.h"

#include "Team.h"

#include "../ObjectLoading/Model.h"
#include "../ObjectLoading/Animated/AnimatedModel.h"

#include "GUI\GUI Primitives\Texture.h"
#include "GUI\GUI Primitives\Font.h"

#include <SDL2\SDL_mixer.h>

#include <vector>
#include <time.h>       /* time */

class AssetLibrary {

public:

	AssetLibrary(glm::vec2 screenDim);

	/**************************************SHADERS*******************************************/

	inline Shader* getCubeMapShader() { return this->cubeMapShader; }
	inline ModelShader* getModelShader() { return this->modelShader; }

	inline AnimationShader* getAnimatedShader() { return this->animatedShader; }
	inline BuildingShader* getBuildingShader() { return this->buildingShader; }
	inline GUIShader* getGUIShader() { return this->guiShader; }
	inline GUIShader* getGUILineShader() { return this->guiLineShader; }
	inline FontShader* getFontShader() { return this->fontShader; }
	inline BarShader* getBarShader() { return this->barShader; }
	inline GridShader* getGridShader() { return this->gridShader; }


	/***************************************MODELS*******************************************/

	inline Model* getGameMap() { return &this->gameMap; }

	//inline AnimatedModel* getArcher() { return &this->archer; }
	//inline AnimatedModel* getBuilder() { return &this->builder; }
	//inline AnimatedModel* getSword() { return &this->swordsman; }
	inline Model* getArcher() { return &this->archer; }
	inline Model* getArrow() { return &this->arrow; }
	inline Model* getBuilder() { return &this->builder; }
	inline Model* getSword() { return &this->swordsman; }

	inline Model* getThrone(Team team) { return (team == ONE) ? &this->Throne_1 : &this->Throne_2;}
	inline Model* getMine(Team team) { return (team == ONE) ? &this->Mine_1 : &this->Mine_2; }
	inline Model* getBarracks(Team team) { return (team == ONE) ? &this->Barracks_1 : &this->Barracks_2; }

	/***************************************GUI STUFF*******************************************/

	inline Texture* getResourceBackgroundTex() { return this->resourceBackgroundTex; }
	inline Texture* getSelectionBackgroundTex() {return this->selectionBackgroundTex; }
	inline Texture* getGoldIconTex(){ return this->goldIconTex; };
	inline Texture* getPopIconTex() { return this->popIconTex; }

	inline std::vector<Texture*>* getUnitIcons() { return &this->unitIcons; }
	inline std::vector<Texture*>* getBuildIcons() { return &this->buildIcons; }

	inline Texture* getMenuBackgroundTex() { return this->menuBackgroundTex; }
	inline Texture* getMenuPvAITex() { return this->menuPvAITex; }
	inline Texture* getMenuAIvAITex() { return this->menuAIvAITex; }

	inline Texture* getBlankMenuBackgroundTex() { return this->blankBackgroundTex; }
	inline Texture* getMainMenuTex() { return this->mainMenuTex; }

	inline Font* getFont() { return &this->font; }
	inline glm::vec2 getScreenDim() { return this->screenDim; }

	/***************************************GAME SOUNDS*******************************************/

	inline void playGameMusic() { Mix_FadeInMusic(gameMusic, 10, 1000); Mix_VolumeMusic(60); }
	inline void playMenuMusic() { Mix_FadeInMusic(menuMusic, 10, 1000); Mix_VolumeMusic(80);
	}
	inline void stopMusic() { Mix_FadeOutMusic(1000); }

	inline void playVillagerSound() { Mix_PlayChannel(-1, villagerSound, 0); }
	inline void playMilitarySound() { Mix_PlayChannel(-1, militarySound, 0); }

	inline void playUnitSelectMale() {
		int random = rand() % 2 + 1;
		switch (random) {
		case 1:
			Mix_PlayChannel(-1, selectMale1, 0);
			break;
		case 2:
			Mix_PlayChannel(-1, selectMale2, 0);
			break;
		}
	}
	inline void playUnitSelectFemale() {
		int random = rand() % 2 + 1;
		switch (random) {
		case 1:
			Mix_PlayChannel(-1, selectFemale1, 0);
			break;
		case 2:
			Mix_PlayChannel(-1, selectFemale2, 0);
			break;
		}
	}

	inline void playArcherAction(){ Mix_PlayChannel(-1, archerAction, 0); }
	inline void playSwordAction() { Mix_PlayChannel(-1, swordAction, 0); }
	inline void playBuilderAction() { Mix_PlayChannel(-1, builderAction, 0); }
	inline void playBuilderBuild(){ Mix_PlayChannel(-1, builderBuild, 0); }

	inline void playBarracksSound() { Mix_PlayChannel(-1, barracksSound, 0); }
	inline void playMineSound() { Mix_PlayChannel(-1, mineSound, 0); }
	inline void playThroneSound() { Mix_PlayChannel(-1, throneSound, 0); }

	inline void playArcherDeathSound() { int random = rand() % 3 + 1;
	switch (random) {
	case 1:
		Mix_PlayChannel(-1, archerDeath1, 0);
		break;
	case 2:
		Mix_PlayChannel(-1, archerDeath2, 0);
		break;
	case 3:
		Mix_PlayChannel(-1, archerDeath3, 0);
		break;
	} }

	inline void playManDeathSound() {
		int random = rand() % 3 + 1;
		switch (random) {
		case 1:
			Mix_PlayChannel(-1, swordDeath1, 0);
			break;
		case 2:
			Mix_PlayChannel(-1, swordDeath2, 0);
			break;
		case 3:
			Mix_PlayChannel(-1, swordDeath3, 0);
			break;
		}
	}

	void playBuildingDeath(){ Mix_PlayChannel(-1, buildingDeath, 0); }

	void inline playSwordAttack(){ Mix_PlayChannel(-1, swordAttack, 0); }
	void inline playArcherAttack(){ Mix_PlayChannel(-1, archerAttack, 0); }


private:
	/**************************************SHADERS*******************************************/

	Shader* cubeMapShader;
	ModelShader* modelShader;

	AnimationShader* animatedShader;
	BuildingShader* buildingShader;
	GUIShader* guiShader;
	GUIShader* guiLineShader;
	FontShader* fontShader;
	BarShader* barShader;
	GridShader* gridShader;

	/***************************************MODELS*******************************************/
	Model gameMap;

	Model Barracks_1, Barracks_2;

	Model Mine_1, Mine_2;

	Model Throne_1, Throne_2;

	Model archer;
	Model arrow;
	Model builder;
	Model swordsman;


	/***************************************GUI STUFF*******************************************/
	Texture* resourceBackgroundTex;
	Texture* selectionBackgroundTex;
	Texture* goldIconTex;
	Texture* popIconTex;

	std::vector<Texture*> unitIcons;
	std::vector<Texture*> buildIcons;

	Texture* menuBackgroundTex;
	Texture* menuPvAITex;
	Texture* menuAIvAITex;

	Texture* blankBackgroundTex;
	Texture* mainMenuTex;

	Font font;
	glm::vec2 screenDim;

	/***************************************GAME SOUNDS*******************************************/

	//The music that will be played
	Mix_Music *gameMusic = NULL;
	Mix_Music *menuMusic = NULL;

	//The sound effects that will be used
	Mix_Chunk *villagerSound = NULL;
	Mix_Chunk *militarySound = NULL;

	Mix_Chunk *selectMale1 = NULL;
	Mix_Chunk *selectMale2 = NULL;
	Mix_Chunk *selectFemale1 = NULL;
	Mix_Chunk *selectFemale2 = NULL;

	Mix_Chunk *mineSound = NULL;
	Mix_Chunk *barracksSound = NULL;
	Mix_Chunk *throneSound = NULL;

	Mix_Chunk *archerAttack = NULL;
	Mix_Chunk *archerAction = NULL;
	Mix_Chunk *archerDeath1 = NULL;
	Mix_Chunk *archerDeath2 = NULL;
	Mix_Chunk *archerDeath3 = NULL;

	Mix_Chunk *swordAttack = NULL;
	Mix_Chunk *swordAction = NULL;
	Mix_Chunk *swordDeath1 = NULL;
	Mix_Chunk *swordDeath2 = NULL;
	Mix_Chunk *swordDeath3 = NULL;

	Mix_Chunk *builderAction = NULL;
	Mix_Chunk *builderBuild = NULL;

	Mix_Chunk *buildingDeath = NULL;
};

