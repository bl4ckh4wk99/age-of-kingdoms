#pragma once

#include <vector>

#include "../Units/Unit.h"
#include "../Units/Archer.h"
#include "../Units/Builder.h"
#include "../Units/Swordsman.h"

#include "../Buildings/Building.h"
#include "../Buildings/Throne.h"
#include "../Buildings/Barracks.h"
#include "../Buildings/Mine.h"

#include "../AssetLibrary.h"

#include "../Team.h"

static const glm::vec2 PlayerOneStart = glm::vec2(21, 35);
static const glm::vec2 PlayerTwoStart = glm::vec2(21, 5);

#define STARTING_RESOURCES 500
#define POP_CAP 20

class Player {
protected:
	double resourceTimer;

	ModelShader* modelShader;
	BarShader* barShader;

	AssetLibrary* assets;

	AnimationShader* animationShader;
	Model* builder;
	Model* archer;
	Model* swordsman;

	BuildingShader* buildingShader;
	Model* throne;
	Model* mine;
	Model* barracks;

public:

	Player* enemy;

	int resources;
	int income;

	//Game Stats
	int buildingsBuilt;
	int buildingsLost;
	int unitsBuilt;
	int unitsLost;
	int resourcesGained;
	int resourcesSpent;

	Unit* selectedUnit;
	std::vector<Unit*> units;

	Building* selectedBuilding;
	std::vector<Building*> buildings;

	Team team;

	Grid* gameWorld;

	virtual void Init(Player* enemy) = 0;

	virtual void Draw(Camera* cam) = 0;

	virtual void Update(double deltaT) = 0;

	virtual void select(glm::vec2 cell) = 0;

	virtual void action(glm::vec2 cell) = 0;

	Unit* addUnit(glm::vec2 cellPos, UnitType type);

	Building* addBuilding(glm::vec2 cellPos, BuildingType type);

	void checkAndMove(Unit* movingUnit, glm::vec2 targetCell);

	void moveToUnit(Unit* movingUnit, glm::vec2 targetCell);
	void moveToBuilding(Unit* movingUnit, glm::vec2 targetCell);

	void attackUnit(AttackingUnit* movingUnit, glm::vec2 targetCell);
	void attackBuilding(AttackingUnit* movingUnit, glm::vec2 targetCell);

	bool isDead();
};
