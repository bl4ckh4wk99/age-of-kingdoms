#include "AI.h"

AI::AI() {
	createBuildTree();
	createUnitTree();
	createMoveTree();
}

AI::AI(Grid* gameWorld, AssetLibrary* assets, Team team) {
	this->gameWorld = gameWorld;
	this->assets = assets;
	this->animationShader = assets->getAnimatedShader();
	this->buildingShader = assets->getBuildingShader();
	this->modelShader = assets->getModelShader();
	this->barShader = assets->getBarShader();

	archer = assets->getArcher();
	builder = assets->getBuilder();
	swordsman = assets->getSword();

	throne = assets->getThrone(team);
	mine = assets->getMine(team);
	barracks = assets->getBarracks(team);
	
	this->team = team;

	//create behaviour trees
	createBuildTree();
	createUnitTree();
	createMoveTree();
}

void AI::Init(Player* enemy) {
	this->enemy = enemy;
	
	this->resources = STARTING_RESOURCES;
	this->income = 0;
	this->resourceTimer = 0.0;
	this->aiDelay = 0.0;

	unitsBuilt = 0;
	unitsLost = 0;
	buildingsBuilt = 0;
	buildingsLost = 0;
	resourcesGained = STARTING_RESOURCES;
	resourcesSpent = 0;
	 
}

void AI::Draw(Camera* cam) {

	for (Unit* unit : units) {
		unit->Draw(cam);
	}

	for (Building* b : buildings) {
		b->Draw(cam);
	}
}

void AI::Update(double deltaT) {
	//execute behaviour trees after delay
	aiDelay += deltaT;
	if (aiDelay > DELAY_TIME) {
		buildTree.execute(this);
		unitTree.execute(this);
		moveTree.execute(this);
		aiDelay = 0.0;
	}

	Player::Update(deltaT);

	for (Building* building : buildings) {
		building->Update(deltaT);
		if (building->state == BUILDING) {
			if (building->checkBuild())
				income += building->income;
		}
		if (building->state == PRODUCING) {
			if (building->checkProduction((POP_CAP > this->units.size()))) {
				addUnit(building->exitCell(), building->curProduction);
			}
		}
	}
}

void AI::select(glm::vec2 cell) {


}

void AI::action(glm::vec2 cell) {
	units.at(0)->moveTo(cell);
}

void AI::createBuildTree(){

	std::vector<TreeNode<AI*>*> children;

				Condition<AI*>* cond_NoBuilder = new Condition<AI*>(noBuilder);
				Action<AI*>* act_BuildBuilder = new Action<AI*>(buildBuilder);
				children = { cond_NoBuilder, act_BuildBuilder };

			Sequencer<AI*>* seq_Builder = new Sequencer<AI*>(children);

			Condition<AI*>* cond_UnFinished = new Condition<AI*>(unfinishedBuilding);

						Condition<AI*>* cond_StartMines = new Condition<AI*>(starterMines);
						Condition<AI*>* act_BuildMine = new Condition<AI*>(buildMine);
						children = { cond_StartMines, act_BuildMine };

					Sequencer<AI*>* seq_StartMines = new Sequencer<AI*>(children);

						Condition<AI*>* cond_StartBarracks = new Condition<AI*>(starterBarracks);
						Condition<AI*>* act_BuildBarracks = new Condition<AI*>(buildBarracks);
						children = { cond_StartBarracks, act_BuildBarracks };
	
					Sequencer<AI*>* seq_StartBarracks = new Sequencer<AI*>(children);

					children = { seq_StartMines, seq_StartBarracks };

				RandomSelector<AI*>* rSel_LowEco = new RandomSelector<AI*>(children);

				Condition<AI*>* cond_LowEco = new Condition<AI*>(lowEco);

				children = { cond_LowEco, rSel_LowEco };

			Sequencer<AI*>* seq_LowEco = new Sequencer<AI*>(children);

				Condition<AI*>* act_BuildThrone = new Condition<AI*>(buildThrone);
				children = { act_BuildThrone };

			Sequencer<AI*>* seq_Throne = new Sequencer<AI*>(children);

						Condition<AI*>* cond_MedMines = new Condition<AI*>(medMines);
						children = { cond_MedMines, act_BuildMine };
					
					Sequencer<AI*>* seq_MedMines = new Sequencer<AI*>(children);

						Condition<AI*>* cond_MedBarracks = new Condition<AI*>(medBarracks);
						children = { cond_MedBarracks, act_BuildBarracks };

					Sequencer<AI*>* seq_MedBarracks = new Sequencer<AI*>(children);
					children = { seq_MedMines, seq_MedBarracks };

				RandomSelector<AI*>* rSel_MedEco = new RandomSelector<AI*>(children);

				Condition<AI*>* cond_MedEco = new Condition<AI*>(medEco);
				children = { cond_MedEco, rSel_MedEco };

			Sequencer<AI*>* seq_MedEco = new Sequencer<AI*>(children);

						Condition<AI*>* cond_LateMines = new Condition<AI*>(highMines);
						children = { cond_LateMines, act_BuildMine };

					Sequencer<AI*>* seq_LateMines = new Sequencer<AI*>(children);

						Condition<AI*>* cond_LateBarracks = new Condition<AI*>(highBarracks);
						children = { cond_LateBarracks, act_BuildBarracks };

					Sequencer<AI*>* seq_LateBarracks = new Sequencer<AI*>(children);
					children = { seq_LateMines, seq_LateBarracks };

				RandomSelector<AI*>* rSel_LateEco = new RandomSelector<AI*>(children);

				Condition<AI*>* cond_LateEco = new Condition<AI*>(highEco);
				children = { cond_LateEco, rSel_LateEco };

			Sequencer<AI*>* seq_LateEco = new Sequencer<AI*>(children);
		
			children = { seq_Builder, cond_UnFinished, seq_LowEco, seq_Throne, seq_MedEco, seq_LateEco };

		Selector<AI*>* sel_Build = new Selector<AI*>(children);

		Condition<AI*>* cond_HaveMoney = new Condition<AI*>(haveMoney);
		children = { cond_HaveMoney, sel_Build };

	Sequencer<AI*>* seq_BuildTree = new Sequencer<AI*>(children);

	buildTree = BTree<AI*>(seq_BuildTree);

}

void AI::createUnitTree(){
	std::vector<TreeNode<AI*>*> children;

				Condition<AI*>* cond_LowEco = new Condition<AI*>(lowEco);
				Condition<AI*>* cond_LowArch = new Condition<AI*>(starterArch);
				Condition<AI*>* act_BuildArcher = new Condition<AI*>(buildArch);
				children = { cond_LowEco, cond_LowArch, act_BuildArcher };

			Sequencer<AI*>* seq_LowEco = new Sequencer<AI*>(children);

							Condition<AI*>* cond_MedArch = new Condition<AI*>(medArch);
							children = { cond_MedArch, act_BuildArcher };
						
						Sequencer<AI*>* seq_MedArch = new Sequencer<AI*>(children);

							Condition<AI*>* cond_MedSword = new Condition<AI*>(medSwords);
							Condition<AI*>* act_BuildSwords = new Condition<AI*>(buildSwords);
							children = { cond_MedSword, act_BuildSwords };

						Sequencer<AI*>* seq_MedSword = new Sequencer<AI*>(children);

						children = { seq_MedArch, seq_MedSword };

					RandomSelector<AI*>* rSel_MedUnits = new RandomSelector<AI*>(children);

					Condition<AI*>* cond_MedUnits = new Condition<AI*>(medUnits);

					children = { cond_MedUnits, rSel_MedUnits };

				Selector<AI*>* sel_MedUnits = new Selector<AI*>(children);

				Condition<AI*>* cond_MedEco = new Condition<AI*>(medEco);

				children = { cond_MedEco, sel_MedUnits };

			Sequencer<AI*>* seq_MedEco = new Sequencer<AI*>(children);

							Condition<AI*>* cond_LateArch = new Condition<AI*>(highArch);
							children = { cond_LateArch, act_BuildArcher };

						Sequencer<AI*>* seq_LateArch = new Sequencer<AI*>(children);

							Condition<AI*>* cond_LateSword = new Condition<AI*>(highSwords);
							children = { cond_LateSword, act_BuildSwords };

						Sequencer<AI*>* seq_LateSword = new Sequencer<AI*>(children);

						children = { seq_LateArch, seq_LateSword };

					RandomSelector<AI*>* rSel_LateUnits = new RandomSelector<AI*>(children);

					Condition<AI*>* cond_LateUnits = new Condition<AI*>(highUnits);

					children = { cond_LateUnits, rSel_LateUnits };

				Selector<AI*>* sel_LateUnits = new Selector<AI*>(children);

				Condition<AI*>* cond_LateEco = new Condition<AI*>(highEco);

				children = { cond_LateEco, sel_LateUnits };

			Sequencer<AI*>* seq_LateEco = new Sequencer<AI*>(children);

			children = { seq_LowEco, seq_MedEco, seq_LateEco };

		Selector<AI*>* sel_Units = new Selector<AI*>(children);

		Condition<AI*>* cond_HaveBarracks = new Condition<AI*>(haveBarracks);
		Condition<AI*>* cond_HaveMoney = new Condition<AI*>(haveMoney);

		children = { cond_HaveBarracks, cond_HaveMoney, sel_Units };

	Sequencer<AI*>* seq_Units = new Sequencer<AI*>(children);

	unitTree = BTree<AI*>(seq_Units);
}

void AI::createMoveTree(){

	std::vector<TreeNode<AI*>*> children;
				
			Condition<AI*>* cond_EnemyVisible = new Condition<AI*>(enemyVisible);
			Action<AI*>* act_MoveClosest = new Action<AI*>(moveClosest);

			children = { cond_EnemyVisible, act_MoveClosest };

		Sequencer<AI*>* seq_EnemeyVisible = new Sequencer<AI*>(children);

			Condition<AI*>* cond_moreThan3 = new Condition<AI*>(moreThan3Units);
			Action<AI*>* act_Patrol = new Action<AI*>(patrol);

			children = { cond_moreThan3, act_Patrol };

		Sequencer<AI*>* seq_Patrol = new Sequencer<AI*>(children);

			Condition<AI*>* cond_moreThan6 = new Condition<AI*>(moreThan6Units);
			Action<AI*>* act_Explore = new Action<AI*>(explore);

			children = { cond_moreThan6, act_Explore };

		Sequencer<AI*>* seq_Explore = new Sequencer<AI*>(children);

			Condition<AI*>* cond_moreThan12 = new Condition<AI*>(moreThan10Units);
			Action<AI*>* act_AttackBase = new Action<AI*>(attackBase);

			children = { cond_moreThan12, act_AttackBase };

		Sequencer<AI*>* seq_AttackBase = new Sequencer<AI*>(children);

		children = { seq_EnemeyVisible, seq_Patrol, seq_Explore, seq_AttackBase };

	TryAllRoot<AI*>* root_MoveUnits = new TryAllRoot<AI*>(children);

	moveTree = BTree<AI*>(root_MoveUnits);

}

/********************************Behaviour Tree***********************************/

//do we have money
bool haveMoney(AI* player) {
	return player->resources > 0;
}

/********************************Build Tree***********************************/

//do we have a builder busy or not
bool noBuilder(AI* player) {
	for (Unit* u : player->units) {
		if (u->unitType == BUILDER)
			return false;
	}
	return true;
}

//build a builder at the throne
void buildBuilder(AI* player) {
	if (player->resources >= UNIT_COSTS[BUILDER] && player->units.size() < POP_CAP) {
		for (Building* b : player->buildings) {
			if (b->type == THRONE && b->state == BUILT) {
				if (player->resources >= UNIT_COSTS[BUILDER]) {
					b->startProduction(BUILDER);
					player->unitsBuilt++;
					player->resources -= UNIT_COSTS[BUILDER];
					player->resourcesSpent += UNIT_COSTS[BUILDER];
				}
			}
		}
	}
}

//is there an unfinished building we need to complete
bool unfinishedBuilding(AI * player){

	for (Building* b : player->buildings) {
		if (b->state == BUILDING) {
			bool beingBuilt = false;
			for (Unit* u : player->units) {
				if (u->unitType == BUILDER && u->targetBuilding == b)
					beingBuilt = true;
			}
			if (!beingBuilt) {
				for (Unit* u : player->units) {
					if (u->unitType == BUILDER && u->state == IDLE) {
						u->newTarget(b);
						u->targetCell = b->cellPos;
						return true;
					}
				}
			}

		}
	}
	return false;
}

//calcs a random location and builds a building
void build(AI* player, Unit* builder, BuildingType type) {
	
	glm::vec2 basePos;

	glm::vec2 buildPos;

	if (player->team == ONE)
		basePos = PlayerOneStart;
	else
		basePos = PlayerTwoStart;


	switch (type)
	{
	case THRONE: {
		int tries = 0;
		float range = 5.0f;
		while (tries < 20) {
			glm::vec2 min = basePos - range;
			Grid::restrictPoint(min);
			glm::vec2 max = basePos + range;
			Grid::restrictPoint(max);

			// obtain a seed from the system clock:
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937 rng(seed);
			std::uniform_int_distribution<int> genX((int)min.x, (int)max.x); // uniform, unbiased
			std::uniform_int_distribution<int> genY((int)min.y, (int)max.y);
			int x = genX(rng);
			int y = genY(rng);

			if (player->gameWorld->availableToBuild(glm::vec2(x, y), BUILDING_SIZES[THRONE])) {
				buildPos = glm::vec2(x, y);
				break;
			}
			else {
				tries++;
				if (tries == 30) {
					if(range < 10.0f)
						range += 2.0f;
					tries = 0;
				}
			}
		}
		break;
	}
	case MINE: {
		int tries = 0;
		float range = 10.0f;
		while (tries < 5) {
			glm::vec2 min = basePos - range;
			Grid::restrictPoint(min);
			glm::vec2 max = basePos + range;
			Grid::restrictPoint(max);

			// obtain a seed from the system clock:
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937 rng(seed);
			std::uniform_int_distribution<int> genX(min.x, max.x); // uniform, unbiased
			std::uniform_int_distribution<int> genY(min.y, max.y);
			int x = genX(rng);
			int y = genY(rng);

			if (player->gameWorld->availableToBuild(glm::vec2(x, y), BUILDING_SIZES[MINE])) {
				buildPos = glm::vec2(x, y);
				break;
			}
			else {
				tries++;
				if (tries == 10) {
					if (range < 20.0f)
						range += 2.0f;
					tries = 0;
				}
			}
		}
		break;
	}
	case BARRACKS: {
		int tries = 0;
		float range = 7.0f;
		while (tries < 10) {
			glm::vec2 min = basePos - range;
			Grid::restrictPoint(min);
			glm::vec2 max = basePos + range;
			Grid::restrictPoint(max);

			// obtain a seed from the system clock:
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937 rng(seed);
			std::uniform_int_distribution<int> genX((int)min.x, (int)max.x); // uniform, unbiased
			std::uniform_int_distribution<int> genY((int)min.y, (int)max.y);
			int x = genX(rng);
			int y = genY(rng);

			if (player->gameWorld->availableToBuild(glm::vec2(x, y), BUILDING_SIZES[BARRACKS])) {
				buildPos = glm::vec2(x, y);
				break;
			}
			else {
				tries++;
				if (tries == 30) {
					if (range < 20.0f)
						range += 2.0f;
					tries = 0;
				}
			}
		}

		break;
	}
	}

	if (buildPos != glm::vec2(0, 0)) {

		Building* newBuild = player->addBuilding(buildPos, type);
		newBuild->startBuilding();
		player->buildingsBuilt++;
		player->resources -= newBuild->cost;
		player->resourcesSpent += newBuild->cost;

		//move selectunit (builder) to build location and deselect
		player->checkAndMove(builder, newBuild->cellPos);
		builder->targetBuilding = newBuild;
	}

}

/**********************Low Econ**************************/

//early game economy is an income less than 35  with at least 1 barracks
bool lowEco(AI* player){
	if (player->income < (MINE_INCOME * 4 + THRONE_INCOME) || !haveBarracks(player)) {
		return true;
	}
	return false;
}

//if we have 4 or more mines then our starter mines are done
bool starterMines(AI* player){
	int numMines = 0;
	for (Building* b : player->buildings) {
		if (b->type == MINE)
			numMines++;
	}
	return numMines < 4;
}

//choose a random loc to build a mine and get a free builder to build it
bool buildMine(AI* player) {
	for (Unit* u : player->units) {
		if (u->unitType == BUILDER && u->state == IDLE) {
			player->selectedUnit = u;
			if (player->resources >= BUILDING_COSTS[MINE]) {
				build(player, u, MINE);
				player->selectedUnit = nullptr;
				return true;
			}
			player->selectedUnit = nullptr;
		}
	}
	return false;
}

//if we have a barrcks return false, else return true
bool starterBarracks(AI* player){
	return !haveBarracks(player);
}

//choose a random loc to build a barracks
bool buildBarracks(AI* player) {
	for (Unit* u : player->units) {
		if (u->unitType == BUILDER && u->state == IDLE) {
			player->selectedUnit = u;
			if (player->resources >= BUILDING_COSTS[BARRACKS]) {
				build(player, u, BARRACKS);
				player->selectedUnit = nullptr;
				return true;
			}
			player->selectedUnit = nullptr;
		}
	}
	return false;
}

//do we have a throne if not build a new one
bool buildThrone(AI* player){
	//if we have a throne return false
	for (Building* b : player->buildings) {
		if (b->type == THRONE) {
			return false;
		}
	}
	//if we dont find a free builder and build one
	for (Unit* u : player->units) {
		if (u->unitType == BUILDER && u->state == IDLE) {
			if (player->resources >= BUILDING_COSTS[THRONE]) {
				build(player, u, THRONE);
				return true;
			}
			//dont have the resources
			else
				return false;
		}
	}	
	//return false if we didnt have a free builder
	return false;
}


/**********************Med Econ**************************/

//mid game economy is an income between 35 and 55 with at least 1 barracks
bool medEco(AI* player){
	if (player->income >= (MINE_INCOME * 4 + THRONE_INCOME) && player->income < (MINE_INCOME * 6 + THRONE_INCOME) && haveBarracks(player)) {
		//cout << "Mid-Game Economy" << endl;
		return true;
	}
	return false;
}

//if we have 8 or more mines then our mid mines are done
bool medMines(AI* player){
	int numMines = 0;
	for (Building* b : player->buildings) {
		if (b->type == MINE)
			numMines++;
	}
	return numMines < 8;
}

//if we have a barrcks then we dont need another yet
bool medBarracks(AI* player){
	int numBarracks = 0;
	for (Building* b : player->buildings) {
		if (b->type == BARRACKS)
			numBarracks++;
	}
	return numBarracks < 1;
}

/**********************High Econ**************************/

//late game economy is an income greater than 75 with at least 1 barracks
bool highEco(AI* player) {
	int numBarracks = 0;
	for (Building* b : player->buildings) {
		if (b->type == BARRACKS)
			numBarracks++;
	}
	if (player->income >= (MINE_INCOME * 6 + THRONE_INCOME) && numBarracks >= 1) {
		//cout << "Late-Game Economy" << endl;
		return true;
	}
	return false;
}

//if we have 10 or more mines then we dont need anymore
bool highMines(AI* player){
	int numMines = 0;
	for (Building* b : player->buildings) {
		if (b->type == MINE)
			numMines++;
	}
	return numMines < 10;
}

//if we have 2 or more barracks then we dont need anymore
bool highBarracks(AI* player){
	int numBarracks = 0;
	for (Building* b : player->buildings) {
		if (b->type == BARRACKS)
			numBarracks++;
	}
	return numBarracks < 2;
}



/********************************Unit Tree***********************************/

//do we have a barracks, free or not
bool haveBarracks(AI* player) {
	for (Building* b : player->buildings) {
		if (b->type == BARRACKS)
			return true;
	}
	return false;
}

/**********************Early Game**************************/

//do we have less than 4 archers
bool starterArch(AI* player){
	int numArchers = 0;
	for (Unit* u : player->units) {
		if (u->unitType == ARCHER)
			numArchers++;
	}
	return numArchers < 4;
}

//do we have less than 20 units, if so do we have a barracks thats not producing anything, if so build an archer at that barracks
bool buildArch(AI* player) {
	if (player->units.size() < POP_CAP) {
		for (Building* b : player->buildings) {
			if (b->type == BARRACKS && b->state == BUILT) {
				if (player->resources >= UNIT_COSTS[ARCHER]) {
					b->startProduction(ARCHER);
					player->unitsBuilt++;
					player->resources -= UNIT_COSTS[ARCHER];
					player->resourcesSpent += UNIT_COSTS[ARCHER];
					return true;
				}
			}
		}
	}
	return false;
}

/**********************Mid Game**************************/

//do we have more than 10 military units total
bool medUnits(AI* player){
	int numUnits = 0;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER)
			numUnits++;
	}
	return numUnits >= 10;
}

//do we have less than 7 archers
bool medArch(AI* player){
	int numArchers = 0;
	for (Unit* u : player->units) {
		if (u->unitType == ARCHER)
			numArchers++;
	}
	return numArchers < 7;
}

//do we have less than 7 swordsman
bool medSwords(AI* player){
	int numSwords = 0;
	for (Unit* u : player->units) {
		if (u->unitType == SWORDSMAN)
			numSwords++;
	}
	return numSwords < 7;
}

//do we have less than 20 units, if so do we have a barracks thats not producing anything, if so build an swordsman at that barracks
bool buildSwords(AI* player){
	if (player->units.size() < POP_CAP) {
		for (Building* b : player->buildings) {
			if (b->type == BARRACKS && b->state == BUILT) {
				if (player->resources >= UNIT_COSTS[SWORDSMAN]) {
					b->startProduction(SWORDSMAN);
					player->unitsBuilt++;
					player->resources -= UNIT_COSTS[SWORDSMAN];
					player->resourcesSpent += UNIT_COSTS[SWORDSMAN];
					return true;
				}
			}
		}
	}
	return false;
}

/**********************Late Game**************************/

//do we have 19 military units
bool highUnits(AI* player){

	int numUnits = 0;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER)
			numUnits++;
	}
	return numUnits >= 19;
}

//do we have less than 15 archers
bool highArch(AI* player){
	int numArchers = 0;
	for (Unit* u : player->units) {
		if (u->unitType == ARCHER)
			numArchers++;
	}
	return numArchers < 15;
}

//do we have less than 15 swordsman
bool highSwords(AI* player){
	int numSwords = 0;
	for (Unit* u : player->units) {
		if (u->unitType == SWORDSMAN)
			numSwords++;
	}
	return numSwords < 15;
}

/********************************Move Tree***********************************/

/**********************Enemy Visible**************************/

//is there an enemy thing within 10 square radius of any of our things
bool enemyVisible(AI* player){

	bool foundEnemy = false;
	//check if a enemy unit is within 10 squares of any of our stuff
	glm::vec2 cellPos;
	for (Unit* u : player->units) {
		cellPos = u->cellPos;
		for (int y = -VISIBLE_RANGE; y <= AI_VISIBLE_RANGE; y++) {
			for (int x = -VISIBLE_RANGE; x <= AI_VISIBLE_RANGE; x++) {
				glm::vec2 enemyCellPos = glm::vec2(cellPos.x + x, cellPos.y + y);
				if (player->gameWorld->isInGrid(enemyCellPos)) {
					if (player->gameWorld->isOccupiedByEnemyUnit(enemyCellPos, player->team)) {
						if (enemyCellPos != player->visEnemyCellPos) {
							player->visEnemyCellPos = enemyCellPos;
							foundEnemy = true;
						}
					}
					if (player->gameWorld->isOccupiedByEnemyBuilding(enemyCellPos, player->team)) {
						if (enemyCellPos != player->visEnemyCellPos) {
							player->visEnemyCellPos = enemyCellPos;
							foundEnemy = true;
						}
					}
				}
			}
		}
	}

	for (Building* b : player->buildings) {
		cellPos = b->cellPos;
		for (int y = -13; y <= 13; y++) {
			for (int x = -13; x <= 13; x++) {
				glm::vec2 enemyCellPos = glm::vec2(cellPos.x + x, cellPos.y + y);
				if (player->gameWorld->isInGrid(enemyCellPos)) {
					if (player->gameWorld->isOccupiedByEnemyUnit(enemyCellPos, player->team)) {
						if (enemyCellPos != player->visEnemyCellPos) {
							player->visEnemyCellPos = enemyCellPos;
							foundEnemy = true;
						}
					}
					if (player->gameWorld->isOccupiedByEnemyBuilding(enemyCellPos, player->team)) {
						if (enemyCellPos != player->visEnemyCellPos) {
							player->visEnemyCellPos = enemyCellPos;
							foundEnemy = true;
						}
					}
				}
			}
		}
	}

	return foundEnemy;
}

void moveClosest(AI* player) {

	//grab an idle unit
	Unit* closestUnit = nullptr;
	float closest = 1000;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER && u->state == IDLE) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState != ATTACKING) {
				if (glm::distance(u->cellPos, player->visEnemyCellPos) < closest)
					closestUnit = u;
			}
		}
	}

	if (closestUnit != nullptr)
		player->checkAndMove(closestUnit, player->visEnemyCellPos);

}

/**********************Early Game**************************/

//do we have more than 3 units
bool moreThan3Units(AI * player) {
	int numUnits = 0;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER)
			numUnits++;
	}
	return numUnits >= 3;
}

//move unit to patrol location (outskirt of base)
void patrol(AI* player) {

	//count number of units currently patrolling
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState == PATROL)
				return;
		}
	}

	//grab an idle unit
	AttackingUnit* idleUnit = nullptr;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState == NONE) {
				idleUnit = au;
				continue;
			}
		}
	}
	if (idleUnit == nullptr)
		return;

	glm::vec2 min, max;
	glm::vec2 patrolPos;

	min.x = (float)player->gameWorld->gridSize; max.x = 0.0f;
	min.y = (float)player->gameWorld->gridSize; max.y = 0.0f;
	for (Building* b : player->buildings) {
		if (b->cellPos.x > max.x)
			max.x = b->cellPos.x;
		if (b->cellPos.x < min.x)
			min.x = b->cellPos.x;

		if (b->cellPos.y < min.y)
			min.y = b->cellPos.y;
		if (b->cellPos.y > max.y)
			max.y = b->cellPos.y;
	}

	Grid::restrictPoint(min);
	Grid::restrictPoint(max);

	bool pathMade = false;
	while (!pathMade) {
		// obtain a seed from the system clock:
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::mt19937 rng(seed);
		std::uniform_int_distribution<int> genX((int)min.x, (int)max.x); // uniform, unbiased
		std::uniform_int_distribution<int> genY((int)min.y, (int)max.y);

		int x = genX(rng);
		int y = genY(rng);
		patrolPos = glm::vec2(x, y);
			
		player->checkAndMove(idleUnit, patrolPos);
		if (idleUnit->currentPath.size() > 0)
			pathMade = true;
	}
	idleUnit->actionState = PATROL;
}


/**********************Mid Game**************************/

bool moreThan6Units(AI * player){
	int numUnits = 0;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER)
			numUnits++;
	}
	return numUnits >= 6;
}


//move unit to explore location (further from base)
void explore(AI* player) {

	//count number of units currently exploring
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState == EXPLORE)
				return;
		}
	}

	//grab an idle unit
	AttackingUnit* idleUnit = nullptr;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState == NONE) {
				idleUnit = au;
				continue;
			}
		}
	}
	if (idleUnit == nullptr)
		return;

	glm::vec2 min, max;
	glm::vec2 explorePos;

	min.x = (float)player->gameWorld->gridSize; max.x = 0.0f;
	min.y = (float)player->gameWorld->gridSize; max.y = 0.0f;
	for (Building* b : player->buildings) {
		if (b->cellPos.x > max.x)
			max.x = b->cellPos.x;
		if (b->cellPos.x < min.x)
			min.x = b->cellPos.x;

		if (b->cellPos.y < min.y)
			min.y = b->cellPos.y;
		if (b->cellPos.y > max.y)
			max.y = b->cellPos.y;
	}

	min -= (player->gameWorld->gridSize / 2);
	max += (player->gameWorld->gridSize / 2);

	Grid::restrictPoint(min);
	Grid::restrictPoint(max);

	bool pathMade = false;
	while (!pathMade) {
		// obtain a seed from the system clock:
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::mt19937 rng(seed);
		std::uniform_int_distribution<int> genX((int)min.x, (int)max.x); // uniform, unbiased
		std::uniform_int_distribution<int> genY((int)min.y, (int)max.y);

		int x = genX(rng);
		int y = genY(rng);
		explorePos = glm::vec2(x, y);

		player->checkAndMove(idleUnit, explorePos);
		if (idleUnit->currentPath.size() > 0)
			pathMade = true;
	}
	idleUnit->actionState = EXPLORE;
}


/**********************Late Game**************************/

bool moreThan10Units(AI * player){
	int numUnits = 0;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER)
			numUnits++;
	}
	return numUnits >= 10;
}

//gather army and move towards enemy base
void attackBase(AI* player) {

	//grab all non essential units
	std::vector<AttackingUnit*> attackForce;
	for (Unit* u : player->units) {
		if (u->unitType != BUILDER) {
			AttackingUnit* au = (AttackingUnit*)u;
			if (au->actionState == NONE || au->actionState == EXPLORE)
				attackForce.push_back(au);
		}
	}

	if (attackForce.empty())
		return;

	//for each unit in the attack force
	for (AttackingUnit* au : attackForce) {
		//get the random attack target
		glm::vec2 min, max;
		glm::vec2 attackPos;

		min.x = (float)player->gameWorld->gridSize; max.x = 0.0f;
		min.y = (float)player->gameWorld->gridSize; max.y = 0.0f;
		for (Building* b : player->enemy->buildings) {
			if (b->cellPos.x > max.x)
				max.x = b->cellPos.x;
			if (b->cellPos.x < min.x)
				min.x = b->cellPos.x;

			if (b->cellPos.y < min.y)
				min.y = b->cellPos.y;
			if (b->cellPos.y > max.y)
				max.y = b->cellPos.y;
		}
		for (Unit* u : player->enemy->units) {
			if (u->unitType == BUILDER) {
				if (u->cellPos.x > max.x)
					max.x = u->cellPos.x;
				if (u->cellPos.x < min.x)
					min.x = u->cellPos.x;

				if (u->cellPos.y < min.y)
					min.y = u->cellPos.y;
				if (u->cellPos.y > max.y)
					max.y = u->cellPos.y;
			}
		}

		Grid::restrictPoint(min);
		Grid::restrictPoint(max);

		bool pathMade = false;
		while (!pathMade) {
			// obtain a seed from the system clock:
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937 rng(seed);
			std::uniform_int_distribution<int> genX((int)min.x, (int)max.x); // uniform, unbiased
			std::uniform_int_distribution<int> genY((int)min.y, (int)max.y);

			int x = genX(rng);
			int y = genY(rng);
			glm::vec2 attackPos = glm::vec2(x, y);

			player->checkAndMove(au, attackPos);
			if (au->currentPath.size() > 0)
				pathMade = true;
		}
		au->actionState = ATTACKING;
	}
}

