#include "Player.h"


void Player::Update(double deltaT) {
	//increment resources
	resourceTimer += deltaT;
	if (resourceTimer >= 5.0) {
		resourceTimer = 0;
		resources += income;
		resourcesGained += income;
	}

	//check all units and remove any dead ones
	vector<Unit*> aliveUnits;
	for(int i = 0; i < units.size(); i++){
		Unit* unit = units[i];
		if (unit->health > 0) {
			//update units that arent dead
			unit->Update(deltaT);
			aliveUnits.push_back(unit);
			if (unit->targetCell != unit->cellPos && unit->currentPath.empty()) {
				checkAndMove(unit, unit->targetCell);
			}
		}
		else {
			//if dead play appropriate sound and unoccupy cells
			switch (unit->unitType) {
			case ARCHER:
				assets->playArcherDeathSound();
				break;
			case BUILDER: case SWORDSMAN:
				assets->playManDeathSound();
				break;
			}
			gameWorld->unOccupyCell(unit->cellPos);
		}
	}
	//update statistics
	int loss = units.size() - aliveUnits.size();
	this->unitsLost += loss;
	units = aliveUnits;

	//check all buildings and remove any dead ones
	vector<Building*> aliveBuildings;
	for (Building* b : buildings) {
		if (b->health > 0) {
			//update buildings that arent dead
			b->Update(deltaT);
			aliveBuildings.push_back(b);
		}
		else {
			//if dead play building death sound and unoccupy cells
			assets->playBuildingDeath();
			gameWorld->unOccupyCells(b->cellPos, b->sizeInCells);
		}
	}
	//update statistics
	loss = buildings.size() - aliveBuildings.size();
	this->buildingsLost += loss;
	buildings = aliveBuildings;
}

void Player::checkAndMove(Unit* movingUnit, glm::vec2 targetCell){

	//check to see if the target cell is valid
	if (gameWorld->isValid(targetCell)) {
		movingUnit->targetCell = targetCell;
		movingUnit->moveTo(targetCell);
	}
	//if not valid check to see if its a building or a unit
	else {
		movingUnit->targetCell = targetCell;
		//occupied by unit
		if (gameWorld->isOccupiedByUnit(targetCell)) {
			if (gameWorld->isOccupiedByEnemyUnit(targetCell, team) && movingUnit->unitType != BUILDER) {
				attackUnit((AttackingUnit*)movingUnit, targetCell);
			}
			else {
				moveToUnit(movingUnit, targetCell);
			}
		}
		else {
			//occupied by building
			if (gameWorld->isOccupiedByEnemyBuilding(targetCell, team) && movingUnit->unitType != BUILDER) {
				attackBuilding((AttackingUnit*)movingUnit, targetCell);
			}
			else {
				moveToBuilding(movingUnit, targetCell);
			}
		}
	}
}

void Player::moveToUnit(Unit * movingUnit, glm::vec2 targetCell){
	if (movingUnit->cellPos != targetCell) {
		//generate a path to the unit
		movingUnit->moveTo(targetCell);
		//edit path to be one cell before the target
		movingUnit->currentPath.erase(movingUnit->currentPath.end(), movingUnit->currentPath.end());
		movingUnit->targetCell = movingUnit->currentPath.back();
	}
}

void Player::moveToBuilding(Unit * movingUnit, glm::vec2 targetCell){

	//get the size of the building
	int size = 0;
	Building* target = nullptr;
	for (Building* b : buildings) {
		if (b->coversCell(targetCell)) {
			size = b->sizeInCells;
			target = b;
		}
	}
	for (Building* b : enemy->buildings) {
		if (b->coversCell(targetCell)) {
			size = b->sizeInCells;
			target = b;
		}
	}
	if (target != nullptr) {
		//generate a path to the building
		movingUnit->moveTo(target->cellPos, size);

		//edit path to be size amount of cells before the target
		//movingUnit->currentPath.erase(movingUnit->currentPath.end(), movingUnit->currentPath.end());

		if (movingUnit->unitType == BUILDER && movingUnit->team == target->team) {
			movingUnit->newTarget(target);
		}
	}
}

void Player::attackUnit(AttackingUnit * movingUnit, glm::vec2 targetCell){
	//generate a path to the unit
	movingUnit->moveTo(targetCell);

	//get the target unit
	Unit* target = nullptr;
	for (Unit* u : enemy->units) {
		if (u->cellPos == targetCell)
			target = u;
	}

	if (target != nullptr) {
		if (movingUnit->currentPath.size() > movingUnit->range) {
			//edit path to be range amount of cells before the target
			movingUnit->currentPath.erase(movingUnit->currentPath.end() - movingUnit->range, movingUnit->currentPath.end());
			movingUnit->newTarget(target);
			movingUnit->targetCell = movingUnit->currentPath.back();
		}
		else {
			movingUnit->currentPath.clear();
			movingUnit->newTarget(target);
			movingUnit->targetCell = movingUnit->cellPos;
		}
	}
}

void Player::attackBuilding(AttackingUnit * movingUnit, glm::vec2 targetCell){

	//get the target Building
	Building* target= nullptr;
	int size = 0;
	for (Building* b : enemy->buildings) {
		if (b->coversCell(targetCell)) {
			target = b;
			size = b->sizeInCells;
		}
	}

	if (target != nullptr) {
		//generate a path to the building
		movingUnit->moveTo(target->cellPos, target->sizeInCells);

		if (movingUnit->currentPath.size() >= movingUnit->range) {
			//edit path to be range + size amount of cells before the target 
			movingUnit->currentPath.erase(movingUnit->currentPath.end() - (movingUnit->range - 1), movingUnit->currentPath.end());
			movingUnit->newTarget(target);
			movingUnit->targetCell = movingUnit->currentPath.back();
		}
		else {
			movingUnit->currentPath.clear();
			movingUnit->newTarget(target);
			movingUnit->targetCell = movingUnit->cellPos;
		}
	}
}

bool Player::isDead(){
	bool hasBuilder = false;
	bool hasThrone = false;

	for (Unit* u : units) {
		if (u->unitType == BUILDER)
			hasBuilder = true;
	}

	for (Building* b : buildings) {
		if (b->type == THRONE && b->state != BUILDING)
			hasThrone = true;
	}
	//if they have a builder or a throne they are not dead
	return !(hasBuilder || hasThrone);
}

Unit* Player::addUnit(glm::vec2 cellPos, UnitType type){
	switch (type) {
	case BUILDER: {

		Builder* newBuilder = new Builder(assets);
		newBuilder->Init(cellPos, gameWorld, BUILDER, team);
		units.push_back(newBuilder);
		return newBuilder;
		break;
	}
	case ARCHER: {

		Archer* newArcher = new Archer(assets);
		newArcher->Init(cellPos, gameWorld, ARCHER, team);
		units.push_back(newArcher);
		return newArcher;
		break;
	}
	case SWORDSMAN: {
		Swordsman* newSword = new Swordsman(assets);
		newSword->Init(cellPos, gameWorld, SWORDSMAN, team);
		units.push_back(newSword);
		return newSword;
		break;
	}
	case UNKNOWN_UNIT:
		cerr << "Unknown Unit added" << endl;;
		break;
	}
	return nullptr;
}

Building* Player::addBuilding(glm::vec2 cellPos, BuildingType type){
	switch (type)
	{
	case THRONE: {
		Throne* newThrone = new Throne(throne, buildingShader, barShader);
		newThrone->Init(cellPos, gameWorld, team);
		selectedBuilding = newThrone;
		buildings.push_back(newThrone);
		return newThrone;
		break;
	}
	case MINE: {
		Mine* newMine = new Mine(mine, buildingShader, barShader);
		newMine->Init(cellPos, gameWorld, team);
		selectedBuilding = newMine;
		buildings.push_back(newMine);
		return newMine;
		break;
	}
	case BARRACKS: {
		Barracks* newBarracks = new Barracks(barracks, buildingShader, barShader);
		newBarracks->Init(cellPos, gameWorld, team);
		selectedBuilding = newBarracks;
		buildings.push_back(newBarracks);
		return newBarracks;
		break;
	}
	case UNKNOWN_BUILDING:
		cerr << "Unknown Building added" << endl;;
		break;
	}
	return nullptr;
}
