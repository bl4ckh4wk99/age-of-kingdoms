#include "Human.h"

Human::Human(Grid* gameWorld, AssetLibrary* assets, Team team){
	this->gameWorld = gameWorld;
	this->assets = assets;
	this->animationShader = assets->getAnimatedShader();
	this->buildingShader = assets->getBuildingShader();
	this->modelShader = assets->getModelShader();
	this->barShader = assets->getBarShader();

	archer = assets->getArcher();
	builder = assets->getBuilder();
	swordsman = assets->getSword();

	throne = assets->getThrone(team);
	mine = assets->getMine(team);
	barracks = assets->getBarracks(team);

	guiShader = assets->getGUIShader();

	gui = GameGUI(assets);
	gui.Init();

	this->selectedUnit = nullptr;
	buildGUI = BuildGUI(assets);

	this->team = team;
}

void Human::Init(Player* enemy){
	this->enemy = enemy;
	this->resources = STARTING_RESOURCES;
	this->income = 0;
	this->resourceTimer = 0.0;

	unitsBuilt = 0;
	unitsLost = 0;
	buildingsBuilt = 0;
	buildingsLost = 0;
	resourcesGained = STARTING_RESOURCES;
	resourcesSpent = 0;
}

void Human::Draw(Camera* cam){

	for (Unit* unit : units) {
		unit->Draw(cam);
	}

	for (Building* building : buildings) {
		building->Draw(cam);
	}
}

void Human::DrawGUI(){
	if (selectedUnit != nullptr || selectedBuilding != nullptr) {
		buildGUI.Draw(POP_CAP);
	}
	else {
		gui.Draw(POP_CAP);
	}
}

void Human::Update(double deltaT){
	Player::Update(deltaT);

	for (Building* building : buildings) {
		building->Update(deltaT);
		if (building->state == BUILDING) {
			if (building->checkBuild())
				income += building->income;
		}
		if (building->state == PRODUCING) {
			if (building->checkProduction((POP_CAP > this->units.size()))) {
				addUnit(building->exitCell(), building->curProduction);
				switch (building->curProduction) {
				case BUILDER:
					assets->playVillagerSound();
					break;
				case ARCHER: case SWORDSMAN:
					assets->playMilitarySound();
					break;
				}
			}
		}
	}

	if (selectedUnit != nullptr || selectedBuilding != nullptr) {
		buildGUI.Update(resources, units.size());
	}
	else {
		gui.Update(resources, units.size());
	}
}

void Human::select(glm::vec2 cell){

	//if we have a selected building
	if (selectedBuilding != nullptr) {
		//if currently placing the building
		if (selectedBuilding->state == PLACING) {
			//stop placing building and delete it
			buildings.pop_back();
			selectedBuilding = nullptr;
			buildGUI.deSelectButtons();
		}
	}

	for (Unit* u : units) {
		//found unit in selected cell
		if (u->cellPos == cell) {
			//selected unit is this unit, remove selected building if one
			selectedUnit = u;
			selectedBuilding = nullptr;

			//construct gui for the selected unit
			buildGUI.Init(selectedUnit->unitType);

			switch (selectedUnit->unitType) {
			case ARCHER:
				assets->playUnitSelectFemale();
				break;
			case BUILDER: case SWORDSMAN:
				assets->playUnitSelectMale();
				break;
			}
		}
	}

	for (Building* b : buildings) {
		//found building that overlaps selected cell
		if (b->coversCell(cell)) {
			//selected building is this building, remove selected unit if one
			selectedBuilding = b;
			selectedUnit = nullptr;

			if (selectedBuilding->state >= BUILT) {
				//construct gui for the selected building
				buildGUI.Init(selectedBuilding->type, &selectedBuilding->currentlyBuilding);
			}

			switch (selectedBuilding->type) {
			case BARRACKS:
				assets->playBarracksSound();
				break;
			case MINE:
				assets->playMineSound();
				break;
			case THRONE:
				assets->playThroneSound();
				break;
			}
		}
	}

}

void Human::action(glm::vec2 cell){	
	//if we have a selected building
	if (selectedBuilding != nullptr) {
		//if currently placing the building
		if (selectedBuilding->state == PLACING) {
			if (gameWorld->availableToBuild(cell, selectedBuilding->sizeInCells)) {
				selectedBuilding->startBuilding();
				buildingsBuilt++;
				resources -= selectedBuilding->cost;
				resourcesSpent += selectedBuilding->cost;

				//move selectunit (builder) to build location and deselect
				checkAndMove(selectedUnit, cell);
				selectedUnit->targetBuilding = selectedBuilding;
				assets->playBuilderBuild();
				//deselect stuff
				selectedBuilding = nullptr;
				selectedUnit = nullptr;
			}
		}
	}
	else {
		//if we have a selected unit, do movement
		if (selectedUnit != nullptr) {
			switch (selectedUnit->unitType) {
			case ARCHER:
				assets->playArcherAction();
				break;
			case BUILDER:
				assets->playBuilderAction();
				break;
			case SWORDSMAN:
				assets->playSwordAction();
				break;
			}
			checkAndMove(selectedUnit, cell);
		}
	}

}

bool Human::inGUI(glm::vec4 normalisedCoords){

	return gui.checkGUIcollision(normalisedCoords);
}

void Human::processGUIMovement(glm::vec4 normalisedCoords){
	buildGUI.checkButtonHighlights(normalisedCoords);
}

void Human::processGUIClick(glm::vec4 normalisedCoords, bool leftClick){
	if (selectedBuilding != nullptr) {
		//building is being placed
		if (selectedBuilding->state == PLACING) {
			//stop placing building and delete it
			buildings.pop_back();
			selectedBuilding = nullptr;
		}
		else {
			//selected a production from that building
			UnitType newType = buildGUI.checkBuildingButtons(normalisedCoords, this->resources, (POP_CAP > this->units.size()), leftClick);
			if (leftClick) {
				if (newType != UNKNOWN_UNIT) {
					selectedBuilding->startProduction(newType);
					this->unitsBuilt++;
					this->resources -= UNIT_COSTS[newType];
					this->resourcesSpent += UNIT_COSTS[newType];
				}
			}
			else {
				if (newType != UNKNOWN_UNIT) {
					bool removed = selectedBuilding->removeProduction(newType);
					if (removed) {
						this->unitsBuilt--;
						this->resources += UNIT_COSTS[newType];
						this->resourcesSpent -= UNIT_COSTS[newType];
					}
				}
			}
		}
	}
	//if we have a unit selected
	if (selectedUnit != nullptr) {
		if (selectedUnit->unitType == BUILDER) {
			BuildingType newType = buildGUI.checkBuildersButtons(normalisedCoords, this->resources);
			if (newType != UNKNOWN_BUILDING && this->resources >= BUILDING_COSTS[newType]) {
				addBuilding(glm::vec2(5, 30), newType);
			}
		}
	}
}
