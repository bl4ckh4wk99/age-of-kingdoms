#pragma once

#include "Player.h"
#include "../../BehaviourTree/BTree.h"

#include <random>
#include <chrono>

#define AI_VISIBLE_RANGE 5
#define DELAY_TIME 1.5

class AI : public Player{
private:

	BTree<AI*> buildTree;
	BTree<AI*> unitTree;
	BTree<AI*> moveTree;

	double aiDelay;

public:
	AI();
	AI(Grid* gameWorld, AssetLibrary* assets, Team team);

	void Init(Player* enemy);

	void Draw(Camera* cam);

	void Update(double deltaT);

	void select (glm::vec2 cell);

	void action(glm::vec2 cell);

	void createBuildTree();

	void createUnitTree();

	void createMoveTree();

	glm::vec2 visEnemyCellPos;
	//vector<AttackingUnit*> patrolingUnits;
	//vector<AttackingUnit*> exploringUnits;

};

static bool haveMoney(AI* player);

/********************************Build Tree***********************************/

static bool noBuilder(AI* player);

static void buildBuilder(AI* player);

static bool unfinishedBuilding(AI* player);

static void build(AI* player, Unit* builder, BuildingType type);

/**********************Low Econ**************************/

static bool lowEco(AI* player);

static bool starterMines(AI* player);

//static bool freeBuilder(AI* player);

static bool buildMine(AI* player);

static bool starterBarracks(AI* player);

static bool buildBarracks(AI* player);

//static bool noThrone(AI* player);

static bool buildThrone(AI* player);

/**********************Med Econ**************************/

static bool medEco(AI* player);

static bool medMines(AI* player);

static bool medBarracks(AI* player);

/**********************High Econ**************************/

static bool highEco(AI* player);

static bool highMines(AI* player);

static bool highBarracks(AI* player);


/********************************Unit Tree***********************************/

static bool haveBarracks(AI* player);

/**********************Low Econ**************************/

static bool starterArch(AI* player);

//static bool freeBarracks(AI* player);

static bool buildArch(AI* player);

/**********************Med Econ**************************/

static bool medUnits(AI* player);

static bool medArch(AI* player);

static bool medSwords(AI* player);

static bool buildSwords(AI* player);

/**********************High Econ**************************/

static bool highUnits(AI* player);

static bool highArch(AI* player);

static bool highSwords(AI* player);


/********************************Move Tree***********************************/

/**********************Enemy Visible**************************/

static bool enemyVisible(AI* player);

static void moveClosest(AI* player);

/**********************Early Game**************************/

static bool moreThan3Units(AI* player);

static void patrol(AI* player);

/**********************Mid Game**************************/

static bool moreThan6Units(AI* player);

static void explore(AI* player);

/**********************Late Game**************************/

static bool moreThan10Units(AI* player);

static void attackBase(AI* player);