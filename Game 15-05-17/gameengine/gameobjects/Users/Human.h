#pragma once

#include "Player.h"
#include "../GUI/BuildGUI.h"

class Human : public Player {
private:
	GUIShader* guiShader;
	GameGUI gui;
	BuildGUI buildGUI;

public:

	Human() {}
	Human(Grid* gameWorld, AssetLibrary* assets, Team team);

	void Init(Player* enemy);

	void Draw(Camera* cam);

	void DrawGUI();

	void Update(double deltaT);

	void select(glm::vec2 cell);

	void action(glm::vec2 cell);

	bool inGUI(glm::vec4 normalisedCoords);
	void processGUIMovement(glm::vec4 normalisedCoords);
	void processGUIClick(glm::vec4 normalisedCoords, bool leftClick);

};
