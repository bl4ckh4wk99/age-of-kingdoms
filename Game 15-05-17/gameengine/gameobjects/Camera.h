#pragma once
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <iostream>

#define moveValue 2.5f
#define rotValue 1.0f
#define zoomValue 5.0f

struct Camera
{
private:

	glm::mat4* projectionMatrix;
	glm::vec3 pos;
	glm::vec3 lookat;
	glm::vec3 up;

	float rotation;
	float zoomVal;

	glm::vec3 forward;
	glm::vec3 right;

	
public:
	Camera() {}

	Camera(glm::vec3 pos, glm::mat4* projectionMatrix);

	void lookAtPlayerOne();
	void lookAtCenter();

	void move(double deltaT);
	void rotate(double deltaT);
	void zoom(double deltaT);

	glm::mat4 getView();
	glm::mat4 getProjectionMatrix();

	glm::vec3 getPos() { return this->pos; }
	glm::vec3 getRight() { return this->right; }
	glm::vec3 getUp() { return this->up; }


	glm::vec2 moveDir;
	int rotDir;
	int zoomDir;
};