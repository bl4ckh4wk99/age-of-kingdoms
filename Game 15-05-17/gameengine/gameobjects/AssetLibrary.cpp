#include "AssetLibrary.h"

AssetLibrary::AssetLibrary(glm::vec2 screenDim){

	/**************************************SHADERS*******************************************/
	
	cubeMapShader = new Shader("Shader/Shaders/cubeMap");

	modelShader = new ModelShader("Shader/Shaders/BasicModel");

	//animatedShader = new AnimationShader("Shader/Shaders/AnimatedModel");
	animatedShader = new AnimationShader("Shader/Shaders/BasicModel");

	buildingShader = new BuildingShader("Shader/Shaders/Building");

	guiShader = new GUIShader("Shader/Shaders/GUIShader");

	guiLineShader = new GUIShader("Shader/Shaders/GUILine");

	fontShader = new FontShader("Shader/Shaders/Text");

	barShader = new BarShader("Shader/Shaders/Line");

	gridShader = new GridShader("Shader/Shaders/HighLight_Grid");

	/***************************************MODELS*******************************************/

	this->gameMap.LoadModelFromFile("Assets/Models/World/World2.obj");
	this->gameMap.FinalizeVBO();

	this->Barracks_1.LoadModelFromFile("Assets/Models/Buildings/Barracks.obj");
	this->Barracks_1.FinalizeVBO();

	this->Barracks_2.LoadModelFromFile("Assets/Models/Buildings/Barracks2.obj");
	this->Barracks_2.FinalizeVBO();

	this->Mine_1.LoadModelFromFile("Assets/Models/Buildings/Mine.obj");
	this->Mine_1.FinalizeVBO();

	this->Mine_2.LoadModelFromFile("Assets/Models/Buildings/Mine2.obj");
	this->Mine_2.FinalizeVBO();

	this->Throne_1.LoadModelFromFile("Assets/Models/Buildings/Throne.obj");
	this->Throne_1.FinalizeVBO();

	this->Throne_2.LoadModelFromFile("Assets/Models/Buildings/Throne2.obj");
	this->Throne_2.FinalizeVBO();

	this->archer.LoadModelFromFile("Assets/Models/Units/Archer/Archer.obj");
	this->archer.FinalizeVBO();

	this->arrow.LoadModelFromFile("Assets/Models/Units/Archer/Arrow.obj");
	this->arrow.FinalizeVBO();

	this->builder.LoadModelFromFile("Assets/Models/Units/Builder/Builder.obj");
	this->builder.FinalizeVBO();

	this->swordsman.LoadModelFromFile("Assets/Models/Units/Swordsman/Swordsman.obj");
	this->swordsman.FinalizeVBO();

	//if (!archer.LoadMesh("Assets/Models/Units/Archer/Archer.fbx")) {
	//	printf("Mesh load failed\n");
	//}

	//if (!swordsman.LoadMesh("Assets/Models/Units/Swordsman/Paladin_w_Prop_J_Nordstrom.dae")) {
	//	printf("Mesh load failed\n");
	//}

	//if (!builder.LoadMesh("Assets/Models/Units/Builder/Builder_Idle01.fbx")) {
	//	printf("Mesh load failed\n");
	//}

	/***************************************GUI STUFF*******************************************/

	goldIconTex = new Texture("Assets/GUI/Gold.png");
	popIconTex = new Texture("Assets/GUI/Population.png");

	resourceBackgroundTex = new Texture("Assets/GUI/GUI_Resources.png");
	selectionBackgroundTex = new Texture("Assets/GUI/GUI_Background.png");

	unitIcons.push_back(new Texture("Assets/GUI/BuildIcon.png"));
	unitIcons.push_back(new Texture("Assets/GUI/ArcherIcon.png"));
	unitIcons.push_back(new Texture("Assets/GUI/SwordIcon.png"));
	

	buildIcons.push_back(new Texture("Assets/GUI/CastleIcon.png"));
	buildIcons.push_back(new Texture("Assets/GUI/MineIcon.png"));
	buildIcons.push_back(new Texture("Assets/GUI/BarracksIcon.png"));

	menuBackgroundTex = new Texture("Assets/GUI/Menu.png");
	menuPvAITex = new Texture("Assets/GUI/PvAI.png");
	menuAIvAITex = new Texture("Assets/GUI/AIvAI.png");

	blankBackgroundTex = new Texture("Assets/GUI/BlankMenu.png");
	mainMenuTex = new Texture("Assets/GUI/MainMenu.png");

	Texture* fontTex = new Texture("Assets/GUI/Consolas.png");
	font = Font(fontTex, 0.5f);

	//Load music
	gameMusic = Mix_LoadMUS("Assets/Sound/AoE2_GameMix.wav");
	if (gameMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	menuMusic = Mix_LoadMUS("Assets/Sound/AoE2_Tazer.wav");
	if (menuMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	villagerSound = Mix_LoadWAV("Assets/Sound/AoE2_Villager.wav");
	if (villagerSound == NULL)
	{
		printf("Failed to load villager sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	archerAttack = Mix_LoadWAV("Assets/Sound/AoE2_Archer.wav");
	if (archerAttack == NULL)
	{
		printf("Failed to load archer sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	militarySound = Mix_LoadWAV("Assets/Sound/AoE2_Unit.wav");
	if (militarySound == NULL)
	{
		printf("Failed to load swordsman sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	selectMale1 = Mix_LoadWAV("Assets/Sound/AoE2_UnitSelectMale1.wav");
	if (selectMale1 == NULL)
	{
		printf("Failed to load unit selection sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	selectMale2 = Mix_LoadWAV("Assets/Sound/AoE2_UnitSelectMale2.wav");
	if (selectMale2 == NULL)
	{
		printf("Failed to load unit selection sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	selectFemale1 = Mix_LoadWAV("Assets/Sound/AoE2_UnitSelectFemale1.wav");
	if (selectFemale1 == NULL)
	{
		printf("Failed to load unit selection sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	selectFemale2 = Mix_LoadWAV("Assets/Sound/AoE2_UnitSelectFemale2.wav");
	if (selectFemale2 == NULL)
	{
		printf("Failed to load unit selection sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	mineSound = Mix_LoadWAV("Assets/Sound/AoE2_Mine.wav");
	if (mineSound == NULL)
	{
		printf("Failed to load mine sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	barracksSound = Mix_LoadWAV("Assets/Sound/AoE2_Barracks.wav");
	if (barracksSound == NULL)
	{
		printf("Failed to load barracks sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	//Load sound effects
	throneSound = Mix_LoadWAV("Assets/Sound/AoE2_Throne.wav");
	if (throneSound == NULL)
	{
		printf("Failed to load throne sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	archerAction = Mix_LoadWAV("Assets/Sound/AoE2_ArcherAction.wav");
	if (archerAction == NULL)
	{
		printf("Failed to load archerDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	archerDeath1 = Mix_LoadWAV("Assets/Sound/AoE2_ArcherDeath1.wav");
	if (archerDeath1 == NULL)
	{
		printf("Failed to load archerDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	archerDeath2 = Mix_LoadWAV("Assets/Sound/AoE2_ArcherDeath2.wav");
	if (archerDeath2 == NULL)
	{
		printf("Failed to load archerDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	archerDeath3 = Mix_LoadWAV("Assets/Sound/AoE2_ArcherDeath3.wav");
	if (archerDeath3 == NULL)
	{
		printf("Failed to load archerDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	swordAction = Mix_LoadWAV("Assets/Sound/AoE2_SwordAction.wav");
	if (swordAction == NULL)
	{
		printf("Failed to load sword action sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	swordAttack = Mix_LoadWAV("Assets/Sound/AoE2_SwordAttackUnit.wav");
	if (swordAttack == NULL)
	{
		printf("Failed to load sword attack sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	swordDeath1 = Mix_LoadWAV("Assets/Sound/AoE2_SwordDeath1.wav");
	if (swordDeath1 == NULL)
	{
		printf("Failed to load swordDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	swordDeath2 = Mix_LoadWAV("Assets/Sound/AoE2_SwordDeath2.wav");
	if (swordDeath2 == NULL)
	{
		printf("Failed to load swordDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	swordDeath3 = Mix_LoadWAV("Assets/Sound/AoE2_SwordDeath3.wav");
	if (swordDeath3 == NULL)
	{
		printf("Failed to load swordDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	builderAction = Mix_LoadWAV("Assets/Sound/AoE2_BuilderAction.wav");
	if (builderAction == NULL)
	{
		printf("Failed to load builderAction sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}
	builderBuild = Mix_LoadWAV("Assets/Sound/AoE2_BuilderBuild.wav");
	if (builderBuild == NULL)
	{
		printf("Failed to load builderBuild sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}


	buildingDeath = Mix_LoadWAV("Assets/Sound/AoE2_BuildingDeath.wav");
	if (buildingDeath == NULL)
	{
		printf("Failed to load buildingDeath sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	this->screenDim = screenDim;


}