#pragma once

#include "AttackingUnit.h"

#define SWORD_HEALTH 150.0f
#define SWORD_DAMAGE 40.0f
#define SWORD_RANGE 1
#define SWORD_SPEED 10.0f
#define SWORD_ATTACK_SPEED 2.0f;

class Swordsman : public AttackingUnit {
private:

public:

	Swordsman(AssetLibrary* assets);

	void Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team);

	void Draw(Camera* cam);

	void Update(double deltaT);

	void attack(double deltaT);
};
