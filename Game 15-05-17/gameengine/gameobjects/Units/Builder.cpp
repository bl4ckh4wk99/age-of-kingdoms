#include "Builder.h"
#include "../Buildings/Building.h"

void Builder::build(double deltaT){
	if (targetBuilding != nullptr && targetBuilding->state == BUILDING) {
		if (glm::length(targetBuilding->cellPos - cellPos) <= 3 + (targetBuilding->sizeInCells / 2)) {
			state = ACTION;
			this->targetCell = this->cellPos;
			targetBuilding->currentBuildTime += deltaT;
			if (targetBuilding->currentBuildTime >= targetBuilding->buildTime) {
				state = IDLE;
				targetBuilding = nullptr;
			}
		}
	}
}

Builder::Builder(AssetLibrary* assets) {

	this->assets = assets;

	this->animationShader = assets->getAnimatedShader();
	this->model = assets->getBuilder();

	this->barShader = assets->getBarShader();

	selected = false;
	this->maxHealth = BUILDER_HEALTH;
	this->health = BUILDER_HEALTH;
	this->cost = UNIT_COSTS[BUILDER];
	this->speed = BUILDER_SPEED;

	glGenVertexArrays(1, &this->healthVAO);
	glGenBuffers(2, this->healthVBO);
}

void Builder::Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team) {

	this->cellPos = cellPos;
	this->targetCell = cellPos;
	this->worldPos = gameWorld->getCellCen(cellPos);
	this->worldDir = glm::vec3(0, 0, 1);
	this->worldGrid = gameWorld;
	this->unitType = unitType;
	this->team = team;
	//Update world map so cell is occupied
	worldGrid->occupyNewCell(glm::vec2(-1, -1), cellPos, team);

	state = IDLE;

	this->pathFinder = AStar(gameWorld);

	this->transform = Transform();

	transform.setPos(worldPos);
}

void Builder::Draw(Camera* cam) {

	animationShader->Bind();

	animationShader->Update(&transform, cam);

	model->BindModelsVAO();
	model->RenderModel();

	//model->Render();

	drawHealthBar(cam);
}

void Builder::Update(double deltaT) {

	followPath(deltaT);

	transform.setPos(worldPos);

	//rotate to face current direction
	float rotAngle = acos(glm::dot(glm::vec3(0, 0, 1), worldDir));
	//convert to degrees
	if (worldDir.x >= 0) {
		rotAngle = rotAngle * 57.2173304937f;
	}
	else {
		rotAngle = -rotAngle * 57.2173304937f;
	}

	transform.setRot(glm::vec3(0, rotAngle, 0));

	//build target building
	build(deltaT);

	if (currentPath.empty() && targetBuilding == nullptr) {
		state = IDLE;
	}

	/*Animation stuff*/
	//animationTimer += deltaT;
	//if (animationTimer > 0.016) {
	//	animationShader->Bind();
	//	vector<glm::mat4> Transforms;
	//	model->BoneTransform(runningTime, Transforms);

	//	for (uint i = 0; i < Transforms.size(); i++) {
	//		animationShader->UpdateBones(i, Transforms[i]);
	//	}
	//	animationTimer = 0.0;
	//}

}

