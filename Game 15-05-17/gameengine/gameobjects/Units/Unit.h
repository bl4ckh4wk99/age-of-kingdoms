#pragma once

#include "../Shader/AnimationShader.h"
#include "../Shader/BarShader.h"
#include "../../ObjectLoading/Animated/AnimatedModel.h"
#include "../../ObjectLoading/Model.h"
#include "../Primitives/AStar.h"
#include "../Team.h"

class Building;

enum UnitType {BUILDER, ARCHER, SWORDSMAN, UNKNOWN_UNIT};

enum UnitStatus {IDLE, MOVING, ACTION, DYING};

const int UNIT_TIMES[3] = { 20, 20, 30 };

const int UNIT_COSTS[3] = { 100, 100, 175 };

class Unit {
protected:
	AssetLibrary* assets;
	AnimationShader* animationShader;
	//AnimatedModel* model;
	Model* model;
	
	Transform transform;
	AStar pathFinder;

	double currentWait;

	void drawHealthBar(Camera* cam);

	BarShader* barShader;

	glm::vec3 healthBars[4];
	glm::vec3 healthCol[4];

	GLuint healthVAO, healthVBO[2];

	double deltaT;

public:
	glm::vec3 worldPos;
	glm::vec3 worldDir;
	glm::vec2 cellPos;
	vector<glm::vec2> currentPath;
	glm::vec2 targetCell;

	Building* targetBuilding;

	Team team;
	Grid* worldGrid;

	float maxHealth;
	float health;
	int cost;

	float speed;

	UnitStatus state;
	bool selected;
	UnitType unitType;

	virtual void Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team) = 0;
	virtual void Draw(Camera* cam) = 0;
	virtual void Update(double deltaT) = 0;

	void newTarget(Building* target);

	void moveTo(glm::vec2 cellPos);
	void moveTo(glm::vec2 targetCen, int targetDim);

	void followPath(double deltaT);
	double getDeltaT() { return this->deltaT; }

};