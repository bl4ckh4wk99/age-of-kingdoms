#include "AttackingUnit.h"
#include "../Buildings/Building.h"

void AttackingUnit::buildBTree() {

	std::vector<TreeNode<AttackingUnit*>*> children;

						Condition<AttackingUnit*>* cond_targetInRange = new Condition<AttackingUnit*>(targetInRange);
						Action<AttackingUnit*>* act_ActAttack = new Action<AttackingUnit*>(attackTarget);

						children = { cond_targetInRange, act_ActAttack };

					Sequencer<AttackingUnit*>* seq_targetInRange = new Sequencer<AttackingUnit*>(children);
					
						Condition<AttackingUnit*>* cond_targetMoved = new Condition<AttackingUnit*>(targetMoved);
						Action<AttackingUnit*>* act_moveToTarget = new Action<AttackingUnit*>(moveToTarget);

						children = { cond_targetMoved, act_moveToTarget };

					Sequencer<AttackingUnit*>* seq_targetMoved = new Sequencer<AttackingUnit*>(children);

				children = { seq_targetInRange, seq_targetMoved };
				
			Selector<AttackingUnit*>* sel_Target = new Selector<AttackingUnit*>(children);

			Condition<AttackingUnit*>* cond_haveTarget = new Condition<AttackingUnit*>(haveTarget);

			children = { cond_haveTarget, sel_Target };

		Sequencer<AttackingUnit*>* seq_Target = new Sequencer<AttackingUnit*>(children);

			Condition<AttackingUnit*>* cond_havePath = new Condition<AttackingUnit*>(havePath);
			Action<AttackingUnit*>* act_followPath = new Action<AttackingUnit*>(continuePath);
			children = { cond_havePath, act_followPath };

		Sequencer<AttackingUnit*>* seq_Path = new Sequencer<AttackingUnit*>(children);
			
			Condition<AttackingUnit*>* cond_EnemeyVisible= new Condition<AttackingUnit*>(enemyVisible);
			children = { cond_EnemeyVisible/*, act_ActAttack */};

		Sequencer<AttackingUnit*>* seq_Visible = new Sequencer<AttackingUnit*>(children);

			Condition<AttackingUnit*>* cond_NoPath = new Condition<AttackingUnit*>(noPathOrTarget);
			Action<AttackingUnit*>* act_ActNone = new Action<AttackingUnit*>(statesToNone);

			children = { cond_NoPath, act_ActNone };

		Sequencer<AttackingUnit*>* seq_NoPath = new Sequencer<AttackingUnit*>(children);

		children = { seq_Path, seq_Target, seq_Visible, seq_NoPath };

	Selector<AttackingUnit*>* sel_BTree = new Selector<AttackingUnit*>(children);

	this->unitAI = BTree<AttackingUnit*>(sel_BTree);

}

void AttackingUnit::newTarget(Unit * target){
	this->targetUnit = target;
	this->targetBuilding = nullptr;
}

void AttackingUnit::newTarget(Building * target){
	this->targetBuilding = target;
	this->targetUnit = nullptr;
}

void AttackingUnit::followPath(double deltaT){
	if (!currentPath.empty()) {
		glm::vec2 nextCell = currentPath.front();

		//check validity of next cell
		if (worldGrid->isValid(nextCell)) {
			glm::vec3 targetPos = worldGrid->getCellCen(nextCell);

			glm::vec3 direction = glm::normalize(targetPos - worldPos);
			worldDir = direction;
			worldPos = worldPos + (direction * (float)(speed * deltaT));

			if (glm::length(worldPos - targetPos) <= 1) {
				worldGrid->occupyNewCell(cellPos, nextCell, team);
				cellPos = nextCell;
				currentPath.erase(currentPath.begin());
				//targetCell = cellPos;
			}
		}
		else {
			//is it occupied by a unit
			if (worldGrid->isOccupiedByUnit(nextCell)) {
				currentWait += deltaT;
				if (currentWait >= 1.0) {
					currentPath.clear();
					currentWait = 0.0;
					targetCell = cellPos;
				}
			}
			//if not occupied by unit then clear path so a new one is made
			else {
				currentWait = 0.0;
				currentPath.clear();
				targetCell = cellPos;
			}
		}
	}
	//currentWait = 0.0f;
}

/********************************Behaviour Tree***********************************/

bool havePath(AttackingUnit * unit){
	return (!unit->currentPath.empty());
}

void continuePath(AttackingUnit * unit){
	unit->followPath(unit->getDeltaT());
}

bool haveTarget(AttackingUnit * unit){
	return (unit->targetBuilding != nullptr || unit->targetUnit != nullptr);
}

bool targetInRange(AttackingUnit * unit){
	if (unit->targetUnit != nullptr)
		return (glm::distance(unit->targetUnit->cellPos, unit->cellPos) <= (unit->range + 1));
	if (unit->targetBuilding != nullptr)
		return (glm::distance(unit->targetBuilding->cellPos, unit->cellPos) <= (unit->range + 1) + (unit->targetBuilding->sizeInCells / 2));
	return false;
}

void attackTarget(AttackingUnit * unit){
	unit->currentPath.clear();
	unit->targetCell = unit->cellPos;
	unit->attack(unit->getDeltaT());
	unit->actionState = ATTACKING;
}

bool targetMoved(AttackingUnit * unit){
	if(unit->targetUnit != nullptr)
		return (unit->targetCell == unit->targetUnit->cellPos);
	if (unit->targetBuilding != nullptr)
		return (unit->targetBuilding->coversCell(unit->targetCell));
	return false;
}

void moveToTarget(AttackingUnit * unit){
	unit->currentPath.clear();
	if (unit->targetUnit != nullptr)
		unit->targetCell = unit->targetUnit->cellPos;
	if (unit->targetBuilding != nullptr)
		unit->targetCell = unit->targetBuilding->cellPos;
}

bool enemyVisible(AttackingUnit * unit){

	for (int pad = 1; pad < VISIBLE_RANGE; pad++) {
		//bottom left
		glm::vec2 enemyCellPos = glm::vec2(unit->cellPos.x - pad, unit->cellPos.y + pad);
		//go along bottom row
		for (int x = -pad; x < pad; x++) {
			enemyCellPos = glm::vec2(enemyCellPos.x + 1, enemyCellPos.y);
			if (unit->worldGrid->isInGrid(enemyCellPos)) {
				if (unit->worldGrid->isOccupiedByEnemyUnit(enemyCellPos, unit->team) || unit->worldGrid->isOccupiedByEnemyBuilding(enemyCellPos, unit->team)) {
					unit->targetCell = enemyCellPos;
					return true;
				}
			}
		}
		//go up right side
		for (int y = -pad; y < pad; y++) {
			enemyCellPos = glm::vec2(enemyCellPos.x, enemyCellPos.y - 1);
			if (unit->worldGrid->isInGrid(enemyCellPos)) {
				if (unit->worldGrid->isOccupiedByEnemyUnit(enemyCellPos, unit->team) || unit->worldGrid->isOccupiedByEnemyBuilding(enemyCellPos, unit->team)) {
					unit->targetCell = enemyCellPos;
					return true;
				}
			}
		}
		//go along top row
		for (int x = -pad; x < pad; x++) {
			enemyCellPos = glm::vec2(enemyCellPos.x - 1, enemyCellPos.y);
			if (unit->worldGrid->isInGrid(enemyCellPos)) {
				if (unit->worldGrid->isOccupiedByEnemyUnit(enemyCellPos, unit->team) || unit->worldGrid->isOccupiedByEnemyBuilding(enemyCellPos, unit->team)) {
					unit->targetCell = enemyCellPos;
					return true;
				}
			}
		}
		//go down left side
		for (int y = -pad; y < pad; y++) {
			enemyCellPos = glm::vec2(enemyCellPos.x, enemyCellPos.y + 1);
			if (unit->worldGrid->isInGrid(enemyCellPos)) {
				if (unit->worldGrid->isOccupiedByEnemyUnit(enemyCellPos, unit->team) || unit->worldGrid->isOccupiedByEnemyBuilding(enemyCellPos, unit->team) ) {
					unit->targetCell = enemyCellPos;
					return true;
				}
			}
		}
	}

	return false;

	/*bool enemyFound = false;
	float closest = 1000;

	for (int y = -VISIBLE_RANGE; y <= VISIBLE_RANGE; y++) {
		for (int x = -VISIBLE_RANGE; x <= VISIBLE_RANGE; x++) {

			glm::vec2 enemyCellPos = glm::vec2(unit->cellPos.x + x, unit->cellPos.y + y);

			if (unit->worldGrid->isInGrid(enemyCellPos)) {

				if (unit->worldGrid->isOccupiedByEnemyUnit(enemyCellPos, unit->team)) {
					enemyFound = true;
					if (glm::distance(enemyCellPos, unit->cellPos) < closest) {

						unit->targetCell = enemyCellPos;

					}
				}
				if (unit->worldGrid->isOccupiedByEnemyBuilding(enemyCellPos, unit->team)) {
					enemyFound = true;
					if (glm::distance(enemyCellPos, unit->cellPos) < closest) {

						unit->targetCell = enemyCellPos;

					}
				}
			}
		}
	}
	return enemyFound;*/
}

bool noPathOrTarget(AttackingUnit * unit){
	return (unit->currentPath.empty() && unit->targetUnit == nullptr && unit->targetBuilding == nullptr);
}

void statesToNone(AttackingUnit * unit){
	unit->actionState = NONE;
	unit->state = IDLE;
}
