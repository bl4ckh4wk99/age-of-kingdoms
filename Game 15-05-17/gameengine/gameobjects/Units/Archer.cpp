#include "Archer.h"
#include "../Buildings/Building.h"

Archer::Archer(AssetLibrary* assets){

	this->assets = assets;
	this->animationShader = assets->getAnimatedShader();
	this->model = assets->getArcher();

	this->barShader = assets->getBarShader();

	arrow = Arrow(assets);

	buildBTree();

	selected = false;
	this->maxHealth = ARCHER_HEALTH;
	this->health = ARCHER_HEALTH;
	this->damage = ARCHER_DAMAGE;
	this->range = ARCHER_RANGE;
	this->cost = UNIT_COSTS[ARCHER];
	this->speed = ARCHER_SPEED;
	this->attackSpeed = ARCHER_ATTACK_SPEED;

	targetUnit = nullptr;
	targetBuilding = nullptr;

	glGenVertexArrays(1, &this->healthVAO);
	glGenBuffers(2, this->healthVBO);
}

void Archer::Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team){

	this->cellPos = cellPos;
	this->targetCell = cellPos;
	this->worldPos = gameWorld->getCellCen(cellPos);
	this->worldDir = glm::vec3(0, 0, 1);
	this->worldGrid = gameWorld;
	this->unitType = unitType;
	this->team = team;
	//Update world map so cell is occupied
	worldGrid->occupyNewCell(glm::vec2(-1,-1),cellPos, team);

	state = IDLE;
	actionState = NONE;
	attackRate = 0.0;

	this->pathFinder = AStar(gameWorld);

	this->transform = Transform();

	transform.setPos(worldPos);
}

void Archer::Draw(Camera* cam){

	animationShader->Bind();

	animationShader->Update(&transform, cam);

	model->BindModelsVAO();
	model->RenderModel();


	/*Animation Stuff*/
	//vector<glm::mat4> Transforms;
	//model->BoneTransform(runningTime, Transforms);
	//
	//for (uint i = 0; i < Transforms.size(); i++) {
	//	animationShader->UpdateBones(i, Transforms[i]);
	//}
	//model->Render();

	drawHealthBar(cam);

	//draw any arrows
	arrow.Draw(cam);

}

void Archer::Update(double deltaT){
	this->deltaT = deltaT;
	unitAI.execute(this);
	transform.setPos(worldPos);

	//rotate to face current direction
	float rotAngle = acos(glm::dot(glm::vec3(0, 0, 1), worldDir));
	//convert to degrees
	if (worldDir.x >= 0) {
		rotAngle = rotAngle * 57.2173304937f;
	}
	else {
		rotAngle = -rotAngle * 57.2173304937f;
	}

	transform.setRot(glm::vec3(0, rotAngle, 0));

	//if we have fired an arrow
	if (arrowFired) {
		state = ACTION;
		//update arrow
		attackRate += deltaT;
		arrow.Update(deltaT);
		//check to see if arrow has landed
		if (arrow.endOfPath()) {
			if (targetUnit != nullptr) {
				//do damage
				targetUnit->health -= damage;
				//check to see if target is dead
				if (targetUnit->health <= 0) {
					arrowFired = false;
					targetUnit = nullptr;
					targetCell = this->cellPos;
				}
			}
			if (targetBuilding != nullptr) {
				//do damage
				targetBuilding->health -= damage;
				//check to see if target is dead
				if (targetBuilding->health <= 0) {
					arrowFired = false;
					targetBuilding = nullptr;
					targetCell = this->cellPos;
				}
			}
			arrowFired = false;
		}
	}
	if (attackRate <= attackSpeed) {
		attackRate += deltaT;
	}

}


void Archer::attack(double deltaT) {

	if (targetUnit != nullptr) {
		//get and set target direction
		glm::vec3 direction = glm::normalize(targetUnit->worldPos - worldPos);
		worldDir = direction;
		//if havent already fired an arrow and attack rate is good
		if (!arrowFired && attackRate > attackSpeed) {
			//fire an arrow
			arrow.Init(worldPos, targetUnit->worldPos);
			//play arrow sound
			assets->playArcherAttack();
			arrowFired = true;
			attackRate = 0;
		}
	}
	else {
		if (targetBuilding != nullptr) {
			//get and set target direction
			glm::vec3 direction = glm::normalize(targetBuilding->worldPos - worldPos);
			worldDir = direction;
			//if havent already fired an arrow and attack rate is good
			if (!arrowFired && attackRate > attackSpeed) {
				//fire an arrow
				arrow.Init(worldPos, targetBuilding->worldPos);
				//play arrow sound
				assets->playArcherAttack();
				arrowFired = true;
				attackRate = 0;
			}
		}
	}
}
