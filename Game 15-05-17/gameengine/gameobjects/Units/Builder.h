#pragma once

#include "Unit.h"

#define BUILDER_HEALTH 50.0f

#define BUILDER_SPEED 10.0f

class Builder : public Unit {

private:

	void build(double deltaT);

public:
	Builder(AssetLibrary* assets);

	void Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team);

	void Draw(Camera* cam);

	void Update(double deltaT);


};
