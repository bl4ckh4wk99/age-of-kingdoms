#include "Unit.h"

void Unit::followPath(double deltaT){

	if (!currentPath.empty()) {
		glm::vec2 nextCell = currentPath.front();

		//check validity of next cell
		if (worldGrid->isValid(nextCell)) {
			glm::vec3 targetPos = worldGrid->getCellCen(nextCell);

			//set current direction
			glm::vec3 direction = glm::normalize(targetPos - worldPos);
			worldDir = direction;
			worldPos = worldPos + (direction * (float)(speed * deltaT));

			//if we are in next cell
			if (glm::length(worldPos - targetPos) <= 1) {
				worldGrid->occupyNewCell(cellPos, nextCell, team);
				cellPos = nextCell;
				currentPath.erase(currentPath.begin());
			}
		}
		else {
			//is it occupied by a unit
			if (worldGrid->isOccupiedByUnit(nextCell)) {
				currentWait += deltaT;
				if (currentWait >= 1.0) {
					currentPath.clear();
					currentWait = 0.0;
				}
			}
			//if not occupied by unit then clear path so a new one is made
			else {
				currentWait = 0.0;
				currentPath.clear();
			}
		}
	}
}

//set up and draw health bar
void Unit::drawHealthBar(Camera * cam){

	glm::vec3 cenHealth = worldPos + (10.0f * cam->getUp());

	healthBars[0] = cenHealth + (-2.5f * cam->getRight());
	float percentHealth = (5 * (this->health / this->maxHealth));
	healthBars[1] = cenHealth + ((-2.5f + percentHealth) * cam->getRight());

	healthBars[2] = cenHealth + (-2.5f * cam->getRight());
	healthBars[3] = cenHealth + (2.5f * cam->getRight());

	switch (this->team) {
	case ONE: {
		healthCol[0] = glm::vec3(0, 1, 0);
		healthCol[1] = glm::vec3(0, 1, 0);
		healthCol[2] = glm::vec3(0, 0, 0);
		healthCol[3] = glm::vec3(0, 0, 0);
		break;
	}
	case TWO: {
		healthCol[0] = glm::vec3(1, 0, 0);
		healthCol[1] = glm::vec3(1, 0, 0);
		healthCol[2] = glm::vec3(0, 0, 0);
		healthCol[3] = glm::vec3(0, 0, 0);
		break;
	}
	}

	glBindVertexArray(this->healthVAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->healthVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*healthBars, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, this->healthVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*healthCol, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);


	glBindVertexArray(0);

	barShader->Bind();

	glLineWidth(5);

	barShader->Update(&transform, cam);

	glBindVertexArray(this->healthVAO);

	glDrawArrays(GL_LINES, 0, sizeof(glm::vec3) * 4);

	glBindVertexArray(0);
}

//set a new target building
void Unit::newTarget(Building * target){
	this->targetBuilding = target;
}

//move to a given cell
void Unit::moveTo(glm::vec2 cellPos){
	//check not already there
	if (this->cellPos != cellPos) {
		this->targetCell = cellPos;
		currentPath.clear();
		currentPath = pathFinder.generatePath(this->cellPos, cellPos);
		pathFinder.cleanup();

		vector<glm::vec2>::iterator iter = currentPath.end();
		std::advance(iter, -1);
		targetCell = *iter;

		if(!currentPath.empty())
			state = MOVING;
	}
}

//move to given cell thats occupied by a target of size targetDim
void Unit::moveTo(glm::vec2 cellPos, int targetDim){
	//check not already there
	if (this->cellPos != cellPos) {
		this->targetCell = cellPos;
		currentPath.clear();
		currentPath = pathFinder.generatePath(this->cellPos, cellPos, targetDim);
		pathFinder.cleanup();

		vector<glm::vec2>::iterator iter = currentPath.end();
		std::advance(iter, -1);
		targetCell = *iter;
		if (!currentPath.empty())
			state = MOVING;
	}
}
