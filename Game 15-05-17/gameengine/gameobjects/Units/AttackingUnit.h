#pragma once

#include "Unit.h"
#include "../../BehaviourTree/BTree.h"

#define VISIBLE_RANGE 6

enum UnitAction{NONE, PATROL, EXPLORE, ATTACKING};

class AttackingUnit : public Unit {

protected:

	BTree<AttackingUnit*> unitAI;

public:

	float damage;
	int range;
	double attackRate;
	float attackSpeed;

	UnitAction actionState;

	Unit* targetUnit;

	virtual void Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team) = 0;

	virtual void Draw(Camera* cam) = 0;

	virtual void Update(double deltaT) = 0;

	//virtual void moveTo(glm::vec2 cellPos) = 0;

	void buildBTree();

	void newTarget(Unit* target);
	void newTarget(Building* target);

	virtual void attack(double deltaT) = 0;

	void followPath(double deltaT);

};

/********************************Behaviour Tree***********************************/

static bool haveTarget(AttackingUnit* unit);

static bool targetInRange(AttackingUnit* unit);

static void attackTarget(AttackingUnit* unit);

static bool targetMoved(AttackingUnit* unit);

static void moveToTarget(AttackingUnit* unit);

static bool havePath(AttackingUnit* unit);

static void continuePath(AttackingUnit* unit);

static bool enemyVisible(AttackingUnit* unit);

static bool noPathOrTarget(AttackingUnit* unit);

static void statesToNone(AttackingUnit* unit);
