#include "Swordsman.h"
#include "../Buildings/Building.h"

Swordsman::Swordsman(AssetLibrary* assets) {

	this->assets = assets;

	this->animationShader = assets->getAnimatedShader();
	this->model = assets->getSword();

	this->barShader = assets->getBarShader();

	buildBTree();

	selected = false;
	this->maxHealth = SWORD_HEALTH;
	this->health = SWORD_HEALTH;
	this->damage = SWORD_DAMAGE;
	this->range = SWORD_RANGE;
	this->cost = UNIT_COSTS[SWORDSMAN];
	this->speed = SWORD_SPEED;
	this->attackSpeed = SWORD_ATTACK_SPEED;
	targetUnit = nullptr;
	targetBuilding = nullptr;

	glGenVertexArrays(1, &this->healthVAO);
	glGenBuffers(2, this->healthVBO);
}

void Swordsman::Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team) {

	this->cellPos = cellPos;
	this->targetCell = cellPos;
	this->worldPos = gameWorld->getCellCen(cellPos);
	this->worldDir = glm::vec3(0, 0, 1);
	this->worldGrid = gameWorld;
	this->unitType = unitType;
	this->team = team;
	//Update world map so cell is occupied
	worldGrid->occupyNewCell(glm::vec2(-1, -1), cellPos, team);

	state = IDLE;
	actionState = NONE;
	attackRate = 0.0;

	this->pathFinder = AStar(gameWorld);

	this->transform = Transform();

	transform.setPos(worldPos);
}

void Swordsman::Draw(Camera* cam) {

	animationShader->Bind();

	animationShader->Update(&transform, cam);

	model->BindModelsVAO();
	model->RenderModel();


	/*ANIMATION STUFF*/
	//vector<glm::mat4> Transforms;
	//model->BoneTransform(runningTime, Transforms);
	//
	//for (uint i = 0; i < Transforms.size(); i++) {
	//	animationShader->UpdateBones(i, Transforms[i]);
	//}
	//model->Render();

	drawHealthBar(cam);
}

void Swordsman::Update(double deltaT) {
	this->deltaT = deltaT;
	//execute unit AI behaviour tree
	unitAI.execute(this);

	transform.setPos(worldPos);

	//rotate to face current direction
	float rotAngle = acos(glm::dot(glm::vec3(0, 0, 1), worldDir));
	//convert to degrees
	if (worldDir.x >= 0) {
		rotAngle = rotAngle * 57.2173304937f;
	}
	else {
		rotAngle = -rotAngle * 57.2173304937f;
	}

	transform.setRot(glm::vec3(0, rotAngle, 0));

}

void Swordsman::attack(double deltaT) {

	if (targetUnit != nullptr) {
		//Check in range
		if (glm::length(targetUnit->cellPos - cellPos) <= (range + 1)) {
			state = ACTION;
			attackRate += deltaT;
			//check attack speed
			if (attackRate > attackSpeed) {
				//do damage
				targetUnit->health -= damage;
				//play sound
				assets->playSwordAttack();
				attackRate = 0.0;
				//check if target is dead
				if (targetUnit->health <= 0) {
					targetUnit = nullptr;
					targetCell = this->cellPos;
				}
			}
		}
	}
	if (targetBuilding != nullptr) {
		//Check in range
		if (glm::length(targetBuilding->cellPos - cellPos) <= (range + 1) + (targetBuilding->sizeInCells / 2)) {
			state = ACTION;
			attackRate += deltaT;
			//check attack speed
			if (attackRate > attackSpeed) {
				//do damage
				targetBuilding->health -= damage;
				//play sound
				assets->playSwordAttack();
				attackRate = 0.0;
				//check if target is dead
				if (targetBuilding->health <= 0) {
					targetBuilding = nullptr;
					targetCell = this->cellPos;
				}
			}
		}
	}
}

