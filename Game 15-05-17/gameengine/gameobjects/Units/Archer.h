#pragma once

#include "AttackingUnit.h"
#include "../Primitives/Arrow.h"

#define ARCHER_HEALTH 50.0f
#define ARCHER_DAMAGE 15.0f
#define ARCHER_RANGE 4

#define ARCHER_SPEED 10.0f
#define ARCHER_ATTACK_SPEED 3.0f

class Archer : public AttackingUnit{
private:

	Arrow arrow;
	bool arrowFired = false;
public:

	Archer(AssetLibrary* assets);

	void Init(glm::vec2 cellPos, Grid* gameWorld, UnitType unitType, Team team);

	void Draw(Camera* cam);

	void Update(double deltaT);

	void attack(double deltaT);

};
