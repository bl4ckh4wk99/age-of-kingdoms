#pragma once

#include "Building.h"

#define BARRACKS_HEALTH 250.0f
#define BARRACKS_BUILD_TIME 15

#define BARRACKS_INCOME 0

class Barracks : public Building {


public:

	Barracks(Model* barracksModel, BuildingShader* buildingShader, BarShader* barShader);

	void Init(glm::vec2 cellPos, Grid* gameWorld, Team team);
	void Draw(Camera* cam);
	void Update(double deltaT);

	glm::vec2 exitCell();

};
