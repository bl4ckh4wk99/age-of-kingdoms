#pragma once

#include "Building.h"

#define MINE_HEALTH 100.0f
#define MINE_BUILD_TIME 10

#define MINE_INCOME 15

class Mine : public Building {


public:

	Mine(Model* mineModel, BuildingShader* buildingShader, BarShader* barShader);

	void Init(glm::vec2 cellPos, Grid* gameWorld, Team team);
	void Draw(Camera* cam);
	void Update(double deltaT);

	glm::vec2 exitCell() { return glm::vec2(0, 0); }

};
