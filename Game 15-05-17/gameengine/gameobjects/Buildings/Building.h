#pragma once

#include "../World/Grid.h"
#include "../../ObjectLoading/Model.h"
#include "../Units/Unit.h"

#include "../Shader/BarShader.h"
#include "../Shader/BuildingShader.h"

#include <queue>

enum BuildingType { THRONE, MINE, BARRACKS, UNKNOWN_BUILDING };

enum BuildingStatus { PLACING, BUILDING, BUILT, PRODUCING};

#define STARTING_ALPHA 0.2f

const int BUILDING_COSTS[] = { 400, 100, 300 };

const int BUILDING_SIZES[] = { 3,1,3 };

class Building { 
protected:

	BuildingShader* shader;
	Model* model;

	float buildAlpha;

	Grid* worldGrid;
	Transform transform;

	void drawHealthBar(Camera* cam);

	BarShader* barShader;
	glm::vec3 statusBars[8];
	glm::vec3 statusCol[8];

	GLuint statusVAO, statusVBO[2];

public:
	glm::vec3 worldPos;
	glm::vec2 cellPos;
	int sizeInCells;

	float maxHealth;
	float health;
	int buildTime;
	int cost;
	int income;

	Team team;

	BuildingStatus state;
	double currentBuildTime;

	bool selected;
	BuildingType type;

	vector<UnitType> currentlyBuilding;
	UnitType curProduction;
	double prodFinish;
	double prodTime;

	virtual void Init(glm::vec2 cellPos, Grid* gameWorld, Team team) = 0;
	virtual void Draw(Camera* cam) = 0;
	virtual void Update(double deltaT) = 0;

	virtual glm::vec2 exitCell() = 0;

	void placingBuilding(glm::vec2 cellPos);
	void startBuilding();
	bool checkBuild();
	void instaBuild();
	void startProduction(UnitType type);
	bool removeProduction(UnitType type);
	bool checkProduction(bool popSpace);
	bool coversCell(glm::vec2 selectedCell);

};