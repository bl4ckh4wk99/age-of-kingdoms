#include "Building.h"

void Building::drawHealthBar(Camera * cam){

	/***************************HEALTH BAR****************************/
	glm::vec3 cenStatus = worldPos + (15.0f * cam->getUp());//glm::vec3(0, 30, 0);

	statusBars[0] = cenStatus + (-10.0f * cam->getRight());
	float percentHealth = (20 * (this->health / this->maxHealth));
	statusBars[1] = cenStatus + ((-10 + percentHealth) * cam->getRight());
	statusBars[2] = cenStatus + (-10.0f * cam->getRight());
	statusBars[3] = cenStatus + (10.0f * cam->getRight());

	switch (this->team) {
	case ONE: {
		statusCol[0] = glm::vec3(0, 1, 0);
		statusCol[1] = glm::vec3(0, 1, 0);
		statusCol[2] = glm::vec3(0, 0, 0);
		statusCol[3] = glm::vec3(0, 0, 0);
		break;
	}
	case TWO: {
		statusCol[0] = glm::vec3(1, 0, 0);
		statusCol[1] = glm::vec3(1, 0, 0);
		statusCol[2] = glm::vec3(0, 0, 0);
		statusCol[3] = glm::vec3(0, 0, 0);
		break;
	}
	}

	/***************************PRODUCTION BAR****************************/
	if (state == BUILDING || state == PRODUCING) {
		cenStatus = worldPos + (10.0f * cam->getUp());

		statusBars[4] = cenStatus + (-10.0f * cam->getRight());

		double percentBuilt = 0.0;
		if (state == BUILDING)
			percentBuilt = (20 * (currentBuildTime / buildTime));
		else
			percentBuilt = (20 * (prodTime / prodFinish));

		statusBars[5] = cenStatus + ((-10.0f + (float)percentBuilt) * cam->getRight());
		statusBars[6] = cenStatus + (-10.0f * cam->getRight());
		statusBars[7] = cenStatus + (10.0f * cam->getRight());

		statusCol[4] = glm::vec3(1, 1, 1);
		statusCol[5] = glm::vec3(1, 1, 1);
		statusCol[6] = glm::vec3(0, 0, 0);
		statusCol[7] = glm::vec3(0, 0, 0);

		glBindVertexArray(this->statusVAO);

		glBindBuffer(GL_ARRAY_BUFFER, this->statusVBO[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 8, &*statusBars, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

		glBindBuffer(GL_ARRAY_BUFFER, this->statusVBO[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 8, &*statusCol, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);
	}
	else {
		glBindVertexArray(this->statusVAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->statusVBO[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*statusBars, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

		glBindBuffer(GL_ARRAY_BUFFER, this->statusVBO[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*statusCol, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);
	}


	glBindVertexArray(0);

	barShader->Bind();

	glLineWidth(5);

	barShader->Update(&transform, cam);

	glBindVertexArray(this->statusVAO);
	if (state == BUILDING || state == PRODUCING) {
		glDrawArrays(GL_LINES, 0, sizeof(glm::vec3) * 8);
	}
	else {
		glDrawArrays(GL_LINES, 0, sizeof(glm::vec3) * 4);
	}

	glBindVertexArray(0);
}

void Building::placingBuilding(glm::vec2 cellPos){

	if (state == PLACING) {

		this->cellPos = cellPos;
		this->worldPos = worldGrid->getCellCen(cellPos);
		this->transform.setPos(this->worldPos);
	}

}

void Building::startBuilding(){
	//Update world map so cell is occupied
	worldGrid->occupyNewCells(cellPos, sizeInCells, team);
	this->worldPos = worldGrid->getCellCen(cellPos);
	this->transform.setPos(this->worldPos);

	state = BUILDING;
}

bool Building::checkBuild() {
	if (currentBuildTime >= buildTime) {
		state = BUILT;
		buildAlpha = 1.0f;
		return true;
	}
	else {
		float buildPercent = currentBuildTime / buildTime;
		buildAlpha = STARTING_ALPHA + (0.8f * buildPercent);
		return false;
	}
}

void Building::instaBuild(){
	//Update world map so cell is occupied
	worldGrid->occupyNewCells(cellPos, sizeInCells, team);
	state = BUILT;
	buildAlpha = 1.0f;
}

void Building::startProduction(UnitType type){

	if (currentlyBuilding.empty()) {
		curProduction = type;
		currentlyBuilding.push_back(type);
		prodTime = 0;
		prodFinish = UNIT_TIMES[type];
		state = PRODUCING;
	}
	else {
		currentlyBuilding.push_back(type);
	}
}

bool Building::removeProduction(UnitType type){
	
	bool removed = false;

	for (int i = currentlyBuilding.size() - 1; i >= 0; i--) {
		if (currentlyBuilding[i] == type) {
			currentlyBuilding.erase(currentlyBuilding.begin() + i);
			removed = true;
			break;
		}
	}

	if (removed) {
		if (currentlyBuilding.empty()) {
			state = BUILT;
		}
		else {
			if (curProduction != currentlyBuilding.front()) {
				curProduction = currentlyBuilding.front();
				prodTime = 0;
				prodFinish = UNIT_TIMES[currentlyBuilding.front()];
				state = PRODUCING;
			}
		}
	}

	return removed;
}

bool Building::checkProduction(bool popSpace) {
	if (prodTime >= prodFinish) {
		if (popSpace) {
			currentlyBuilding.erase(currentlyBuilding.begin());
			if (currentlyBuilding.empty())
				state = BUILT;
			else {
				curProduction = currentlyBuilding.front();
				prodTime = 0;
				prodFinish = UNIT_TIMES[curProduction];
			}
			return true;
		}
		else {
			prodTime = prodFinish;
			return false;
		}
	}
	return false;
}

bool Building::coversCell(glm::vec2 selectedCell){
	if (glm::length(cellPos.x - selectedCell.x) > (sizeInCells / 2))
		return false;
	if (glm::length(cellPos.y - selectedCell.y) > (sizeInCells / 2))
		return false;
	return true;
	
}
