#include "Barracks.h"

Barracks::Barracks(Model * barracksModel, BuildingShader * buildingShader, BarShader * barShader){
	shader = buildingShader;
	this->barShader = barShader;

	model = barracksModel;

	selected = false;
	sizeInCells = BUILDING_SIZES[BARRACKS];
	type = BARRACKS;
	currentBuildTime = 0.0f;
	buildAlpha = STARTING_ALPHA;

	this->maxHealth = BARRACKS_HEALTH;
	this->health = BARRACKS_HEALTH;
	this->buildTime = BARRACKS_BUILD_TIME;
	this->cost = BUILDING_COSTS[BARRACKS];
	this->income = BARRACKS_INCOME;

	glGenVertexArrays(1, &this->statusVAO);
	glGenBuffers(2, this->statusVBO);
}

void Barracks::Init(glm::vec2 cellPos, Grid * gameWorld, Team team){
	this->cellPos = cellPos;
	this->worldPos = gameWorld->getCellCen(cellPos);
	this->team = team;

	this->worldGrid = gameWorld;

	state = PLACING;

	this->transform = Transform();
}

void Barracks::Draw(Camera * cam){
	// Enable blending and alpha
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	shader->Bind();

	shader->Update(buildAlpha, &transform, cam);

	model->BindModelsVAO();
	model->RenderModel();

	glDisable(GL_BLEND);

	drawHealthBar(cam);
}

void Barracks::Update(double deltaT){
	if (state == PRODUCING) {
		prodTime += deltaT;
	}
}

glm::vec2 Barracks::exitCell(){

	int pad = 1;

	while (true) {
		//bottom left
		glm::vec2 exit = glm::vec2(cellPos.x - (float)((sizeInCells / 2) + pad), cellPos.y + (float)((sizeInCells / 2) + pad));
		//go along bottom row
		for (int x = -((sizeInCells / 2) + pad); x < ((sizeInCells / 2) + pad); x++) {
			exit = glm::vec2(exit.x + 1, exit.y);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go up right side
		for (int y = -((sizeInCells/2) + pad); y < ((sizeInCells / 2) + pad); y++) {
			exit = glm::vec2(exit.x, exit.y - 1);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go along top row
		for (int x = -((sizeInCells / 2) + pad); x < ((sizeInCells / 2) + pad); x++) {
			exit = glm::vec2(exit.x - 1, exit.y);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go down left side
		for (int y = -((sizeInCells / 2) + pad); y < ((sizeInCells / 2) + pad); y++) {
			exit = glm::vec2(exit.x, exit.y + 1);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		pad++;
	}
}
