#include "Throne.h"

Throne::Throne(Model* throneModel, BuildingShader * buildingShader, BarShader* barShader){
	shader = buildingShader;
	this->barShader = barShader;

	model = throneModel;

	selected = false;
	sizeInCells = BUILDING_SIZES[THRONE];
	type = THRONE;
	currentBuildTime = 0.0f;
	buildAlpha = STARTING_ALPHA;

	this->maxHealth = THRONE_HEALTH;
	this->health = THRONE_HEALTH;
	this->buildTime = BUILD_TIME;
	this->cost = BUILDING_COSTS[THRONE];
	this->income = THRONE_INCOME;

	glGenVertexArrays(1, &this->statusVAO);
	glGenBuffers(2, this->statusVBO);
}

void Throne::Init(glm::vec2 cellPos, Grid * gameWorld, Team team){

	this->cellPos = cellPos;
	worldPos = gameWorld->getCellCen(cellPos);
	this->team = team;

	worldGrid = gameWorld;

	state = PLACING;

	transform = Transform();
	transform.setPos(worldPos);

}

void Throne::Draw(Camera * cam){

	// Enable blending and alpha
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	shader->Bind();

	shader->Update(buildAlpha, &transform, cam);

	model->BindModelsVAO();
	model->RenderModel();

	glDisable(GL_BLEND);

	drawHealthBar(cam);
}

void Throne::Update(double deltaT){

	if (state == PRODUCING) {
		prodTime += deltaT;
	}
}

glm::vec2 Throne::exitCell() {

	int pad = 1;

	while (true) {
		//bottom left
		glm::vec2 exit = glm::vec2(cellPos.x - (float)((sizeInCells / 2) + pad), cellPos.y + (float)((sizeInCells / 2) + pad));
		//go along bottom row
		for (int x = -((sizeInCells / 2) + pad); x < ((sizeInCells / 2) + pad); x++) {
			exit = glm::vec2(exit.x + 1, exit.y);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go up right side
		for (int y = -((sizeInCells / 2) + pad); y < ((sizeInCells / 2) + pad); y++) {
			exit = glm::vec2(exit.x, exit.y - 1);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go along top row
		for (int x = -((sizeInCells / 2) + pad); x < ((sizeInCells / 2) + pad); x++) {
			exit = glm::vec2(exit.x - 1, exit.y);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		//go down left side
		for (int y = -((sizeInCells / 2) + pad); y < ((sizeInCells / 2) + pad); y++) {
			exit = glm::vec2(exit.x, exit.y + 1);
			if (worldGrid->isValid(exit)) {
				return exit;
			}
		}
		pad++;
	}
}
