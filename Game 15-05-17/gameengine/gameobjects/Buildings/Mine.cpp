#include "Mine.h"

Mine::Mine(Model * mineModel, BuildingShader * buildingShader, BarShader * barShader){
	shader = buildingShader;
	this->barShader = barShader;

	model = mineModel;

	selected = false;
	sizeInCells = BUILDING_SIZES[MINE];
	type = MINE;
	currentBuildTime = 0.0f;
	buildAlpha = STARTING_ALPHA;

	this->maxHealth = MINE_HEALTH;
	this->health = MINE_HEALTH;
	this->buildTime = MINE_BUILD_TIME;
	this->cost = BUILDING_COSTS[MINE];
	this->income = MINE_INCOME;

	glGenVertexArrays(1, &this->statusVAO);
	glGenBuffers(2, this->statusVBO);
}

void Mine::Init(glm::vec2 cellPos, Grid * gameWorld, Team team){
	this->cellPos = cellPos;
	this->worldPos = gameWorld->getCellCen(cellPos);
	this->team = team;

	this->worldGrid = gameWorld;

	state = PLACING;

	this->transform = Transform();
}

void Mine::Draw(Camera * cam){
	// Enable blending and alpha
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	shader->Bind();

	shader->Update(buildAlpha, &transform, cam);

	model->BindModelsVAO();
	model->RenderModel();

	glDisable(GL_BLEND);

	drawHealthBar(cam);
}

void Mine::Update(double deltaT){
	if (state == PRODUCING) {
		prodTime += deltaT;
	}
}
