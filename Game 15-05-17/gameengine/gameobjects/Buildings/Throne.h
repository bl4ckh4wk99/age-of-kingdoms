#pragma once

#include "Building.h"

#define THRONE_HEALTH 500.0f
#define BUILD_TIME 30

#define THRONE_INCOME 10

class Throne : public Building {

	
public:

	Throne(Model* throneModel, BuildingShader* buildingShader, BarShader* barShader);

	void Init(glm::vec2 cellPos, Grid* gameWorld, Team team);
	void Draw(Camera* cam);
	void Update(double deltaT);

	glm::vec2 exitCell();

};
