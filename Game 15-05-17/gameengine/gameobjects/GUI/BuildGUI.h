#pragma once

#include "SelectedGUI.h"
#include "GUI Primitives\Button.h"

#define BUILD_BORDER 0.02f
#define BUILD_ICON_SIZE 0.075f

#define START_X ICON_MIN_X + (ICON_SIZE /2)
#define START_Y ICON_MIN_Y - 0.12f

#define TEXT_X START_X - 0.03f
#define TEXT_Y 0.82f

class BuildGUI : public SelectedGUI {
protected:
	//Buttons
	vector<Button*> buttons;

	vector<UnitType> *producing;
	Text2D textWriter;

	BuildingType buildingType;

public:
	BuildGUI(AssetLibrary* assets);
	BuildGUI() {}
	~BuildGUI();

	void Init(UnitType type);
	void Init(BuildingType type, vector<UnitType>* producing);
	void Draw() {}
	void Draw(int popCap);
//	void Draw(int popCap);
	void Update(int gold, int pop);

	void deSelectButtons();

	void checkButtonHighlights(glm::vec4 normalisedCoords);
	BuildingType checkBuildersButtons(glm::vec4 normalisedCoords, int curResources);
	UnitType checkBuildingButtons(glm::vec4 normalisedCoords, int curResources, bool popSpace, bool leftClick);
	bool checkGUIcollision(glm::vec4 normalisedCoords);
};