#include "MenuGUI.h"

MenuGUI::MenuGUI(AssetLibrary* assets) {
	//Create basic background mesh and tex here
	this->shader = assets->getGUIShader();

	menuBackground = new Mesh(-1, -1, 1, 1);

	menuBackgroundTex = assets->getMenuBackgroundTex();
	
	Texture* PvAITex = assets->getMenuPvAITex();
	PvAIButt = new Button(glm::vec2(-0.3, -0.5), glm::vec2(0.5, 0.25), PvAITex, shader);
	Texture* AIvAITex = assets->getMenuAIvAITex();
	AIvAIButt = new Button(glm::vec2(0.3, -0.5), glm::vec2(0.5, 0.25), AIvAITex, shader);

}

MenuGUI::~MenuGUI()
{
}

void MenuGUI::Init() {



}

void MenuGUI::Draw(/*glm::vec4 normalisedCoords*/) {

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Draw background gui here
	shader->Bind();
	shader->Update(0);

	//Draw GUI background
	menuBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	menuBackground->Draw();

	//draw buttons
	PvAIButt->Draw();
	AIvAIButt->Draw();


	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}



void MenuGUI::Update() {

}


//Method that checks the buttons for collision with the mouse
Button* MenuGUI::checkButtonCollision(glm::vec4 normalisedCoords) {

	//reset button selection
	PvAIButt->isSelected = CLEAR;
	AIvAIButt->isSelected = CLEAR;

	Box test = PvAIButt->button->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
		return PvAIButt;
	}
	test = AIvAIButt->button->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
		return AIvAIButt;
	}
}

//Method that checks the GUI background for collision with the mouse
bool MenuGUI::checkGUIcollision(glm::vec4 normalisedCoords) {
	Box test = menuBackground->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	return (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY);
}