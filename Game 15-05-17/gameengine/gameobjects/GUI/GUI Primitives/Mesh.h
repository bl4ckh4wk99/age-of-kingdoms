#pragma once

#include <gl/glew.h>
#include <glm/glm.hpp>
#include "Box.h"
#include "../OpenGLHelper.h"

class Mesh
{
public:
	Mesh() {}
	Mesh(glm::vec2 cen, glm::vec2 size);
	Mesh(float minX, float minY, float maxX, float maxY);

	void Draw();

	inline Box getAABB() { return this->AABB; }
	bool AABBCollision(glm::vec3 point);

private:

	Box AABB;

	glm::vec2 *points, *texPoints;

	GLuint vao;
	GLuint vbo[2];
};

