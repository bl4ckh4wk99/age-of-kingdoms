#include "Text2D.h"

Text2D::Text2D(Font* font, FontShader* shader)
{
	this->shader = shader;
	//tex = new Texture(filename);
	//this->font->charWidth = font->charWidth;
	this->font = font;
}

void Text2D::init()
{
	glGenVertexArrays(1, &VAO);
	
	glGenBuffers(1, &Text2DVertexBufferID);
	glGenBuffers(1, &Text2DUVBufferID);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, Text2DVertexBufferID);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glBindBuffer(GL_ARRAY_BUFFER, Text2DUVBufferID);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

	glBindVertexArray(0);
}

void Text2D::printString(const char * text, glm::vec2 normalPos, glm::vec2 screenDim, int size)
{

	glm::vec2 pos = glm::vec2(
		((normalPos.x + 1.0f) * screenDim.x) / 2.0f,	// x		
		((1.0f - normalPos.y) * screenDim.y) / 2.0f		// y
	);

	unsigned int length = strlen(text);

	// Fill buffers
	std::vector<glm::vec2> vertices;
	std::vector<glm::vec2> UVs;
	float posX = pos.x;
	for (unsigned int i = 0; i<length; i++) {
		posX += size * font->charWidth;
		glm::vec2 vertex_up_left = glm::vec2(posX, pos.y + size);
		glm::vec2 vertex_up_right = glm::vec2(posX + size*font->charWidth, pos.y + size);
		glm::vec2 vertex_down_right = glm::vec2(posX + size*font->charWidth, pos.y);
		glm::vec2 vertex_down_left = glm::vec2(posX, pos.y);

		vertices.push_back(vertex_up_left);
		vertices.push_back(vertex_down_left);
		vertices.push_back(vertex_up_right);

		vertices.push_back(vertex_down_right);
		vertices.push_back(vertex_up_right);
		vertices.push_back(vertex_down_left);

		char character = text[i];
		float uv_x = (character % 16) / 16.0f;
		float uv_y = (character / 16) / 16.0f;

		glm::vec2 uv_up_left = glm::vec2(uv_x, uv_y);
		glm::vec2 uv_up_right = glm::vec2(uv_x + font->charWidth / 16.0f, uv_y);
		glm::vec2 uv_down_right = glm::vec2(uv_x + font->charWidth / 16.0f, (uv_y + 1.0f / 16.0f));
		glm::vec2 uv_down_left = glm::vec2(uv_x, (uv_y + 1.0f / 16.0f));
		UVs.push_back(uv_up_left);
		UVs.push_back(uv_down_left);
		UVs.push_back(uv_up_right);

		UVs.push_back(uv_down_right);
		UVs.push_back(uv_up_right);
		UVs.push_back(uv_down_left);
	}
	glBindBuffer(GL_ARRAY_BUFFER, Text2DVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, Text2DUVBufferID);
	glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(glm::vec2), &UVs[0], GL_STATIC_DRAW);


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	// Bind shader
	shader->Bind();

	// Bind texture
	font->tex->Bind(0);
	// Set our "myTextureSampler" sampler to user Texture Unit 0
	shader->setTextureLocation(0);

	glBindVertexArray(VAO);

	// Draw call
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glBindVertexArray(0);
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}
