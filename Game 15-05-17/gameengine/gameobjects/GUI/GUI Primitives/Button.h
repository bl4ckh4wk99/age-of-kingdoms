#pragma once
#include "Mesh.h"
#include "Texture.h"
#include "../../../Shader/GUIShader.h"

enum ButtonSelected {CLEAR, HIGHLIGHTED, CLICKED, FAULT};

class Button {

public:

	glm::vec2 cen, size;

	Mesh* button;
	Texture* texture;

	GUIShader* shader;

	int isSelected;

	Button(glm::vec2 cen, glm::vec2 dim, Texture* tex, GUIShader* guiShader);

	void Draw();

	void Update();

};
