#pragma once

#include <vector>
#include <cstring>

#include<gl\glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../../../Shader/FontShader.h"
#include "Font.h"

class Text2D {
private:
	GLuint Text2DVertexBufferID;
	GLuint Text2DUVBufferID;
	GLuint VAO;
	//Texture* tex;
	FontShader* shader;
	//float charWidth;
	Font* font;
public:
	Text2D() {}
	Text2D(Font* font, FontShader* shader);
	void init();
	void printString(const char * text, glm::vec2 normalPos, glm::vec2 screenDim, int size);

};