#include "Button.h"

Button::Button(glm::vec2 cen, glm::vec2 dim, Texture* tex, GUIShader* guiShader){

	this->cen = cen;
	this->size = dim;
	this->button = new Mesh(cen, dim);
	this->texture = tex;

	this->isSelected = 0;
	this->shader = guiShader;

}

void Button::Draw(){
	this->shader->Bind();
	this->shader->Update(this->isSelected);
	this->texture->Bind(0);

	this->button->Draw();
}

void Button::Update(){



}
