#include "Mesh.h"

Mesh::Mesh(glm::vec2 cen, glm::vec2 size){

	glGenVertexArrays(1, &this->vao);
	glGenBuffers(2, this->vbo);

	points = new glm::vec2[6];
	texPoints = new glm::vec2[6];

	points[0] = glm::vec2(cen.x - (size.x / 2), cen.y - (size.y / 2));
	points[1] = glm::vec2(cen.x - (size.x / 2), cen.y + (size.y / 2));
	points[2] = glm::vec2(cen.x + (size.x / 2), cen.y + (size.y / 2));
	points[3] = glm::vec2(cen.x + (size.x / 2), cen.y + (size.y / 2));
	points[4] = glm::vec2(cen.x + (size.x / 2), cen.y - (size.y / 2));
	points[5] = glm::vec2(cen.x - (size.x / 2), cen.y - (size.y / 2));

	this->AABB = Box(cen.x - (size.x / 2), cen.y - (size.y / 2), 0, cen.x + (size.x / 2), cen.y + (size.y / 2), 0);

	texPoints[0] = glm::vec2(0, 1);
	texPoints[1] = glm::vec2(0, 0);
	texPoints[2] = glm::vec2(1, 0);
	texPoints[3] = glm::vec2(1, 0);
	texPoints[4] = glm::vec2(1, 1);
	texPoints[5] = glm::vec2(0, 1);

	glBindVertexArray(this->vao);

	glBindBuffer(GL_ARRAY_BUFFER, this->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 6, &*points, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);


	glBindBuffer(GL_ARRAY_BUFFER, this->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 6, &*texPoints, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);

	glBindVertexArray(0);
}

Mesh::Mesh(float minX, float minY, float maxX, float maxY){
	glGenVertexArrays(1, &this->vao);
	glGenBuffers(2, this->vbo);

	points = new glm::vec2[6];
	texPoints = new glm::vec2[6];

	points[0] = glm::vec2(minX, minY);
	points[1] = glm::vec2(minX, maxY);
	points[2] = glm::vec2(maxX, maxY);
	points[3] = glm::vec2(maxX, maxY);
	points[4] = glm::vec2(maxX, minY);
	points[5] = glm::vec2(minX, minY);

	this->AABB = Box(minX, minY, 0, maxX, maxY, 0);

	texPoints[0] = glm::vec2(0, 1);
	texPoints[1] = glm::vec2(0, 0);
	texPoints[2] = glm::vec2(1, 0);
	texPoints[3] = glm::vec2(1, 0);
	texPoints[4] = glm::vec2(1, 1);
	texPoints[5] = glm::vec2(0, 1);

	glBindVertexArray(this->vao);

	glBindBuffer(GL_ARRAY_BUFFER, this->vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 6, &*points, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);


	glBindBuffer(GL_ARRAY_BUFFER, this->vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 6, &*texPoints, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);

	glBindVertexArray(0);


}

void Mesh::Draw()
{
	glBindVertexArray(vao); // Bind our VAO at the start

	glDrawArrays(GL_TRIANGLES, 0, sizeof(glm::vec2) * 6);

	openGLErrorCheck(__FILE__, __LINE__);

	glBindVertexArray(0); // Unbind at the end
	
}





bool Mesh::AABBCollision(glm::vec3 point) // AABB on Point Collision	
{
	return (point.x >= this->AABB.minX && point.x <= this->AABB.maxX) &&
		(point.y >= this->AABB.minY && point.y <= this->AABB.maxY) &&
		(point.z >= this->AABB.minZ && point.z <= this->AABB.maxZ);
}
