#pragma once

#include "Texture.h"

class Font{

public:
	Font() {}
	Font(Texture* tex, float charWidth) {
		this->tex = tex; this->charWidth = charWidth;
	}
	Texture* tex;
	float charWidth;

};
