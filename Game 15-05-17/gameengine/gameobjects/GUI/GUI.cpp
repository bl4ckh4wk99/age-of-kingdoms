#include "GUI.h"

GUI::GUI(GUIShader* guiShader){ 
//Create basic background mesh and tex here
	this->shader = guiShader;

	selectionBackground = new Mesh(BACKGROUND_MIN_X, BACKGROUND_MIN_Y, BACKGROUND_MAX_X, BACKGROUND_MAX_Y);

	selectionBackgroundTex = new Texture("Assets/GUI/GUI_Background.png");

	resourceBackground = new Mesh(RESOURCES_MIN_X, RESOURCES_MIN_Y, RESOURCES_MAX_X, RESOURCES_MAX_Y);

	resourceBackgroundTex = new Texture("Assets/GUI/GUI_Resources.png");

	Texture* tex;
	Mesh* mesh;

	//Gold Tag
	goldMinX = RESOURCES_MIN_X + 0.05;
	goldMinY = RESOURCES_MAX_Y - 0.06 - 0.075;
	tex = new Texture("Assets/GUI/Gold.png");
	mesh = new Mesh(goldMinX, goldMinY, goldMinX + 0.075, goldMinY + 0.075);
	gold.push_back(pair<Mesh*, Texture*>(mesh, tex));

	//Population Tag
	popMinX = RESOURCES_MIN_X + 0.05;
	popMinY = goldMinY - 0.02 - 0.075;
	tex = new Texture("Assets/GUI/Population.png");
	mesh = new Mesh(popMinX, popMinY, popMinX + 0.075, popMinY + 0.075);
	pop.push_back(pair<Mesh*, Texture*>(mesh, tex));

	numStartX = popMinX + 0.075 + 0.05;
	for (int i = 0; i < 10; i++) {
		ostringstream os;
		os << "Assets/GUI/" << i << ".png";
		numTexs[i] = new Texture(os.str());
	}
	numTexs[10] = new Texture("Assets/GUI/Alpha.png");

	for (int i = 0; i < 4; i++) {
		mesh = new Mesh(numStartX, goldMinY, numStartX + 0.05, goldMinY + 0.075);
		gold.push_back(pair<Mesh*, Texture*>(mesh, numTexs[10]));
		mesh = new Mesh(numStartX, popMinY, numStartX + 0.05, popMinY + 0.075);
		pop.push_back(pair<Mesh*, Texture*>(mesh, numTexs[10]));
		numStartX += 0.035;
	}
}

GUI::~GUI()
{
}

void GUI::Init(){

	

}

void GUI::Draw(/*glm::vec4 normalisedCoords*/){

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Draw background gui here
	shader->Bind();
	shader->Update(0);

	//Draw GUI background
	selectionBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	selectionBackground->Draw();

	//Draw resources background
	resourceBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	resourceBackground->Draw();

	//draw resources values
	for (int i = 0; i < gold.size(); i++) {
		gold[i].second->Bind(0);
		glEnable(GL_TEXTURE_2D);
		gold[i].first->Draw();
	}

	for (int i = 0; i < pop.size(); i++) {
		pop[i].second->Bind(0);
		glEnable(GL_TEXTURE_2D);
		pop[i].first->Draw();
	}

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}



void GUI::Update(int gold, int pop){

	for (int i = 1; i <= 4; i ++) {
		this->gold[i].second = numTexs[10];
		this->pop[i].second = numTexs[10];
	}

	goldMinX = numStartX;
	vector<int> numbers;
	if (gold == 0) {
		numbers.push_back(0);
	}
	else {
		while (gold > 0) {
			int num = gold % 10;
			numbers.push_back(num);
			gold = gold / 10;
		}
	}
	int it = 1;
	for (int i = numbers.size() - 1; i >= 0; i--) {

		this->gold[it].second = numTexs[numbers[i]];
		it++;
	}

	//add pop numbers
	popMinX = numStartX;
	numbers.clear();
	while (pop > 0) {
		int num = pop % 10;
		numbers.push_back(num);
		pop = pop / 10;
	}
	it = 1;
	for (int i = numbers.size() - 1; i >= 0; i--) {
		this->pop[it].second = numTexs[numbers[i]];
		it++;
	}
	
	
}

//Method that checks the buttons for collision with the mouse
void GUI::checkButtonCollision(glm::vec4 normalisedCoords){

	
}

//Method that checks the GUI background for collision with the mouse
bool GUI::checkGUIcollision(glm::vec4 normalisedCoords){
	Box test = selectionBackground->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	return (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY);
}