#include "SelectedGUI.h"


SelectedGUI::SelectedGUI(AssetLibrary* assets):GameGUI(assets){

	lineShader = assets->getGUILineShader();

	this->icon = new Mesh(ICON_MIN_X, ICON_MIN_Y, ICON_MAX_X, ICON_MAX_Y);

	//Health Bar
	glGenVertexArrays(1, &this->greenVAO);
	glGenBuffers(1, &this->greenVBO);
	glGenVertexArrays(1, &this->redVAO);
	glGenBuffers(1, &this->redVBO);

	greenHealth[0] = glm::vec2(ICON_MAX_X + BORDER_X, ICON_MAX_Y - (ICON_SIZE / 2));
	greenHealth[1] = glm::vec2((ICON_MAX_X + BORDER_X) + HEALTH_SIZE , ICON_MAX_Y - (ICON_SIZE / 2));

	glBindVertexArray(this->greenVAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->greenVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 2, &*greenHealth, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);
	glBindVertexArray(0);

	redHealth[0] = glm::vec2(ICON_MAX_X + BORDER_X, ICON_MAX_Y - (ICON_SIZE / 2));
	redHealth[1] = glm::vec2((ICON_MAX_X + BORDER_X) + HEALTH_SIZE, ICON_MAX_Y - (ICON_SIZE / 2));

	glBindVertexArray(this->redVAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->redVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 2, &*redHealth, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (GLvoid*)0);
	glBindVertexArray(0);

	//unitIcons[ARCHER] = new Texture("Assets/GUI/ArcherIcon.png");
	//unitIcons[SWORDSMAN] = new Texture("Assets/GUI/SwordIcon.png");
	//unitIcons[BUILDER] = new Texture("Assets/GUI/BuildIcon.png");
	unitIcons = assets->getUnitIcons();

	//buildIcons[THRONE] = new Texture("Assets/GUI/CastleIcon.png");
	//buildIcons[MINE] = new Texture("Assets/GUI/MineIcon.png");
	//buildIcons[BARRACKS] = new Texture("Assets/GUI/BarracksIcon.png");
	buildIcons = assets->getBuildIcons();


	this->shader = assets->getGUIShader();
}

SelectedGUI::~SelectedGUI(){
	GameGUI::~GameGUI();
}

void SelectedGUI::Init(UnitType type){
	GameGUI::Init();

	//based on unit type load correct icon
	switch (type) {
	case ARCHER:
		iconTex = unitIcons->at(ARCHER);
		break;
	case SWORDSMAN:
		iconTex = unitIcons->at(SWORDSMAN);
		break;
	case BUILDER:
		iconTex = unitIcons->at(BUILDER);
		break;
	}

}

void SelectedGUI::Init(BuildingType type){
	GameGUI::Init();

	//based on building type load correct icon
	switch (type) {
	case THRONE:
		iconTex = buildIcons->at(THRONE);
		break;
	case MINE:
		iconTex = buildIcons->at(MINE);
		break;
	case BARRACKS:
		iconTex = buildIcons->at(BARRACKS);
		break;
	}
}

void SelectedGUI::Draw(int popCap){
	GameGUI::Draw(popCap);

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Draw background gui here
	this->shader->Bind();
	this->shader->Update(0);

	//Draw GUI background
	iconTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	this->icon->Draw();

	DrawHealthBar();

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}

void SelectedGUI::DrawHealthBar(){
	lineShader->Bind();

	glLineWidth(10);

	glm::vec3 colour = glm::vec3(1.0, 0.0, 0.0);
	//shader->Update(colour, &transform, cam);
	lineShader->UpdateHealthBar(colour);

	glBindVertexArray(this->redVAO);

	glDrawArrays(GL_LINES, 0, sizeof(glm::vec2) * 2);

	colour = glm::vec3(0.0, 1.0, 0.0);
	//healthShader->Update(colour, &transform, cam);
	lineShader->UpdateHealthBar(colour);

	glBindVertexArray(this->greenVAO);

	glDrawArrays(GL_LINES, 0, sizeof(glm::vec2) * 2);

	glBindVertexArray(0);
}

void SelectedGUI::Update(int gold, int pop){
	GameGUI::Update(gold, pop);


}

Button* SelectedGUI::checkButtonCollision(glm::vec4 normalisedCoords){
	GameGUI::checkButtonCollision(normalisedCoords);

	return nullptr;
}

bool SelectedGUI::checkGUIcollision(glm::vec4 normalisedCoords){
	return GameGUI::checkGUIcollision(normalisedCoords);


}
