#pragma once

#include "GameGUI.h"
#include "../Units/Unit.h"
#include "../Buildings/Building.h"

#define BORDER_X 0.05f
#define BORDER_Y 0.08f
#define ICON_SIZE 0.1f
#define HEALTH_SIZE 0.15f

#define ICON_MIN_X (BACKGROUND_MIN_X + BORDER_X)
#define ICON_MIN_Y ((BACKGROUND_MAX_Y - BORDER_Y) - ICON_SIZE)
#define ICON_MAX_X (ICON_MIN_X + ICON_SIZE)
#define ICON_MAX_Y (ICON_MIN_Y + ICON_SIZE)

class SelectedGUI : public GameGUI{
protected:
	//Icon
	Mesh* icon;
	Texture* iconTex;

	std::vector<Texture*>* unitIcons;
	std::vector<Texture*>* buildIcons;

	//Health Bar
	GUIShader* lineShader;

	glm::vec2 greenHealth[2];
	glm::vec2 redHealth[2];

	GLuint greenVAO, redVAO;
	GLuint greenVBO, redVBO;


public:
	SelectedGUI(AssetLibrary* assets);
	SelectedGUI() {}
	~SelectedGUI();

	void Init(UnitType type);
	void Init(BuildingType type);
	void Draw() {}
	void Draw(int popCap);
	void DrawHealthBar();
	void Update(int gold, int pop);

	Button* checkButtonCollision(glm::vec4 normalisedCoords);
	bool checkGUIcollision(glm::vec4 normalisedCoords);

};