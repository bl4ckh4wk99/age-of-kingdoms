#include "EndGUI.h"

EndGUI::EndGUI(AssetLibrary* assets) {
	//Create basic background mesh and tex here
	this->shader = assets->getGUIShader();

	endBackground = new Mesh(-1, -1, 1, 1);

	endBackgroundTex = assets->getBlankMenuBackgroundTex();

	Texture* menuTex = assets->getMainMenuTex();
	mainMenuButt = new Button(glm::vec2(0.0, -0.5), glm::vec2(0.5, 0.25), menuTex, shader);

}

EndGUI::~EndGUI()
{
}

void EndGUI::Init() {



}

void EndGUI::Draw(/*glm::vec4 normalisedCoords*/) {

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Draw background gui here
	shader->Bind();
	shader->Update(0);

	//Draw GUI background
	endBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	endBackground->Draw();

	//draw buttons
	mainMenuButt->Draw();


	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}



void EndGUI::Update() {

}


//Method that checks the buttons for collision with the mouse
Button* EndGUI::checkButtonCollision(glm::vec4 normalisedCoords) {

	//reset button selection
	mainMenuButt->isSelected = CLEAR;

	Box test = mainMenuButt->button->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
		return mainMenuButt;
	}
}

//Method that checks the GUI background for collision with the mouse
bool EndGUI::checkGUIcollision(glm::vec4 normalisedCoords) {
	Box test = endBackground->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	return (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY);
}