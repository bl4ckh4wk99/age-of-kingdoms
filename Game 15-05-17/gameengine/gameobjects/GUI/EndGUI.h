#pragma once
#include "GUI.h"

class EndGUI : public GUI
{
private:


public:
	EndGUI(AssetLibrary* assets);
	EndGUI() {}
	~EndGUI();

	void Init();
	void Draw();
	void Update();

	Button* checkButtonCollision(glm::vec4 normalisedCoords);
	bool checkGUIcollision(glm::vec4 normalisedCoords);

	GUIShader* shader;

	Mesh* endBackground;
	Texture* endBackgroundTex;

	Button* mainMenuButt;

};
