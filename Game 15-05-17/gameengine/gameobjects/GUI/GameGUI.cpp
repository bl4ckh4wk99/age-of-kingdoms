#include "GameGUI.h"

GameGUI::GameGUI(AssetLibrary* assets) {
	//Create basic background mesh and tex here
	this->shader = assets->getGUIShader();
	this->screenDim = assets->getScreenDim();

	selectionBackground = new Mesh(BACKGROUND_MIN_X, BACKGROUND_MIN_Y, BACKGROUND_MAX_X, BACKGROUND_MAX_Y);

	selectionBackgroundTex = assets->getSelectionBackgroundTex();

	resourceBackground = new Mesh(RESOURCES_MIN_X, RESOURCES_MIN_Y, RESOURCES_MAX_X, RESOURCES_MAX_Y);

	resourceBackgroundTex = assets->getResourceBackgroundTex();

	Texture* tex;
	Mesh* mesh;

	//Gold Tag
	goldMinX = RESOURCES_MIN_X + 0.05;
	goldMinY = RESOURCES_MAX_Y - 0.06 - 0.075;
	tex = assets->getGoldIconTex();
	mesh = new Mesh(goldMinX, goldMinY, goldMinX + 0.075, goldMinY + 0.075);
	gold = pair<Mesh*, Texture*>(mesh, tex);

	//Population Tag
	popMinX = RESOURCES_MIN_X + 0.05;
	popMinY = goldMinY - 0.02 - 0.075;
	tex = assets->getPopIconTex();
	mesh = new Mesh(popMinX, popMinY, popMinX + 0.075, popMinY + 0.075);
	pop = pair<Mesh*, Texture*>(mesh, tex);

	numStartX = popMinX + 0.075 + 0.05;

	textWriter = Text2D(assets->getFont(), assets->getFontShader());
	textWriter.init();
	//for (int i = 0; i < 10; i++) {
	//	ostringstream os;
	//	os << "Assets/GUI/" << i << ".png";
	//	numTexs[i] = new Texture(os.str());
	//}
	//numTexs[10] = new Texture("Assets/GUI/Alpha.png");
	//numTexs = assets->getNumTexs();

	//for (int i = 0; i < 4; i++) {
	//	mesh = new Mesh(numStartX, goldMinY, numStartX + 0.05, goldMinY + 0.075);
	//	gold.push_back(pair<Mesh*, Texture*>(mesh, numTexs->at(10)));
	//	mesh = new Mesh(numStartX, popMinY, numStartX + 0.05, popMinY + 0.075);
	//	pop.push_back(pair<Mesh*, Texture*>(mesh, numTexs->at(10)));
	//	numStartX += 0.035;
	//}
}

GameGUI::~GameGUI()
{
}

void GameGUI::Init() {



}

void GameGUI::Draw(int popCap) {

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Draw background gui here
	shader->Bind();
	shader->Update(0);

	//Draw GUI background
	selectionBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	selectionBackground->Draw();

	//Draw resources background
	resourceBackgroundTex->Bind(0);
	glEnable(GL_TEXTURE_2D);
	resourceBackground->Draw();

	//draw resources values
	gold.second->Bind(0);
	glEnable(GL_TEXTURE_2D);
	gold.first->Draw();

	pop.second->Bind(0);
	glEnable(GL_TEXTURE_2D);
	pop.first->Draw();

	stringstream goldPrint;
	goldPrint << goldNum;
	textWriter.printString(goldPrint.str().c_str(), glm::vec2(numStartX, -goldMinY), screenDim, 40);

	stringstream popPrint;
	popPrint << popNum << " / " << popCap;
	textWriter.printString(popPrint.str().c_str(), glm::vec2(numStartX, -popMinY), screenDim, 40);

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}



void GameGUI::Update(int gold, int pop) {

	goldNum = gold;
	popNum = pop;
}

//Method that checks the buttons for collision with the mouse
Button* GameGUI::checkButtonCollision(glm::vec4 normalisedCoords){

	return nullptr;
}

//Method that checks the GUI background for collision with the mouse
bool GameGUI::checkGUIcollision(glm::vec4 normalisedCoords) {
	Box test = selectionBackground->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	return (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY);
}