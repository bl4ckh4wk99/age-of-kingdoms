#include "BuildGUI.h"

BuildGUI::BuildGUI(AssetLibrary* assets):SelectedGUI(assets){
	this->shader = assets->getGUIShader();

	textWriter = Text2D(assets->getFont(), assets->getFontShader());
	textWriter.init();
}

BuildGUI::~BuildGUI(){
	SelectedGUI::~SelectedGUI();
}

void BuildGUI::Init(UnitType type){
	buttons.clear();
	producing = nullptr;
	SelectedGUI::Init(type);

	buildingType = UNKNOWN_BUILDING;

	switch (type)
	{
	case BUILDER: {
		float cenX = START_X;
		Texture* throne = buildIcons->at(THRONE);
		Button* throneButt = new Button(glm::vec2(cenX, START_Y) , glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), throne, shader);
		buttons.push_back(throneButt);
		cenX += BUILD_BORDER + BUILD_ICON_SIZE;

		Texture* mine = buildIcons->at(MINE);
		Button* mineButt = new Button(glm::vec2(cenX, START_Y), glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), mine, shader);
		buttons.push_back(mineButt);
		cenX += BUILD_BORDER + BUILD_ICON_SIZE;

		Texture* barracks = buildIcons->at(BARRACKS);
		Button* barracksButt = new Button(glm::vec2(cenX, START_Y), glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), barracks, shader);
		buttons.push_back(barracksButt);

		break;
	}
	case ARCHER: case SWORDSMAN:
		//Do Nothing as these units do not build anything
		break;
	}
}

void BuildGUI::Init(BuildingType type, vector<UnitType>* producing){
	buttons.clear();
	this->producing = producing;
	SelectedGUI::Init(type);
	float cenX = START_X;

	buildingType = type;

	switch (type)
	{
	case THRONE: {
		Texture* builder = unitIcons->at(BUILDER);
		Button* builderButt = new Button(glm::vec2(cenX, START_Y), glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), builder, shader);
		buttons.push_back(builderButt);

		break;
	}
	case MINE:
		//Do Nothing as this building does not build anything
		break;
	case BARRACKS: {
		Texture* archer = unitIcons->at(ARCHER);
		Button* archerButt = new Button(glm::vec2(cenX, START_Y), glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), archer, shader);
		buttons.push_back(archerButt);
		cenX += BUILD_BORDER + BUILD_ICON_SIZE;

		Texture* sword = unitIcons->at(SWORDSMAN);
		Button* swordButt = new Button(glm::vec2(cenX, START_Y), glm::vec2(BUILD_ICON_SIZE, BUILD_ICON_SIZE), sword, shader);
		buttons.push_back(swordButt);

		break;
	}
	}
}

void BuildGUI::Draw(int popCap){
	SelectedGUI::Draw(popCap);

	for (Button* but : buttons) {
		but->Draw();
	}

	if (producing != nullptr) {
		if (!producing->empty()) {
			//do some number prints
			if (buttons.size() > 1) {
				int archerCounter = 0, swordCounter = 0;

				for (UnitType type : *producing) {
					if (type == ARCHER)
						archerCounter++;
					if (type == SWORDSMAN)
						swordCounter++;
				}

				if (archerCounter > 0) {
					stringstream archerPrint;
					archerPrint << archerCounter;
					textWriter.printString(archerPrint.str().c_str(), glm::vec2(TEXT_X, TEXT_Y), screenDim, 40);
				}
				if (swordCounter > 0) {
					stringstream swordPrint;
					swordPrint << swordCounter;
					textWriter.printString(swordPrint.str().c_str(), glm::vec2(TEXT_X + BUILD_BORDER + BUILD_ICON_SIZE, TEXT_Y), screenDim, 40);
				}

			}
			else {
				int builderCounter = 0;

				for (UnitType type : *producing) {
					if (type == BUILDER)
						builderCounter++;
				}

				if (builderCounter > 0) {
					stringstream buildPrint;
					buildPrint << builderCounter;
					textWriter.printString(buildPrint.str().c_str(), glm::vec2(TEXT_X, TEXT_Y), screenDim, 40);
				}
			}
		}
	}
}

void BuildGUI::Update(int gold, int pop){
	SelectedGUI::Update(gold, pop);

}

void BuildGUI::deSelectButtons(){
	for (Button* butt : buttons) {
		butt->isSelected = CLEAR;
	}
}

void BuildGUI::checkButtonHighlights(glm::vec4 normalisedCoords){
	//clear previously selected
	for (Button* butts : buttons) {
		butts->isSelected = CLEAR;
	}
	for (int i = 0; i < buttons.size(); i++) {
		Button* butt = buttons[i];
		Box test = butt->button->getAABB();
		// Check for the mouse intersection with the buttons of the GUI
		if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
			butt->isSelected = HIGHLIGHTED;
		}
	}
}

BuildingType BuildGUI::checkBuildersButtons(glm::vec4 normalisedCoords, int curResources){
	SelectedGUI::checkButtonCollision(normalisedCoords);

	//clear previously selected
	for (Button* butts : buttons) {
		butts->isSelected = CLEAR;
	}

	for(int i = 0; i < buttons.size(); i++){
		Button* butt = buttons[i];
		Box test = butt->button->getAABB();
		// Check for the mouse intersection with the buttons of the GUI
		if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
			BuildingType type = static_cast<BuildingType>(i);
			//check if we have enough money, if not then return unknown
			if (BUILDING_COSTS[type] > curResources) {
				butt->isSelected = FAULT;
				return UNKNOWN_BUILDING;
			}
			//else return building type
			else {
				butt->isSelected = CLICKED;
				return static_cast<BuildingType>(i);
			}
		}
	}
	return UNKNOWN_BUILDING;
}

UnitType BuildGUI::checkBuildingButtons(glm::vec4 normalisedCoords, int curResources, bool popSpace, bool leftClick) {
	SelectedGUI::checkButtonCollision(normalisedCoords);

	//clear previously selected
	for (Button* butts : buttons) {
		butts->isSelected = CLEAR;
	}

	if (buttons.size() == 1) {
		Button* butt = buttons[0];
		Box test = butt->button->getAABB();
		// Check for the mouse intersection with the buttons of the GUI
		if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
			if (leftClick) {
				if (UNIT_COSTS[BUILDER] > curResources || !popSpace) {
					butt->isSelected = FAULT;
					return UNKNOWN_UNIT;
				}
				else {
					butt->isSelected = CLICKED;
					return BUILDER;
				}
			}
			butt->isSelected = CLICKED;
			return BUILDER;
		}
	}
	else {
		for (int i = 0; i < buttons.size(); i++) {
			Button* butt = buttons[i];
			Box test = butt->button->getAABB();
			// Check for the mouse intersection with the buttons of the GUI
			if (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY) {
				if (i == 0) {
					if (leftClick) {
						if (UNIT_COSTS[ARCHER] > curResources || !popSpace) {
							butt->isSelected = FAULT;
							return UNKNOWN_UNIT;
						}
						else {
							butt->isSelected = CLICKED;
							return ARCHER;
						}
					}
					butt->isSelected = CLICKED;
					return ARCHER;
				}
				else {
					if (leftClick) {
						if (UNIT_COSTS[SWORDSMAN] > curResources || !popSpace) {
							butt->isSelected = FAULT;
							return UNKNOWN_UNIT;
						}
						else {
							butt->isSelected = CLICKED;
							return SWORDSMAN;
						}
					}
					butt->isSelected = CLICKED;
					return SWORDSMAN;
				}
			}
		}
	}

	return UNKNOWN_UNIT;
}

bool BuildGUI::checkGUIcollision(glm::vec4 normalisedCoords){
	SelectedGUI::checkGUIcollision(normalisedCoords);

	return false;
}
