#pragma once

#include <vector>
#include "../Primitives/Ray.h"
#include "GUI Primitives/Button.h"
#include "../AssetLibrary.h"

class GUI
{
public:
	virtual void Init() = 0;
	virtual void Draw() = 0;
	virtual void Update() = 0;

	void checkHighlight(glm::vec4 normalisedCoords) {
		Button* butt = checkButtonCollision(normalisedCoords);
		if(butt != nullptr)
			butt->isSelected = HIGHLIGHTED;
	}
	void checkClicked(glm::vec4 normalisedCoords) {
		Button* butt = checkButtonCollision(normalisedCoords);
		if (butt != nullptr)
			butt->isSelected = CLICKED;
	}

	virtual Button* checkButtonCollision(glm::vec4 normalisedCoords) = 0;
	virtual bool checkGUIcollision(glm::vec4 normalisedCoords) = 0;

	GUIShader* shader;

};