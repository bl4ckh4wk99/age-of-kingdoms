#pragma once
#include "GUI.h"

class MenuGUI : public GUI
{
private:


public:
	MenuGUI(AssetLibrary* assets);
	MenuGUI() {}
	~MenuGUI();

	void Init();
	void Draw();
	void Update();

	Button* checkButtonCollision(glm::vec4 normalisedCoords);
	bool checkGUIcollision(glm::vec4 normalisedCoords);

	GUIShader* shader;

	Mesh* menuBackground;
	Texture* menuBackgroundTex;

	Button* PvAIButt;
	Button* AIvAIButt;

};
