#pragma once
#include "GUI.h"

#include <vector>
#include "../Primitives/Ray.h"
#include "GUI Primitives/Button.h"
#include "GUI Primitives\Text2D.h"

#define BACKGROUND_MIN_X -1.0f
#define BACKGROUND_MIN_Y -1.0f
#define BACKGROUND_MAX_X -0.6f
#define BACKGROUND_MAX_Y -0.4f

#define RESOURCES_MIN_X  0.6f
#define RESOURCES_MIN_Y  0.7f
#define RESOURCES_MAX_X  1.0f
#define RESOURCES_MAX_Y  1.0f

class GameGUI : public GUI
{
protected:
	glm::vec2 screenDim;

	float goldMinX, goldMinY;
	float popMinX, popMinY;
	float numStartX;

	pair<Mesh*, Texture*> gold;
	int goldNum;
	pair<Mesh*, Texture*> pop;
	int popNum;
	Text2D textWriter;

public:
	GameGUI(AssetLibrary* assets);
	GameGUI() {}
	~GameGUI();

	void Init();
	void Draw() {}
	void Draw(int popCap);
	void Update() {}
	void Update(int gold, int pop);

	Button* checkButtonCollision(glm::vec4 normalisedCoords);
	bool checkGUIcollision(glm::vec4 normalisedCoords);

	GUIShader* shader;

	Mesh* selectionBackground;
	Texture* selectionBackgroundTex;

	Mesh* resourceBackground;
	Texture* resourceBackgroundTex;

};