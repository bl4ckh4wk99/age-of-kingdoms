#include "Grid.h"

void Grid::Init(int cellSize, int gridSize){

	this->cellSize = cellSize;
	this->gridSize = gridSize;

	numCells = gridSize * gridSize;

	worldMap = new int[numCells];
	string line;
	ifstream myfile("Map.txt");
	int iterator = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			for (char& c : line) {
				worldMap[iterator] = (int)c - '0';
				iterator++;
			}
		}
		myfile.close();
	}

	unitMap = new int[numCells];
	buildingMap = new int[numCells];
	for (int i = 0; i < numCells; i++) {
		unitMap[i] = 0;
		buildingMap[i] = 0;
	}
	
	highlighting = false;
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(3, this->VBO);
}

void Grid::Draw(Camera* cam){

	if (highlighting) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		gridShader->Bind();

		gridShader->Update(cam);

		glBindVertexArray(this->VAO);

		glDrawArrays(GL_QUADS, 0, sizeof(glm::vec3) * 4);

		glBindVertexArray(0);
		glDisable(GL_BLEND);
	}

}

void Grid::Update(double deltaT){

	if (highlighting) {
		highlightTime += deltaT;
		if (highlightTime >= HIGHLIGHT_DUR) {
			highlighting = false;
		}
		else {
			double fractpart, intpart;
			fractpart = modf(highlightTime, &intpart);
			float newMix = 1 - fractpart;
			mixValue = new float[4];
			mixValue[0] = newMix; mixValue[1] = newMix; mixValue[2] = newMix; mixValue[3] = newMix;

			glBindVertexArray(this->VAO);

			glBindBuffer(GL_ARRAY_BUFFER, this->VBO[2]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4, &*mixValue, GL_DYNAMIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(float), (GLvoid*)0);

			glBindVertexArray(0);
		}
	}

}

void Grid::highLightSelect(glm::vec2 cell) {
	highlighting = true;
	highlightTime = 0.0;

	highLightPoints = new glm::vec3[4];
	glm::vec3 cellCen = getCellCen(cell);
	highLightPoints[0] = glm::vec3(cellCen.x - (cellSize / 2), 2.0, cellCen.z + (cellSize / 2));
	highLightPoints[1] = glm::vec3(cellCen.x + (cellSize / 2), 2.0, cellCen.z + (cellSize / 2));
	highLightPoints[2] = glm::vec3(cellCen.x + (cellSize / 2), 2.0, cellCen.z - (cellSize / 2));
	highLightPoints[3] = glm::vec3(cellCen.x - (cellSize / 2), 2.0, cellCen.z - (cellSize / 2));

	glm::vec3 select = glm::vec3(0.05, 0.85, 0.99);
	highLightColour = new glm::vec3[4];
	highLightColour[0] = select; highLightColour[1] = select; highLightColour[2] = select; highLightColour[3] = select;

	mixValue = new float[4];
	mixValue[0] = 1.0; mixValue[1] = 1.0; mixValue[2] = 1.0; mixValue[3] = 1.0;

	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*highLightPoints, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*highLightColour, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4, &*mixValue, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(float), (GLvoid*)0);

	glBindVertexArray(0);
}

void Grid::highLightAction(glm::vec2 cell){
	highlighting = true;
	highlightTime = 0.0;

	highLightPoints = new glm::vec3[4];
	glm::vec3 cellCen = getCellCen(cell);
	highLightPoints[0] = glm::vec3(cellCen.x - (cellSize / 2), 2.0, cellCen.z + (cellSize / 2));
	highLightPoints[1] = glm::vec3(cellCen.x + (cellSize / 2), 2.0, cellCen.z + (cellSize / 2));
	highLightPoints[2] = glm::vec3(cellCen.x + (cellSize / 2), 2.0, cellCen.z - (cellSize / 2));
	highLightPoints[3] = glm::vec3(cellCen.x - (cellSize / 2), 2.0, cellCen.z - (cellSize / 2));

	glm::vec3 action = glm::vec3(1.0, 0.44, 0.20);
	highLightColour = new glm::vec3[4];
	highLightColour[0] = action; highLightColour[1] = action; highLightColour[2] = action; highLightColour[3] = action;

	mixValue = new float[4];
	mixValue[0] = 1.0; mixValue[1] = 1.0; mixValue[2] = 1.0; mixValue[3] = 1.0;

	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*highLightPoints, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &*highLightColour, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4, &*mixValue, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(float), (GLvoid*)0);

	glBindVertexArray(0);
}

bool Grid::isInGrid(glm::vec2 cell){

	if (cell.x >= gridSize || cell.x < 0) {
		return false;
	}
	if (cell.y >= gridSize || cell.y < 0) {
		return false;
	}
	return true;
}

bool Grid::isValid(glm::vec2 cellPos){
	if (isInGrid(cellPos)) {
		return worldMap[(int)cellPos.x + (int)(cellPos.y * gridSize)] != 0;
	}
	else {
		return false;
	}
}

int Grid::isOccupiedByUnit(glm::vec2 cellPos){
	return unitMap[(int)cellPos.x + (int)(cellPos.y * gridSize)];
}

int Grid::isOccupiedByBuilding(glm::vec2 cellPos) {
	return buildingMap[(int)cellPos.x + (int)(cellPos.y * gridSize)];
}

bool Grid::isOccupiedByEnemyUnit(glm::vec2 cellPos, int team){
	int temp =  unitMap[(int)cellPos.x + (int)(cellPos.y * gridSize)];
	if (temp != team && temp != 0)
		return true;
	return false;
}

bool Grid::isOccupiedByEnemyBuilding(glm::vec2 cellPos, int team){
	int temp = buildingMap[(int)cellPos.x + (int)(cellPos.y * gridSize)];
	if (temp != team && temp != 0)
		return true;
	return false;
}

bool Grid::availableToBuild(glm::vec2 cellPos, int buildingDim){
	int pad = 1;
	for (int y = -(buildingDim / 2 + pad); y < (buildingDim / 2 + pad); y++) {
		for (int x = -(buildingDim / 2 + pad); x < (buildingDim / 2 + pad); x++) {
			if (!isValid(glm::vec2(cellPos.x + x, cellPos.y + y)))
				return false;
		}
	}
	return true;
}

//called when a unit enters a cell to update the maps with its pos
void Grid::occupyNewCell(glm::vec2 oldCell, glm::vec2 newCell, int team){
	//First cell (just spawned)
	if (oldCell.x == -1 && oldCell.y == -1) {
		unitMap[(int)newCell.x + (int)(newCell.y * gridSize)] = team;	//team occupies
		worldMap[(int)newCell.x + (int)(newCell.y * gridSize)] = 0;		//cell invalid
	}
	else {
		unitMap[(int)oldCell.x + (int)(oldCell.y * gridSize)] = 0;		//no team occupy
		worldMap[(int)oldCell.x + (int)(oldCell.y * gridSize)] = 1;		//cell is valid

		unitMap[(int)newCell.x + (int)(newCell.y * gridSize)] = team;	//team occupies
		worldMap[(int)newCell.x + (int)(newCell.y * gridSize)] = 0;		//cell invalid
	}


}

//Called when a building is created to fill the maps with its pos
void Grid::occupyNewCells(glm::vec2 cenCell, int sizeInCells, int team) {
	for (int y = -(sizeInCells / 2); y <= (sizeInCells / 2); y++) {
		for (int x = -(sizeInCells / 2); x <= (sizeInCells / 2); x++) {
			worldMap[(int)(cenCell.x + x) + (int)((cenCell.y + y) * gridSize)] = 0;
			buildingMap[(int)(cenCell.x + x) + (int)((cenCell.y + y) * gridSize)] = team;
		}
	}
}

void Grid::unOccupyCell(glm::vec2 Cell){
	unitMap[(int)Cell.x + (int)(Cell.y * gridSize)] = 0;		//no team occupy
	worldMap[(int)Cell.x + (int)(Cell.y * gridSize)] = 1;		//cell is valid
}

void Grid::unOccupyCells(glm::vec2 cenCell, int sizeInCells){
	for (int y = -(sizeInCells / 2); y <= (sizeInCells / 2); y++) {
		for (int x = -(sizeInCells / 2); x <= (sizeInCells / 2); x++) {
			worldMap[(int)(cenCell.x + x) + (int)((cenCell.y + y) * gridSize)] = 1; //cell is valid
			buildingMap[(int)(cenCell.x + x) + (int)((cenCell.y + y) * gridSize)] = 0; //no team occupy
		}
	}
}

void Grid::printGrid(){

	ofstream myfile;
	myfile.open("GRID_DEBUG.txt");
	if (myfile.is_open()) {
		for (int z = 0; z < gridSize; z++) {
			for (int x = 0; x < gridSize; x++) {
				myfile << worldMap[x + (z * gridSize)];
			}
			myfile << "\n";
		}
	}

	myfile.close();

}

void Grid::getCellBB(glm::vec3* &cellBBArray, glm::vec2 cellPos){
	cellBBArray = new glm::vec3[4];
	glm::vec3 cellCen = getCellCen(cellPos);
	
	cellBBArray[0] = glm::vec3(cellCen.x - (cellSize / 2), 0, cellCen.z + (cellSize / 2));
	cellBBArray[1] = glm::vec3(cellCen.x + (cellSize / 2), 0, cellCen.z + (cellSize / 2));
	cellBBArray[2] = glm::vec3(cellCen.x + (cellSize / 2), 0, cellCen.z - (cellSize / 2));
	cellBBArray[3] = glm::vec3(cellCen.x - (cellSize / 2), 0, cellCen.z - (cellSize / 2));
}

bool Grid::pointInCell(glm::vec3 point, glm::vec2 cell){
	glm::vec3* cellBB = new glm::vec3[4];
	getCellBB(cellBB, cell);

	float minX = gridSize * cellSize; float minZ = gridSize * cellSize;
	float maxX = 0.0f; float maxZ = 0.0f;
	for (int i = 0; i < 4; i++) {
		if (cellBB[i].x < minX)
			minX = cellBB[i].x;
		if (cellBB[i].x > maxX)
			maxX = cellBB[i].x;
		if (cellBB[i].z < minZ)
			minZ = cellBB[i].z;
		if (cellBB[i].z > maxZ)
			maxZ = cellBB[i].z;
	}

	return(minX < point.x && point.x < maxX && minZ < point.z && point.z < maxZ);
}

glm::vec3 Grid::getCellCen(glm::vec2 cellPos){

	float x = (cellPos.x * cellSize) + (cellSize / 2);
	float z = (cellPos.y * cellSize) + (cellSize / 2);

	return glm::vec3(x, 0, z);
}

glm::vec2 Grid::getCellFromPoint(glm::vec3 point){

	float x = (int)(point.x / cellSize);
	float z = (int)(point.z / cellSize);

	return glm::vec2(x,z);
}

void Grid::restrictPoint(glm::vec2 & point){
	
	if (point.x > BASIC_GRIDSIZE) {
		point.x = BASIC_GRIDSIZE - 5;
	}
	if (point.x < 0) {
		point.x = 5;
	}
	if (point.y > BASIC_GRIDSIZE) {
		point.y = BASIC_GRIDSIZE - 5;
	}
	if (point.y < 0) {
		point.y = 5;
	}


}
