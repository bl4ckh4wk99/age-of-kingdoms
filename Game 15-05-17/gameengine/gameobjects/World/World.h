#pragma once

#include "../../Shader/ModelShader.h"
#include "../../ObjectLoading/Model.h"

#include "Grid.h"


class World {
private:

	ModelShader* modelShader;

	Model* world;

	Grid worldGrid;

public:

	World() {}

	World(AssetLibrary* assets);

	void Init();

	void Draw(Camera* cam);

	void Update(double deltaT);

#ifdef GRID_DEBUG
	void selectCell(glm::vec3 point);
	void selectCell(glm::vec2 cellPos);
#endif // GRID_DEBUG

	void highLightSelect(glm::vec2 cell);
	void highLightAction(glm::vec2 cell);

	void printGrid();

	Grid* getGrid() { return &this->worldGrid; }


};