#pragma once

#include <glm\glm.hpp>

#include <gl\glew.h>

#include <iostream>
#include <fstream>

//#include "../../Shader/GridShader.h"
#include "../AssetLibrary.h"
//#include "../Users/Player.h"

#define BASIC_CELLSIZE 10
#define BASIC_GRIDSIZE 40

#define HIGHLIGHT_DUR 2

class Grid {

public:
	int cellSize;
	int gridSize;
	int numCells;

	int* worldMap;		//used to check whether cell is occupied, 0 = occupied, 1 = unoccupied
	int* unitMap;		//used to check whether a unit occupies a cell, 0 = no unit, 1 = player 1 unit, 2 = player 2 unit
	int* buildingMap;	//used to check whether a building occupies a cell, 0 = no building, 1 = player 1 building, 2 = player 2 building

	//Player* playerOne;
	//Player* playerTwo;

private:

	bool highlighting;
	double highlightTime;

	glm::vec3* highLightPoints;
	glm::vec3* highLightColour;
	float* mixValue;
	GLuint VAO;
	GLuint VBO[3];

	GridShader* gridShader;

public:

	Grid() {}
	Grid(AssetLibrary* assets) {
		this->gridShader = assets->getGridShader();
	}

	void Init(int cellSize, int gridSize);

	void Draw(Camera* cam);

	void Update(double deltaT);

	//void setPlayers(Player* p1, Player* p2) { this->playerOne = p1; this->playerTwo = p2; }

#ifdef GRID_DEBUG
	void selectCell(glm::vec3 point);
	void selectCell(glm::vec2 cellPos);
#endif // GRID_DEBUG

	void highLightSelect(glm::vec2 cell);
	void highLightAction(glm::vec2 cell);

	bool isInGrid(glm::vec2 cell);

	bool isValid(glm::vec2 cellPos);							//is the worldMap position a 1
	int isOccupiedByUnit(glm::vec2 cellPos);					//is the unitMap a 0
	int isOccupiedByBuilding(glm::vec2 cellPos);				//is the buildingMap a 0
	bool isOccupiedByEnemyUnit(glm::vec2 cellPos, int team);	//is the unitMap a 0
	bool isOccupiedByEnemyBuilding(glm::vec2 cellPos, int team);//is the buildingMap a 0

	//Unit* getUnitFromOccupiedCell(glm::vec2 cell);

	bool availableToBuild(glm::vec2 cellPos, int buildingDim);


	void occupyNewCell(glm::vec2 oldCell, glm::vec2 newCell, int team);
	void occupyNewCells(glm::vec2 cenCell, int sizeInCells, int team);

	void unOccupyCell(glm::vec2 Cell);
	void unOccupyCells(glm::vec2 cenCell, int sizeInCells);

	void printGrid();

	void getCellBB(glm::vec3* &cellBBArray, glm::vec2 cellPos);
	bool pointInCell(glm::vec3 point, glm::vec2 cell);
	glm::vec3 getCellCen(glm::vec2 cellPos);
	glm::vec2 getCellFromPoint(glm::vec3 point);

	static void restrictPoint(glm::vec2 &point);

};
