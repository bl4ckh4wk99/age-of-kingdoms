#include "World.h"

World::World(AssetLibrary* assets){

	this->modelShader = assets->getModelShader();

	this->world = assets->getGameMap();

	worldGrid = Grid(assets);
}

void World::Init(){
	
	worldGrid.Init(BASIC_CELLSIZE, BASIC_GRIDSIZE);

}

void World::Draw(Camera* cam){

	modelShader->Bind();

	modelShader->UpdateStatic(cam);

	world->BindModelsVAO();
	world->RenderModel();

	worldGrid.Draw(cam);
}

void World::Update(double deltaT){

	worldGrid.Update(deltaT);
}

void World::highLightSelect(glm::vec2 cell){
	worldGrid.highLightSelect(cell);
}

void World::highLightAction(glm::vec2 cell){
	worldGrid.highLightAction(cell);
}

void World::printGrid(){
	worldGrid.printGrid();
}
