#include "Camera.h"

Camera::Camera(glm::vec3 pos, glm::mat4* projectionMatrix)
{
	this->projectionMatrix = projectionMatrix;
	this->pos = pos;							// Position of camera
	this->lookat = glm::vec3(0.0f, 0.0f, -1.0f);	// Lookat vector of camera
	this->up = glm::vec3(0.0f, 1.0f, 0.0f);		// Upwards vector of camera

												//Movement vectors
	this->forward = glm::vec3(0, 0, -1);
	this->right = glm::vec3(1, 0, 0);
	//Movement values
	this->rotation = 0;
	this->zoomVal = 0;

	moveDir = glm::vec2(0, 0);
	rotDir = 0;
	zoomDir = 0;

}

void Camera::lookAtPlayerOne(){

	this->pos = this->pos + (this->right * 200.0f);
	this->lookat = this->lookat + (this->right * 200.0f);
	this->pos = this->pos + (this->forward * -350.0f);
	this->lookat = this->lookat + (this->forward * -350.0f);

}

void Camera::lookAtCenter(){
	//move to center
	this->pos = this->pos + (this->right * 200.0f);
	this->lookat = this->lookat + (this->right * 200.0f);
	this->pos = this->pos + (this->forward * -200.0f);
	this->lookat = this->lookat + (this->forward * -200.0f);
}

void Camera::move(double deltaT) {
	switch ((int)moveDir.x) {
	case -1:
		this->pos = this->pos + (this->right * -(float)(moveValue * deltaT));
		this->lookat = this->lookat + (this->right * -(float)(moveValue * deltaT));
		break;
	case 1:
		this->pos = this->pos + (this->right * (float)(moveValue * deltaT));
		this->lookat = this->lookat + (this->right * (float)(moveValue * deltaT));
	}

	switch ((int)moveDir.y) {
	case -1:
		this->pos = this->pos + (this->forward * -(float)(moveValue * deltaT));
		this->lookat = this->lookat + (this->forward * -(float)(moveValue * deltaT));
		break;
	case 1:
		this->pos = this->pos + (this->forward * (float)(moveValue * deltaT));
		this->lookat = this->lookat + (this->forward * (float)(moveValue * deltaT));
	}
}

void Camera::rotate(double deltaT) {

	glm::mat4 rotMat = glm::mat4(1.0);
	switch (rotDir) {
	case -1:
		rotMat = glm::rotate(glm::mat4(1.0), (float)(this->rotation - (rotValue * deltaT)), glm::vec3(0, 1, 0));
		this->lookat = glm::vec3(glm::vec4(this->lookat, 1.0) * rotMat);
		this->up = glm::vec3(glm::vec4(this->up, 1.0) * rotMat);
		this->forward = glm::vec3(glm::vec4(this->forward, 1.0) * rotMat);
		this->right = glm::vec3(glm::vec4(this->right, 1.0) * rotMat);
		break;
	case 1:
		rotMat = glm::rotate(glm::mat4(1.0), (float)(this->rotation + (rotValue * deltaT)), glm::vec3(0, 1, 0));
		this->lookat = glm::vec3(glm::vec4(this->lookat, 1.0) * rotMat);
		this->up = glm::vec3(glm::vec4(this->up, 1.0) * rotMat);
		this->forward = glm::vec3(glm::vec4(this->forward, 1.0) * rotMat);
		this->right = glm::vec3(glm::vec4(this->right, 1.0) * rotMat);
		break;
	}
}

void Camera::zoom(double deltaT) {

	switch (zoomDir) {
	case -1:
		this->pos = this->pos + (glm::vec3(0,1,0) * (float)(zoomValue * deltaT));
		break;
	case 1:
		this->pos = this->pos + (glm::vec3(0, 1, 0) * -(float)(zoomValue * deltaT));
		break;
	}

}

glm::mat4 Camera::getView() {
	return glm::lookAt(this->pos, this->lookat, this->up);
}

glm::mat4 Camera::getProjectionMatrix(){
	return *this->projectionMatrix;
}
