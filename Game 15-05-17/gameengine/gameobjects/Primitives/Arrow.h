#pragma once

#include "../AssetLibrary.h"
#include "CatmullRom.h"

#define PEAK_HEIGHT 5.0f

class Arrow {
private:

	Model* arrow;
	ModelShader* shader;

	std::vector <glm::vec3> flightPath;
	Transform transform;
public:

	Arrow() {}

	Arrow(AssetLibrary* assets);

	void Init(glm::vec3 from, glm::vec3 to);

	void Draw(Camera* cam);

	void Update(double deltaT);

	bool endOfPath();
};
