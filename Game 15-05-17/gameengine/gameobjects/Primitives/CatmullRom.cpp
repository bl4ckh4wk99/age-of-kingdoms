#include "CatmullRom.h"

std::vector<glm::vec3> CatmullRom::CatmullRomSpline(glm::vec3 point0, glm::vec3 point1, glm::vec3 point2, glm::vec3 point3, int steps){

	std::vector<glm::vec3> splinePoints;

	float interval = 1.0 / steps;
	float t = 0.0f;

	for (int i = 0; i < steps; i++) {
		t = 0.0 + (i * interval);
		float x = q(point0.x, point1.x, point2.x, point3.x, t);
		float y = q(point0.y, point1.y, point2.y, point3.y, t);
		float z = q(point0.z, point1.z, point2.z, point3.z, t);

		splinePoints.push_back(glm::vec3(x, y, z));
	}

	return splinePoints;
}

float CatmullRom::q(float point0, float point1, float point2, float point3, float t){

	return 0.5f *	((2 * point1) +
					(point2 - point0) * t +
					(2 * point0 - 5 * point1 + 4 * point2 - point3) * t * t +
					(3 * point1 - point0 - 3 * point2 + point3) * t * t * t);
}
