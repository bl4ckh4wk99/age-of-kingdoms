#include "AStar.h"

bool compareNodes(Node* a, Node* b) { return a->totalCost > b->totalCost; }

vector<glm::vec2> AStar::generatePath(glm::vec2 from, glm::vec2 to){

	openList.clear();
	closedList.clear();
	goalList.clear();

	nodeMap = new Node*[gridMap->numCells];

	for (int z = 0; z < gridMap->gridSize; z++) { //Up & down
		for (int x = 0; x < gridMap->gridSize; x++) { //Across
			Node* temp = new Node(glm::vec2(x, z));
			temp->status = NOT_PROCESSED;
			nodeMap[x + (z * gridMap->gridSize)] = temp;
		}
	}

	this->start = nodeMap[getMapPos(from)];

	//check target is valid cell
	this->goal = nodeMap[getMapPos(to)];

	//Add start node to openlist
	openList.push_back(start);
	start->status = OPEN_LIST;

	//Find all children
	findChildren(start);

	//Remove start from openlist and add to closedList
	closedList.push_back(openList.front());
	openList.erase(openList.begin());
	start->status = CLOSED_LIST;


	//While not at goal node	- choose node from openList with lowest total cost
	//							- find children for said node adding them to openList & calcing their costs
	//							- remove this node from open and put in closed
	//							- repeat
	while (closedList.back() != goal) {
		search();
	}
	return getPath();
}

vector<glm::vec2> AStar::generatePath(glm::vec2 from, glm::vec2 targetCen, int targetDim){

	openList.clear();
	closedList.clear();
	goalList.clear();

	nodeMap = new Node*[gridMap->numCells];

	for (int z = 0; z < gridMap->gridSize; z++) { //Up & down
		for (int x = 0; x < gridMap->gridSize; x++) { //Across
			Node* temp = new Node(glm::vec2(x, z));
			temp->status = NOT_PROCESSED;
			nodeMap[x + (z * gridMap->gridSize)] = temp;
		}
	}

	this->start = nodeMap[getMapPos(from)];

	//centre of target
	goalList.push_back(nodeMap[getMapPos(targetCen)]);

	int pad = 1;
	//bottom left
	glm::vec2 exit = glm::vec2(targetCen.x - (int)((targetDim / 2) + pad), targetCen.y + (int)((targetDim / 2) + pad));
	//go along bottom row
	for (int x = -((targetDim / 2) + pad); x < ((targetDim / 2) + pad); x++) {
		exit = glm::vec2(exit.x + 1, exit.y);
		if (gridMap->isValid(exit)) {
			goalList.push_back(nodeMap[getMapPos(exit)]);
		}
	}
	//go up right side
	for (int y = -((targetDim / 2) + pad); y < ((targetDim / 2) + pad); y++) {
		exit = glm::vec2(exit.x, exit.y - 1);
		if (gridMap->isValid(exit)) {
			goalList.push_back(nodeMap[getMapPos(exit)]);
		}
	}
	//go along top row
	for (int x = -((targetDim / 2) + pad); x < ((targetDim / 2) + pad); x++) {
		exit = glm::vec2(exit.x - 1, exit.y);
		if (gridMap->isValid(exit)) {
			goalList.push_back(nodeMap[getMapPos(exit)]);
		}
	}
	//go down left side
	for (int y = -((targetDim / 2) + pad); y < ((targetDim / 2) + pad); y++) {
		exit = glm::vec2(exit.x, exit.y + 1);
		if (gridMap->isValid(exit)) {
			goalList.push_back(nodeMap[getMapPos(exit)]);
		}
	}

	//get node with smallest heuristic cost
	float curSmallest = 1000000.0f;
	int smallestIt = 0;
	for (int i = 0; i < goalList.size(); i++) {
		if (glm::length(from - goalList[i]->cellPos) < curSmallest) {
			curSmallest = glm::length(from - goalList[i]->cellPos);
			smallestIt = i;
		}
	}
		
	this->goal = goalList[smallestIt];

	//Add start node to openlist
	openList.push_back(start);
	start->status = OPEN_LIST;

	//Find all children
	findChildren(start);

	//Remove start from openlist and add to closedList
	closedList.push_back(openList.front());
	openList.erase(openList.begin());
	start->status = CLOSED_LIST;


	//While not at goal node	- choose node from openList with lowest total cost
	//							- find children for said node adding them to openList & calcing their costs
	//							- remove this node from open and put in closed
	//							- repeat
	while (closedList.back() != goal) {
		search();
	}

	return getPath();
}

void AStar::cleanup(){


	for (int i = 0; i < gridMap->gridSize * gridMap->gridSize; i++) {
		delete nodeMap[i];
	}
	delete[] nodeMap;

	goalList.clear();

	openList.clear();

	closedList.clear();
}

void AStar::findChildren(Node * parent) {

	//Check & add adjacent children
	glm::vec2 possChild = glm::vec2(parent->cellPos.x, parent->cellPos.y - 1);//Above
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);  
		checkChild(parent, mapLoc, STR_UP);
	}

	possChild = glm::vec2(parent->cellPos.x - 1, parent->cellPos.y);  //Left
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, STR_LEFT);
	}

	possChild = glm::vec2(parent->cellPos.x + 1, parent->cellPos.y);  //Right
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, STR_RIGHT);
	}

	possChild = glm::vec2(parent->cellPos.x, parent->cellPos.y + 1);  //Below
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, STR_DOWN);
	}

	//Check & add diagonal children
	possChild = glm::vec2(parent->cellPos.x - 1, parent->cellPos.y - 1);  //Above & Left
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, DIA_UP_LEFT);
	}

	possChild = glm::vec2(parent->cellPos.x + 1, parent->cellPos.y - 1);  //Above & Right
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, DIA_UP_RIGHT);
	}

	possChild = glm::vec2(parent->cellPos.x - 1, parent->cellPos.y + 1);  //Below & Left
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, DIA_DOWN_LEFT);
	}

	possChild = glm::vec2(parent->cellPos.x + 1, parent->cellPos.y + 1);  //Below & Right
	if (gridMap->isInGrid(possChild)) {
		int mapLoc = getMapPos(possChild);
		checkChild(parent, mapLoc, DIA_DOWN_RIGHT);
	}
}

void AStar::checkChild(Node* parent, int nodePos, int cost){

	//out of map
	if (nodePos >= 0 && nodePos < gridMap->numCells) {
		bool goalCell = false;
		if (nodePos == getMapPos(goal->cellPos))
			goalCell = true;
		//valid cell - not ocupied or goal cell
		if (gridMap->worldMap[nodePos] || goalCell) {
			//Not already Processed
			if (nodeMap[nodePos]->status == NOT_PROCESSED) {
				nodeMap[nodePos]->parent = parent;
				nodeMap[nodePos]->calcCosts(parent, cost, goal->cellPos);

				openList.push_back(nodeMap[nodePos]);
				nodeMap[nodePos]->status = OPEN_LIST;
				parent->children[parent->numChildren] = nodeMap[nodePos];
				parent->numChildren++;

			}
			else {
				//If cell is already in OpenList
				if (nodeMap[nodePos]->status == OPEN_LIST) {
					//compare costs of using this path instead
					if (newPathBetter(nodeMap[nodePos], (parent->startCost + cost))) {
						//change old parent to current parent & recalc costs
						nodeMap[nodePos]->parent = parent;
						nodeMap[nodePos]->calcCosts(parent, cost, goal->cellPos);
						
						openList.push_back(nodeMap[nodePos]);
						nodeMap[nodePos]->status = OPEN_LIST;
						parent->children[parent->numChildren] = nodeMap[nodePos];
						parent->numChildren++;
					}
				}
			}
		}
		else {
			nodeMap[nodePos]->status = INVALID;
		}
	}
}

void AStar::search(){

	//Find Node with lowest total cost
	Node* curBest = getBestNode();

	//Remove bestNode from openlist and add to closedList
	if(openList.size() != 0)
		openList.erase(openList.begin());

	closedList.push_back(curBest);
	curBest->status = CLOSED_LIST;

	//Find children
	findChildren(curBest);
}

vector<glm::vec2> AStar::getPath(){

	list<glm::vec2> travelList;

	//Add finish nodes postion to travel list
	travelList.push_back(closedList.back()->cellPos);
	//get the parent of finish node
	Node* nextNode = closedList.back()->parent;

	//while the next node isn't the start node
	while (nextNode != start) {
		//add the next nodes postion to travel list
		travelList.push_back(nextNode->cellPos);
		//set the next node to its parent
		nextNode = nextNode->parent;
	}

	//Reverse to in correct order (start to finish rather than finish - start)
	travelList.reverse();

	//Convert to vector for ease later
	vector<glm::vec2> path{ make_move_iterator(begin(travelList)), make_move_iterator(end(travelList)) };

	return path;
}

Node * AStar::getBestNode(){

	make_heap(openList.begin(), openList.end(), compareNodes);

	if (openList.size() == 0)
		return this->start;

	return openList.front();
}
