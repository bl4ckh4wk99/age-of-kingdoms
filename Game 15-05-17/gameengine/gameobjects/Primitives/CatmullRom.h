#pragma once


#include <glm\glm.hpp>
#include <vector>


class CatmullRom {

public:

	CatmullRom() {}

	std::vector<glm::vec3> CatmullRomSpline(glm::vec3 point0, glm::vec3 point1, glm::vec3 point2, glm::vec3 point3, int steps);

	float q(float point0, float point1, float point2, float point3, float t);


};