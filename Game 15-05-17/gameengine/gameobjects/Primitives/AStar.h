#pragma once

#include <glm\glm.hpp>
#include <vector>
#include <list>
#include <algorithm>
#include <Windows.h>

#include "../World/Grid.h"

#define ADJACENT_COST 10
#define DIAGONAL_COST 14

#define STR_UP 10
#define STR_RIGHT 10
#define STR_DOWN 10
#define STR_LEFT 10

#define DIA_UP_LEFT 14
#define DIA_UP_RIGHT 12
#define DIA_DOWN_RIGHT 14
#define DIA_DOWN_LEFT 12


using namespace std;

enum NodeStatus{NOT_PROCESSED, INVALID, OPEN_LIST, CLOSED_LIST};

struct Node {

	int startCost;	//Cost to go from this node to start node (folowwing path of nodes)
	int goalCost;	//Cost to reach target (estimate)
	int baseCost;	//Cost for this node

	int totalCost;	// Base + Start + Goal

	glm::vec2 cellPos;	//Cell pos of node

	Node* parent;	//Pointer to parent node
	Node* next;		//Pointer to next node (to be decided upon)

	Node* children[8];	//pointer to 8 adjacent nodes that become nodes children
	int numChildren;

	NodeStatus status;

	Node() {
		startCost = goalCost = baseCost = totalCost = 0;

		cellPos = glm::vec2(0, 0);

		parent = nullptr;
		next = nullptr;

		for (int i = 0; i < 8; i++) {
			children[i] = nullptr;
		}
		numChildren = 0;
		status = NOT_PROCESSED;
	}

	Node(glm::vec2 cellPos) {

		this->cellPos = cellPos;

		startCost = goalCost = baseCost = totalCost = 0;

		parent = nullptr;
		next = nullptr;

		for (int i = 0; i < 8; i++) {
			children[i] = nullptr;
		}
		numChildren = 0;
		status = NOT_PROCESSED;
	}

	~Node() {
		

	}

	void giveParent(Node* parentNode) {
		parent = parentNode;
	}

	void giveNext(Node* nextNode) {
		next = nextNode;
	}

	void giveChildren(Node* children[8]) {
		for (int i = 0; i < 8; i++) {
			this->children[i] = children[i];
		}
	}

	void calcCosts(Node* parent, int cost, glm::vec2 goal) {

		baseCost = cost; 
		startCost = parent->startCost + baseCost;
		goalCost = calcHeuristic(cellPos, goal);
		totalCost = baseCost + startCost + goalCost; 
	}

	int calcHeuristic(glm::vec2 from, glm::vec2 to) {
		return (int)glm::length(from - to);
	}

	bool operator<(const Node* node) const { return totalCost < node->totalCost; }

	bool operator!=(const Node* node) const {
		return cellPos.x == node->cellPos.x && cellPos.y == node->cellPos.y;
	}

};


class AStar {
private:

	Node *start;
	Node *goal;
	vector<Node*> goalList;

	vector<Node*> openList;
	vector<Node*> closedList;

	Grid* gridMap;
	Node** nodeMap;

	void findChildren(Node* parent);

	void checkChild(Node* parent, int nodePos, int cost);

	void search();

	vector<glm::vec2> getPath();

	Node* getBestNode();

	int getMapPos(glm::vec2 cellPos) {
		return (int)(cellPos.x + (cellPos.y * gridMap->gridSize));
	}

	bool newPathBetter(Node* previouslyProcessedNode, int startCost) {
		return previouslyProcessedNode->startCost > startCost;
	}

public:

	AStar() {}

	AStar(Grid* map) { this->gridMap = map; }

	vector<glm::vec2> generatePath(glm::vec2 start, glm::vec2 target);

	vector<glm::vec2> generatePath(glm::vec2 start, glm::vec2 targetCen, int targetDim);

	void cleanup();

};