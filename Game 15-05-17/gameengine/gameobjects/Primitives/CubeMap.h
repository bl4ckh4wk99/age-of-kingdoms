#pragma once

#include <gl\glew.h>
#include <glm\glm.hpp>
#include "../../include/SOIL.h"
#include <vector>
#include <iostream>

using namespace std;

class CubeMap{
private:
	//OpenGL variables
	GLuint vbo, vao;
	GLuint cubeTex;
	vector<const GLchar*> faceTex;

public:

	CubeMap();

	void createVBO();

	void createCubeMap();

	void draw();

};