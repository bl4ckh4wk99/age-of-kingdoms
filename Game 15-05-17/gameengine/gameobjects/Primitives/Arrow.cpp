#include "Arrow.h"

Arrow::Arrow(AssetLibrary * assets){

	this->arrow = assets->getArrow();
	this->shader = assets->getModelShader();

	this->flightPath = vector<glm::vec3>();

	transform = Transform();
	transform.setScale(glm::vec3(0.1, -0.1, 0.1));
}

void Arrow::Init(glm::vec3 from, glm::vec3 to){


	glm::vec3 parabPeak = from + ((to - from)/2.0f);
	float yComp = (glm::distance(from, to) / 10.0f)* PEAK_HEIGHT;

	parabPeak.y += yComp;

	glm::vec3 p0, p1, p2, p3;
	CatmullRom splineMaker;
	int steps = 30;

	//first half
	p0 = from;
	p1 = from;
	p2 = parabPeak;
	p3 = to;
	vector<glm::vec3> tempPoints = splineMaker.CatmullRomSpline(p0, p1, p2, p3, steps);
	for (glm::vec3 p : tempPoints) {
		flightPath.push_back(p);
	}

	//second half
	p0 = from;
	p1 = parabPeak;
	p2 = to;
	p3 = to;
	tempPoints = splineMaker.CatmullRomSpline(p0, p1, p2, p3, steps);
	for (glm::vec3 p : tempPoints) {
		flightPath.push_back(p);
	}

}

void Arrow::Draw(Camera* cam){

	shader->Bind();

	shader->Update(&transform, cam);

	arrow->BindModelsVAO();
	arrow->RenderModel();

}

void Arrow::Update(double deltaT){

	if (!flightPath.empty()) {

		transform.setPos(flightPath.front());

		flightPath.erase(flightPath.begin());

	}

}

bool Arrow::endOfPath(){
	return flightPath.empty();
}
