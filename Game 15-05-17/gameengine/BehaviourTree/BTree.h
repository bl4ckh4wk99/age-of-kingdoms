#pragma once

#include "Components\Action.h"
#include "Components\Condition.h"
#include "Components\Selector.h"
#include "Components\Sequencer.h"
#include "Components\RandomSelector.h"
#include "Components\RandomSequencer.h"
#include "Components\TryAllRoot.h"

template <class T>
class BTree {
private:

	TreeNode<T>* root;

public:
	BTree() {}

	//template <class T>
	BTree(TreeNode<T> * root) {
		this->root = root;
	}

	//template <class T>
	bool execute(T type) {
		return root->execute(type);
	}


};

//#include "BTree.cpp"

