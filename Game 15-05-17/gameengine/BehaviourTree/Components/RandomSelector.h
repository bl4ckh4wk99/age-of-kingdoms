#pragma once

#include "TreeNode.h"

template <class T>
class RandomSelector : public TreeNode <T> {

public:

	//template <class T>
	RandomSelector(std::vector<TreeNode*> children) {
		this->children = children;
	}

	//template <class T>
	bool execute(T type) {

		std::random_shuffle(children.begin(), children.end());

		for (TreeNode* child : children) {
			bool success = child->execute(type);
			if (success)
				return true;
		}

		return false;
	}


};

//#include "RandomSelector.cpp"
