#pragma once
#include "TreeNode.h"

template <class T>
class Sequencer : public TreeNode <T> {

public:

	//template <class T>
	Sequencer(std::vector<TreeNode*> children) {
		this->children = children;
	}

	//template <class T>
	bool execute(T type) {

		for (TreeNode* child : children) {
			bool success = child->execute(type);
			if (!success)
				return false;
		}
		return true;
	}


};

//#include "Sequencer.cpp"
