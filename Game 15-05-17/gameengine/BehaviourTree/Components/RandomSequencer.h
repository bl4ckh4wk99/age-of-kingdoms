#pragma once
#include "TreeNode.h"

template <class T>
class RandomSequencer : public TreeNode <T> {

public:

	//template <class T>
	RandomSequencer(std::vector<TreeNode*> children) {
		this->children = children;
	}

	//template <class T>
	bool execute(T type) {

		std::random_shuffle(children.begin(), children.end());

		for (TreeNode* child : children) {
			bool success = child->execute(type);
			if (!success)
				return false;
		}
		return true;
	}


};

//#include "RandomSequencer.cpp"