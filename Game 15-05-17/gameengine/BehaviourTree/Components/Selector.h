#pragma once
#include "TreeNode.h"

template <class T>
class Selector : public TreeNode <T> {

public:

	Selector(std::vector<TreeNode*> children) {
		this->children = children;
	}

	bool execute(T type) {

		for (TreeNode* child : children) {
			bool success = child->execute(type);
			if (success)
				return true;
		}

		return false;
	}
};