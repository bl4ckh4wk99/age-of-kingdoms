#pragma once
#include "TreeNode.h"

template <class T>
class Action : public TreeNode <T> {
private:

	void(*func)(T type);

public:

	Action(void(*func)(T type)) {
		this->func = func;
	}

	bool execute(T type) {
		func(type);
		return true;
	}
};
