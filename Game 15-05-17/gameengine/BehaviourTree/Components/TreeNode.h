#pragma once
#include <vector>
//#include "../../gameobjects/Users/Player.h"

//enum Evaluation{FAIL, RUNNING, PASS};

template <class T>
class TreeNode {
protected:

	std::vector <TreeNode<T>*> children;

public:

	virtual bool execute(T type) = 0;

};


