#pragma once
#include "TreeNode.h"

template <class T>
class Condition : public TreeNode <T> {
private:
	bool (*func)(T type);

public:

	//template <class T>
	Condition(bool(*func)(T type)) {
		this->func = func;
	}

	//template <class T>
	bool execute(T type) {
		return func(type);
	}


};

//#include "Condition.cpp"