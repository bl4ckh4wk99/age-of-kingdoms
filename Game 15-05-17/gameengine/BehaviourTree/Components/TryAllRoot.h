#pragma once
#include "TreeNode.h"

template <class T>
class TryAllRoot : public TreeNode <T> {

public:

	//template <class T>
	TryAllRoot(std::vector<TreeNode*> children) {
		this->children = children;
	}

	//template <class T>
	bool execute(T type) {

		for (TreeNode* child : children) {
			child->execute(type);
		}
		return true;
	}


};
